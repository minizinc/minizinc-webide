
`$$a` ... enum or int type

MiniZinc Questions:

 * more reflection features
 * advanced model data per variable/input/output
  * also include attached annotations (name and parameter values)
  * include domain limit if possible (e.g. `1..n`)
  * enums in ???
  * `int: n :: slider(0, 100);`
  * annotation to constraints
  * does generic overload work, like that: `f1($0: a) = f2(a), f2(int: a) = "a", f2($0: b) = "other"` ... not supported
  * unify error messages: syntax errors: `file:line... message` but also have seen for others `message ... file`
  * overwrite value in `data.json` that is defined
 * error format:
syntax errors:
``` 
model.mzn:14.1-10:
constraint defineUI([
^^^^^^^^^^
Error: syntax error, unexpected constraint, expecting ':'
```
vs
```
MiniZinc: evaluation error:  
  model.mzn:5: 
  in variable declaration for 'ir'
  parameter value out of range
```
 * 
Tasks
 * different modi for the ui similar to most editors: code, output, input
 * more input widgets: slider, array input so text area again


Features
 * syntax highlighting
 * auto analyze/compile + inplace errors
 * auto detect inputs and create UI for it
 * JS REST api
 * charts/json/interactive charts
 * 
 * Electron



## MiniZinc Magic Keywords

```
%%%mzn-json-time
%%%mzn-json-init:<filename>
%%%mzn-json-init-end
%%%mzn-json:<filename>
%%%mzn-json-end
```
