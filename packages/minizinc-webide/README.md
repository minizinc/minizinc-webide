minizinc-webide
============
[![License: MIT][mit-image]][mit-url] [![Netlify Status][netlify-image]][netlify-url]


Usage
-----

**TODO**


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[netlify-image]: https://api.netlify.com/api/v1/badges/f9341ac5-e7bd-4abd-9552-4f54cb087d51/deploy-status
[netlify-url]: https://app.netlify.com/sites/minzinc-ide/deploys
