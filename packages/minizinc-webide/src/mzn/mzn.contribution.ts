
import {registerLanguage} from 'monaco-editor/esm/vs/basic-languages/_.contribution.js';
import * as mzn from './mzn';

registerLanguage({
    id: 'minizinc',
    extensions: ['.mzn', '.dzn', '.fzn'],
    aliases: ['minizinc', 'mzn'],
    loader: () => Promise.resolve(mzn)
});
