import {languages} from 'monaco-editor/esm/vs/editor/editor.api';

export const conf: languages.LanguageConfiguration = {
  comments: {
    // symbol used for single line comment. Remove this entry if your language does not support line comments
    lineComment: '%',
    // symbols used for start and end a block comment. Remove this entry if your language does not support block comments
    blockComment: ['/*', '*/']
  },
  // symbols used as brackets
  brackets: [
    ['{', '}'],
    ['[', ']'],
    ['(', ')']
  ],
  // symbols that are auto closed when typing
  autoClosingPairs: [
    {open: '{', close: '}'},
    {open: '[', close: ']'},
    {open: '(', close: ')'},
    {open: '"', close: '"'},
    {open: '\'', close: '\''}
  ],
  // symbols that that can be used to surround a selection
  surroundingPairs: [
    {open: '{', close: '}'},
    {open: '[', close: ']'},
    {open: '(', close: ')'},
    {open: '"', close: '"'},
    {open: '\'', close: '\''}
  ]
};

export const language = <languages.IMonarchLanguage>{
  defaultToken: '',
  tokenPostfix: '.mzn',
  keywords: [
    //
    'annotation', 'constraint', 'function', 'include', 'op', 'output', 'minimize', 'maximize', 'predicate', 'satisfy', 'solve', 'test', 'type',
    //
    'for', 'forall', 'if', 'then', 'elseif', 'else', 'endif', 'where', 'let', 'in',
    //
    'any', 'case', 'op', 'record'
  ],

  builtins: [
    'abort', 'abs', 'acosh', 'array_intersect', 'array_union', 'array1d', 'array2d', 'array3d', 'array4d', 'array5d', 'array6d', 'asin', 'assert', 'atan', 'bool2int', 'card', 'ceil', 'concat', 'cos', 'cosh', 'dom', 'dom_array', 'dom_size', 'fix', 'exp', 'floor', 'index_set', 'index_set_1of2', 'index_set_2of2', 'index_set_1of3', 'index_set_2of3', 'index_set_3of3', 'int2float', 'is_fixed', 'join', 'lb', 'lb_array', 'length', 'ln', 'log', 'log2', 'log10', 'min', 'max', 'pow', 'product', 'round', 'set2array', 'show', 'show_int', 'show_float', 'sin', 'sinh', 'sqrt', 'sum', 'tan', 'tanh', 'trace', 'ub', 'ub_array',
    //
    'circuit', 'disjoint', 'maximum', 'maximum_arg', 'member', 'minimum', 'minimum_arg', 'network_flow', 'network_flow_cost', 'partition_set', 'range', 'roots', 'sliding_sum', 'subcircuit', 'sum_pred',
    //
    'alldifferent', 'all_different', 'all_disjoint', 'all_equal', 'alldifferent_except_0', 'nvalue', 'symmetric_all_different',
    //
    'lex2', 'lex_greater', 'lex_greatereq', 'lex_less', 'lex_lesseq', 'strict_lex2', 'value_precede', 'value_precede_chain',
    //sorting constraints (globals)
    'arg_sort', 'decreasing', 'increasing', 'sort',
    //channeling constraints (globals)
    'int_set_channel', 'inverse', 'inverse_set', 'link_set_to_booleans',
    //counting constraints (globals)
    'among', 'at_least', 'at_most', 'at_most1', 'count', 'count_eq', 'count_geq', 'count_gt', 'count_leq', 'count_lt', 'count_neq', 'distribute', 'exactly', 'global_cardinality', 'global_cardinality_closed', 'global_cardinality_low_up', 'global_cardinality_low_up_closed',
    //packing constraints (globals)
    'bin_packing', 'bin_packing_capa', 'bin_packing_load', 'diffn', 'diffn_k', 'diffn_nonstrict', 'diffn_nonstrict_k', 'geost', 'geost_bb', 'geost_smallest_bb', 'knapsack',
    //scheduling constraints (globals)
    'alternative', 'cumulative', 'disjunctive', 'disjunctive_strict', 'span',
    //extensional constraints (globals)
    'regular', 'regular_nfa', 'table'
  ],

  typeKeywords: [
    //
    'ann', 'array', 'bool', 'enum', 'float', 'int', 'list', 'of', 'par', 'set', 'string', 'tuple', 'var', 'opt',
    //
    'true', 'false'
  ],

  operators: [
    'not', '<->', '->', '<-', '\\/', 'xor', '/\\',
    '<', '>', '<=', '>=', '==', '=', '!=',
    '+', '-', '*', '/', 'div', 'mod', '',
    'in', 'subset', 'superset', 'union', 'diff', 'symdiff', 'intersect'
  ],


  // we include these common regular expressions
  symbols: /[=><!~?:&|+\-*\\\/\^%]+/,
  // we include these common regular expressions
  escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
  digits: /\d+(_+\d+)*/,
  octaldigits: /[0-7]+(_+[0-7]+)*/,
  binarydigits: /[0-1]+(_+[0-1]+)*/,
  hexdigits: /[[0-9a-fA-F]+(_+[0-9a-fA-F]+)*/,


  // The main tokenizer for our languages
  tokenizer: {
    root: [
      // identifiers and keywords
      [/[a-zA-Z_\x80-\xFF][\w\x80-\xFF]*/, {
        cases: {
          '@keywords': 'keyword',
          '@typeKeywords': 'keyword',
          '@builtins': 'predefined',
          '@default': 'identifier'
        }
      }],

      // whitespace
      {include: '@whitespace'},

      [/[{}()\[\]]/, '@brackets'],
      [/@symbols/, {
        cases: {
          '@operators': 'delimiter',
          '@default': ''
        }
      }],

      // numbers
      [/(@digits)[eE]([\-+]?(@digits))?[fFdD]?/, 'number.float'],
      [/(@digits)\.(@digits)([eE][\-+]?(@digits))?[fFdD]?/, 'number.float'],
      [/0[xX](@hexdigits)[Ll]?/, 'number.hex'],
      [/0(@octaldigits)[Ll]?/, 'number.octal'],
      [/0[bB](@binarydigits)[Ll]?/, 'number.binary'],
      [/(@digits)[fFdD]/, 'number.float'],
      [/(@digits)[lL]?/, 'number'],

      // delimiter: after number because of .\d floats
      [/[;,.]/, 'delimiter'],

      // strings
      [/"([^"\\]|\\.)*$/, 'string.invalid'],  // non-teminated string
      [/"/, {token: 'string.quote', bracket: '@open', next: '@string'}],
    ],

    comment: [
      [/[^\/*]+/, 'comment'],
      [/\/\*/, 'comment', '@push'],    // nested comment
      ['\\*/', 'comment', '@pop'],
      [/[\/*]/, 'comment']
    ],

    string: [
      [/[^\\"&]+/, 'string'],
      [/\\"/, 'string.escape'],
      [/&\w+;/, 'string.escape'],
      [/[\\&]/, 'string'],
      [/"/, {token: 'string.quote', bracket: '@close', next: '@pop'}]
    ],

    whitespace: [
      [/[ \t\r\n]+/, 'white'],
      [/\/\*/, 'comment', '@comment'],
      [/\/\/.*$/, 'comment'],
      [/%.*$/, 'comment'],
    ],
  }
};
