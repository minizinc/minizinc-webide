import {ApplicationStore} from './stores';
import {IDataObject} from 'minizinc';

export interface IFile {
  fileName: string;
  content: string;
}


export interface IUIAdapter {
  store: ApplicationStore | null;

  openFile(): Promise<IFile>;
  openData(): Promise<IDataObject>;

  save(fileName: string, code: string, data: IDataObject): void;

  saveAs(code: string, data: IDataObject): string | null;
}

export function cleanFileName(fileName: string) {
  fileName = fileName.replace('\\', '/');
  let extension = fileName.lastIndexOf('.');
  if (extension < 0) {
    extension = fileName.length;
  }
  const dir = fileName.lastIndexOf('/');
  return fileName.slice(dir + 1, extension);
}
