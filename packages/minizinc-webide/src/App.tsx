import * as React from 'react';
import {observer, inject} from 'mobx-react';
import Editor from './components/Editor';
import Inputs from './components/Inputs';
import Solutions from './components/Solutions';
import Header from './components/Header';
import Footer from './components/Footer';
import {IWithStore} from './stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import classNames from 'classnames';

const styles = (theme: Theme) => createStyles({
  root: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    flexDirection: 'column'
  },
  appBarSpacer: theme.mixins.toolbar,
  main: {
    flex: '1 1 0',
    display: 'grid',
    gridGap: '1rem',
    paddingTop: '0.5rem',
    gridTemplateColumns: 'repeat(5, 1fr)',
    gridTemplateRows: 'repeat(2, 1fr)',
    gridTemplateAreas: `"editor editor editor inputs inputs"
    "editor editor editor solutions solutions"`,
    minHeight: 0,
    minWidth: 0
  },
  mainInOutput: {
    gridTemplateAreas: `"inputs inputs solutions solutions solutions"
    "inputs inputs solutions solutions solutions"`,
  },
  mainOutput: {
    gridTemplateAreas: `"solutions solutions solutions solutions solutions"
    "solutions solutions solutions solutions solutions"`,
  },
  mainEditorOutput: {
    gridTemplateAreas: `"editor editor editor solutions solutions"
    "editor editor editor solutions solutions"`,
  },
  editor: {
    gridArea: 'editor',
    minHeight: 0,
    minWidth: 0
  },
  inputs: {
    gridArea: 'inputs',
    padding: '1rem',
    overflow: 'auto',
    minHeight: 0,
    minWidth: 0
  },
  solutions: {
    gridArea: 'solutions',
    padding: '1rem',
    overflow: 'auto',
    minHeight: 0,
    minWidth: 0
  }
});



export interface IAppProps extends WithStyles<typeof styles>, IWithStore {

}


@inject('store')
@observer
class App extends React.Component<IAppProps> {
  render() {
    const classes = this.props.classes;
    const store = this.props.store!;

    return <div className={classes.root}>
      <CssBaseline/>
      <Header />
      <div className={classes.appBarSpacer} />
      <main className={classNames(classes.main, {
        [classes.mainOutput]: !store.ui.editorVisible && !store.ui.inputsVisible,
        [classes.mainInOutput]: !store.ui.editorVisible && store.ui.inputsVisible,
        [classes.mainEditorOutput]: store.ui.editorVisible && !store.ui.inputsVisible
      })}>
        <Solutions className={classes.solutions} />
        {store.ui.inputsVisible && <Inputs className={classes.inputs} />}
        {store.ui.editorVisible && <Editor className={classes.editor} />}
      </main>
      <Footer />
    </div>;
  }
}

export default withStyles(styles)(App);
