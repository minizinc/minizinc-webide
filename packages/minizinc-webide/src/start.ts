// import EmbeddedMiniZinc from 'minizinc/build/EmbeddedMiniZinc';
import launch from './';
import WebUIAdapter from './WebUIAdapter';
import charts from 'raw-loader!../scripts/charts.mzn';
import json from 'raw-loader!../scripts/json.mzn';
import ui from 'raw-loader!../scripts/ui.mzn';
import createMiniZinc from 'minizinc/build/internal/workerloader';

const minizinc = createMiniZinc({
  extraIncludeFiles: {
    'charts.mzn': charts,
    'json.mzn': json,
    'ui.mzn': ui,
  }
});

launch(minizinc, new WebUIAdapter());



