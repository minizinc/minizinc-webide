import {observable, action, autorun, toJS, computed} from 'mobx';
import {IModelMetaData, IResult, IMiniZinc, IError, IEnteredValue, IModelParams, IDataObject, defaultValue, formatValue, injectTypeInfo, ITypedDataObject, asTypedValue, EType, AbortSignal, ISolverConfiguration} from 'minizinc';
import {bind} from 'decko';
import {IUIAdapter} from '../interfaces';

const DEFAULT_MODEL = `
include "ui.mzn";
include "charts.mzn";

int: n; % The number of queens.

array [1..n] of var 1..n: q;

include "alldifferent.mzn";

constraint alldifferent(q);
constraint alldifferent(i in 1..n)(q[i] + i);
constraint alldifferent(i in 1..n)(q[i] - i);

constraint defineUI([
  h4("Solutions"),
  perSolution([
    h6("Solution"),
    barchart("bb")
  ])
]);

output outputJSON();

output outputUIData([
  "bb", barchartData(q)
]);

`;

const DEFAULT_FILENAME = 'Untitled';

export interface IUIFlags {
  solverChooserAnchor: HTMLElement | null;
  nrSolutionsChooserAnchor: HTMLElement | null;
  visibleErrorAnchors: HTMLElement | null;
  optionsMenuAnchor: HTMLElement | null;
  drawerOpen: boolean;
  solutionTab: number;
  editorVisible: boolean;
  inputsVisible: boolean;
  solutionsUpTo: number;
  showAboutDialog: boolean;
}

export class ApplicationStore {
  @observable
  fileName: string = DEFAULT_FILENAME;

  @observable
  savedState: {code: string, data: string} | null = null;

  @observable
  enteredData: IEnteredValue[] = [];

  @observable
  interactiveData: IDataObject = {};

  @observable
  running: boolean = false;

  @observable
  errors: IError[] = [];

  @observable
  modelMetaData: IModelMetaData | null = null;

  @observable
  result: IResult | null = null;

  @observable
  selectedSolution = 0;

  @observable
  autoAnalyze: boolean = true;

  @observable
  abortSignal: AbortSignal | null = null;

  @observable
  solvers: ISolverConfiguration[] = [];

  @observable
  params: IModelParams = {
    model: DEFAULT_MODEL,
    solver: undefined
  };

  @observable
  version: string = 'Unknown';

  @observable
  ui: IUIFlags = {
    solutionsUpTo: 10,
    solverChooserAnchor: null,
    nrSolutionsChooserAnchor: null,
    optionsMenuAnchor: null,
    visibleErrorAnchors: null,
    drawerOpen: false,
    solutionTab: 0,
    editorVisible: true,
    inputsVisible: true,
    showAboutDialog: false
  };

  constructor(private readonly backend: IMiniZinc, public readonly adapter: IUIAdapter) {
    adapter.store = this;

    let firstRun = true;

    const stored = localStorage.getItem('store');
    if (stored) {
      Object.assign(this, JSON.parse(stored!));
    }

    autorun(() => {
      const json: any = toJS(this);
      delete json.backend;
      delete json.adapter;
      delete json.abortSignal;
      delete json.ui.visibleErrorAnchors;
      delete json.ui.solverChooserAnchor;
      delete json.ui.nrSolutionsChooserAnchor;
      delete json.ui.optionsMenuAnchor;

      if (!firstRun) {
        localStorage.setItem('store', JSON.stringify(json));
      }
      firstRun = false;
    }, {
        delay: 100
      });

    let lastCode: string = '';

    autorun(() => {
      // access code for being reactive
      if (!this.code || !this.autoAnalyze || lastCode === this.code.trim()) {
        return;
      }
      lastCode = this.code.trim();
      this.compile();
    }, {
        delay: 1000
      });

    this.init();
  }

  @computed
  get code() {
    return this.params.model;
  }

  set code(value: string) {
    this.params.model = value;
  }

  private init() {
    this.backend.resolveSolvers()
      .then((solvers) => this.solvers = solvers)
      .catch(this.catchError);
    this.backend.version()
      .then((version) => this.version = version)
      .catch(this.catchError);
  }

  @action
  compile() {
    this.running = true;
    this.errors = [];

    this.abortSignal = new AbortSignal();

    return this.backend.analyze(this.code, {fileName: this.fileName, signal: this.abortSignal}).then((modelMetaData) => {
      this.running = false;
      this.modelMetaData = modelMetaData;
      this.abortSignal = null;
      this.deriveEnteredValues();
      return true;
    }).catch(this.catchError);
  }

  @bind
  private catchError(error: any) {
    this.running = false;
    this.abortSignal = null;
    console.warn(error);
    if (Array.isArray(error.errors)) {
      error.errors.forEach((e: any) => this.errors.push(e));
    } else {
      this.errors.push(error.error || error);
    }
    return false;
  }

  private deriveEnteredValues() {
    const existing = new Map(this.enteredData.map((d) => <[string, IEnteredValue]>[d.key, d]));

    let changed = false;

    this.enteredData = this.modelMetaData!.inputs.map((input) => {
      let old = existing.get(input.key);
      if (!old || old.type !== input.type || old.dim !== input.dim || old.set !== input.set || old.enum_type !== input.enum_type) {
        old = undefined;
      }
      if (old) {
        return old;
      }
      changed = true;
      const value = defaultValue(input);
      return Object.assign({
        value,
        valueAsString: formatValue(input, value),
        error: value == null ? 'value required' : undefined
      }, input);
    });

    if (changed) {
      this.result = null;
    }
  }

  @computed
  get canAbort() {
    return this.abortSignal != null;
  }

  abort() {
    if (!this.abortSignal) {
      return;
    }
    this.abortSignal.abort();
  }

  @action
  solve() {
    if (!this.validInput) {
      return;
    }
    this.running = true;
    this.errors = [];
    this.result = null;

    this.abortSignal = new AbortSignal();

    const model: IModelParams = Object.assign({}, this.params);

    const onPartialResult = (_type: string, result: IResult) => {
      this.result = result;
    };

    return this.backend.solve(model, this.data, {fileName: this.fileName, signal: this.abortSignal, onPartialResult}).then((result) => {
      this.running = false;
      this.result = result;
      this.abortSignal = null;
      return true;
    }).catch(this.catchError);
  }

  @computed
  get data() {
    const r: IDataObject = {};
    for (const entry of this.enteredData) {
      if (!entry.error && entry.value != null) {
        r[entry.key] = toJS(entry.value);
      }
    }
    Object.assign(r, this.interactiveData);

    return r;
  }

  @computed
  get validInput() {
    return this.modelMetaData && this.enteredData.every(((d) => !d.error && d.value != null));
  }

  save() {
    const data = this.data;
    if (this.fileName !== DEFAULT_FILENAME) {
      this.adapter.save(this.fileName, this.code, data);
      this.ui.drawerOpen = false;
      this.savedState = {code: this.code, data: JSON.stringify(data)};
      return;
    }
    return this.saveAs();
  }

  saveAs() {
    const data = this.data;
    const f = this.adapter.saveAs(this.code, data);
    if (f) {
      this.fileName = f;
      this.ui.drawerOpen = false;
      this.savedState = {code: this.code, data: JSON.stringify(data)};
    }
    return f != null;
  }

  @action
  createNew(code: string = DEFAULT_MODEL, fileName: string = DEFAULT_FILENAME) {
    this.ui.drawerOpen = false;
    this.code = code;
    this.fileName = fileName;
    this.errors = [];
    this.modelMetaData = null;
    this.enteredData = [];
    this.interactiveData = {};
    this.result = null;
    this.savedState = null;
    this.ui.editorVisible = true;

    if (this.autoAnalyze) {
      this.compile();
    }
  }

  open() {
    this.adapter.openFile().then((file) => {
      this.createNew(file.content, file.fileName);
      this.savedState = {code: file.content, data: JSON.stringify({})};
    }).catch((error) => console.warn(error));
  }

  openData() {
    this.adapter.openData().then((data) => {
      this.ui.drawerOpen = false;
      this.integrateData(data);
      if (this.savedState) {
        this.savedState.data = JSON.stringify(this.data);
      }
    }).catch((error) => console.warn(error));
  }

  private integrateData(data: IDataObject) {
    for(const elem of this.enteredData) {
      const value = data[elem.key];
      if (value == null) {
        elem.value = null;
        elem.valueAsString = '';
      } else {
        elem.value = value;
        elem.valueAsString = formatValue(elem, value);
      }
    }
  }

  @computed
  get uiDefinition(): any[] {
    if (!this.result) {
      return [];
    }
    return (<any>this.result.header)._uiDefinition || [];
  }

  @computed
  get solution() {
    const s = this.solutions;
    return s.length > 0 ? s[s.length - 1] : null;
  }


  @computed
  get uiSolutionData(): any {
    if (!this.result) {
      return {};
    }
    const s = this.result.solutions[this.result.solutions.length - 1];
    return s.assignments._uiData || {};
  }

  @computed
  get uiSolutionDatas(): any[] {
    if (!this.result) {
      return [];
    }
    return this.result.solutions.map((s) => s.assignments._uiData || {});
  }

  @computed
  get solutions(): ITypedDataObject[] {
    if (!this.result) {
      return [];
    }
    const outputs = this.result.outputs;
    return this.result.solutions.map((s) => {
      const r: ITypedDataObject = {};
      for (const out of outputs) {
        r[out.key] = asTypedValue(toJS(s.assignments[out.key]), out);
      }
      return r;
    });
  }

  @computed
  get objectives() {
    const values = !this.result ? [] : this.result.solutions.map((d) => d.objective!);
    return injectTypeInfo(values, {
      key: 'objectives',
      type: EType.float,
      dim: 1,
      dims: ['float']
    });
  }

  @computed
  get isDefaultFileName() {
    return this.fileName === DEFAULT_FILENAME;
  }

  @computed
  get isDirtyState() {
    if (!this.savedState) {
      return true;
    }
    return this.savedState.code !== this.code || JSON.stringify(this.data) !== this.savedState.data;
  }
}
