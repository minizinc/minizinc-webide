import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Paper from '@material-ui/core/Paper';
import ValueInput from './ValueInput';
import classNames from 'classnames';

const styles = (_theme: Theme) => createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  form: {
    flex: '1 1 0'
  },
  input: {
    margin: '0.5rem'
  }
});

export interface IInputsProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
  style?: React.CSSProperties;
}

@inject('store')
@observer
class Inputs extends React.Component<IInputsProps> {
  @bind
  private onAnalyze() {
    const store = this.props.store!;
    store.compile();
  }

  render() {
    const store = this.props.store!;
    const classes = this.props.classes;

    const analyze = <IconButton onClick={this.onAnalyze} color="inherit" title="Analyze" disabled={store.running}>
      <CheckCircle />
    </IconButton>;

    return <Paper className={classNames(this.props.className, classes.root)} style={this.props.style}>
        <Typography variant="h6" color="inherit">
          Inputs
        </Typography>
        <form className={classes.form}>
        {!store.modelMetaData ? analyze : store.enteredData.map((prop) => <ValueInput prop={prop} key={prop.key} className={classes.input}/>)}
        </form>
      </Paper>;
  }
}


export default withStyles(styles)(Inputs);
