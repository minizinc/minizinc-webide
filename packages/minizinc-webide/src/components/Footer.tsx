
import * as React from 'react';
//import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';


const styles = (_theme: Theme) => createStyles({

});

export interface IFooterProps extends IWithStore, WithStyles<typeof styles> {

}

@inject('store')
@observer
class Footer extends React.Component<IFooterProps> {
  render() {
    const store = this.props.store!;
    return store.running ? <LinearProgress /> : <div/>;
  }
}


export default withStyles(styles)(Footer);
