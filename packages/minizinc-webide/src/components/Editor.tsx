
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import {asyncComponent} from 'react-async-component';


const styles = (theme: Theme) => createStyles({
  root: {
  },
  errorLine: {
    background: theme.palette.error.light
  },
  errorIcon: {
    background: theme.palette.error.main
  }
});

export interface IEditorProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
  style?: React.CSSProperties;
}

const ASYNC_EDITOR = asyncComponent({
  resolve: () => import('./EditorWrapper')
});

@inject('store')
@observer
class Editor extends React.Component<IEditorProps> {
  @bind
  private setValue(code: string) {
    this.props.store!.code = code;
  }

  render() {
    const classes = this.props.classes;
    const store = this.props.store!;

    // dummy identity to force mobx to check we need all of them
    const errors = store.errors.map((error) => error);

    return <ASYNC_EDITOR className={`${classes.root} ${this.props.className || ''}`} value={store.code}
      onChange={this.setValue} style={this.props.style} errors={errors}/>;
  }
}


export default withStyles(styles)(Editor);
