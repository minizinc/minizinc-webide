
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from './ui/Container';

const styles = (_theme: Theme) => createStyles({

});

export interface IUIOutputProps extends IWithStore, WithStyles<typeof styles> {
}

@inject('store')
@observer
class UIOutput extends React.Component<IUIOutputProps> {

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    console.log(error, errorInfo);
  }

  static getDerivedStateFromError() {
    return null;
  }

  @bind
  private onSelectSolution(solution: number) {
    const store = this.props.store!;
    store.selectedSolution = solution;
  }

  render() {
    const store = this.props.store!;
    const definition = store.uiDefinition;

    if (definition.length === 0) {
      return <Typography color="error">no UI defined, did you call <code>constraint defineUI([...]);</code>?</Typography>;
    }

    const data = store.uiSolutionDatas;
    const solution = data.length === 1 ? data[0] : undefined;

    return <Container type="container" display="displayBlock" elements={definition} _selectedData={solution} _solutionsData={data}
      _onClickSolution={this.onSelectSolution}
      _selectedDataIndex={solution == null ? -1 : 0} _selectedSolution={store.selectedSolution}/>;
  }
}


export default withStyles(styles)(UIOutput);
