
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {EType, IEnteredValue, parseValue} from 'minizinc';
import FormControl from '@material-ui/core/FormControl';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const styles = (_theme: Theme) => createStyles({
});

export interface IValueInputProps extends IWithStore, WithStyles<typeof styles> {
  prop: IEnteredValue;
  className?: string;
}

@inject('store')
@observer
class ValueInput extends React.Component<IValueInputProps> {

  @bind
  private handleChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const prop = this.props.prop;
    prop.valueAsString = evt.target.value;

    const {error, v} = parseValue(prop, prop.valueAsString);
    prop.error = error;
    prop.value = v;
  }

  render() {
    const {prop, className} = this.props;

    let type: string = 'text';
    if (prop.dim === 0 && !prop.set && !prop.enum_type && (prop.type === EType.float || prop.type === EType.int)) {
      type = 'number';
    }

    if (prop.dim === 0 && !prop.set && prop.type === EType.bool) {
      return <FormControl margin="dense" className={className}>
          <FormControlLabel
          control={
            <Switch checked={prop.valueAsString === 'true'} onChange={this.handleChange} value="true" />
          }
          label={prop.key}
        />
      </FormControl>;
    }

    let typeInfo = `${prop.set ? 'set of ' : ''}${prop.type}${'[]'.repeat(prop.dim)}${prop.set || prop.dim > 0 ? ' (one value per line)' : ''}`;
    if (prop.enum_type) {
      typeInfo = 'enum';
    }

    return <TextField
      className={className}
      id={prop.key}
      required
      label={prop.key}
      value={prop.valueAsString}
      type={type}
      error={prop.error != null}
      multiline={prop.set || prop.dim > 0}
      rows={5}
      helperText={prop.error == null ? typeInfo : prop.error}
      onChange={this.handleChange}
      margin="dense"
      variant="outlined"
    />;
  }
}


export default withStyles(styles)(ValueInput);
