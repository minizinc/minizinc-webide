
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import Settings from '@material-ui/icons/Settings';

const styles = (_theme: Theme) => createStyles({
});

export interface IOptionsMenuProps extends IWithStore, WithStyles<typeof styles> {

}

@inject('store')
@observer
class OptionsMenu extends React.Component<IOptionsMenuProps> {
  private get store() {
    return this.props.store!;
  }

  @bind
  private handleClickListItem(event: React.MouseEvent<HTMLElement>) {
    this.store.ui.optionsMenuAnchor = event.currentTarget;
  }

  @bind
  private handleClose() {
    this.store.ui.optionsMenuAnchor = null;
  }

  @bind
  private toggleFreeSearch() {
    if (this.store.params.free_search) {
      delete this.store.params.free_search;
    } else {
      this.store.params.free_search = true;
    }
  }

  @bind
  private handleProcessesChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.currentTarget.valueAsNumber;
    if (!value) {
      delete this.store.params.processes;
    } else {
      this.store.params.processes = value;
    }
  }

  @bind
  private handleRandomSeedChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.currentTarget.valueAsNumber;
    if (!value) {
      delete this.store.params.random_seed;
    } else {
      this.store.params.random_seed = value;
    }
  }
  @bind
  private handleSolutionsClickListItem(event: React.MouseEvent<HTMLElement>) {
    const store = this.props.store!;
    store.ui.nrSolutionsChooserAnchor = event.currentTarget;
  }
  @bind
  private handleSolutionsMenuItemClick(value: number | null) {
    const store = this.props.store!;
    store.ui.nrSolutionsChooserAnchor = null;
    this.setValue(value);
  }

  private setValue(value: number | null) {
    const store = this.props.store!;
    if (value == null) {
      delete store.params.nr_solutions;
      delete store.params.all_solutions;
    } else if (value < 0) {
      delete store.params.nr_solutions;
      store.params.all_solutions = true;
    } else {
      delete store.params.all_solutions;
      store.params.nr_solutions = value;
    }
  }

  @bind
  private handleSolutionsClose() {
    const store = this.props.store!;
    store.ui.nrSolutionsChooserAnchor = null;
  }

  @bind
  private handleNrSolutionChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const value = evt.currentTarget.valueAsNumber;
    this.setValue(value);
  }

  render() {
    const store = this.store;
    const params = store.params;

    // const classes = this.props.classes;
    return <>
      <IconButton aria-owns={store.ui.optionsMenuAnchor ? 'options-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleClickListItem}
      >
        <Settings />
      </IconButton>
      { store.ui.optionsMenuAnchor && <>
      <Menu
        id="options-menu"
        anchorEl={store.ui.optionsMenuAnchor}
        open={Boolean(store.ui.optionsMenuAnchor)}
        onClose={this.handleClose}
      >
        <ListItem button aria-haspopup="true"
          aria-controls="solver-menu"
          aria-label="# Solutions"
          onClick={this.handleSolutionsClickListItem}
        >
          <ListItemText
            primary="# Solutions"
            secondary={params.all_solutions ? 'All Solutions' : (params.nr_solutions != null ? `${params.nr_solutions} Solutions` : 'Default')}
          />
        </ListItem>
        <ListItem>
          <ListItemText primary="Free Search" />
          <ListItemSecondaryAction>
            <Switch onChange={this.toggleFreeSearch}
              checked={params.free_search}
            />
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem>
          <TextField
              id="processes"
              label="# Processes"
              value={params.processes}
              type="number"
              onChange={this.handleProcessesChange}
              margin="dense"
              variant="outlined"
            />
        </ListItem>
        <ListItem>
          <TextField
              id="random_seed"
              label="Random Seed"
              value={params.random_seed}
              type="number"
              onChange={this.handleRandomSeedChange}
              margin="dense"
              variant="outlined"
            />
        </ListItem>
      </Menu>,
      <Menu
        id="solver-menu"
        anchorEl={store.ui.nrSolutionsChooserAnchor}
        open={Boolean(store.ui.nrSolutionsChooserAnchor)}
        onClose={this.handleSolutionsClose}
      >
        <MenuItem selected={!params.all_solutions && params.nr_solutions == null} onClick={() => this.handleSolutionsMenuItemClick(null)} >Default</MenuItem>
        <MenuItem selected={params.all_solutions} onClick={() => this.handleSolutionsMenuItemClick(-1)} >All Solutions</MenuItem>
        <ListItem>
          <TextField
            id="nr_solutions"
            required
            label="# Solutions"
            value={params.nr_solutions == null ? 2 : params.nr_solutions}
            type="number"
            onChange={this.handleNrSolutionChange}
            margin="dense"
            variant="outlined"
          />
        </ListItem>
      </Menu>
      </>}
    </>;
  }
}


export default withStyles(styles)(OptionsMenu);
