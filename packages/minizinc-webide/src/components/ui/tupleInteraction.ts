import {IPrimitiveValue, formatValue} from 'minizinc';
import {ApplicationStore} from '../../stores';


export interface ITupleInteraction<T> {
  tuples: [IPrimitiveValue, IPrimitiveValue, T][]; // [JOB, TASK, start]
  variable: string; // variable to store the start values in
}


export function isSelected(interaction: ITupleInteraction<any> | undefined | null, x: IPrimitiveValue, y: IPrimitiveValue) {
  if (!interaction) {
    return false;
  }
  return interaction.tuples.find((d) => d && d[0] === x && d[1] === y) != null;
}

export function toggleSelection<T>(interaction: ITupleInteraction<any> | undefined | null, store: ApplicationStore, x: IPrimitiveValue, y: IPrimitiveValue, value: T) {
  if (!interaction) {
    return;
  }

  const old = interaction.tuples.findIndex((d) => d && d[0] === x && d[1] === y);
  if (old >= 0) {
    // reset
    interaction.tuples.splice(old, 1);
  } else {
    interaction.tuples.push([x, y, value]);
  }

  updateInteractionVariable(store, interaction.variable, interaction.tuples);
}

export function updateInteractionVariable(store: ApplicationStore, variable: string, value: any) {
  const entered = store.enteredData.find((d) => d.key === variable);
  if (!entered) {
    store.interactiveData[variable] = value;
    return;
  }

  entered.value = value;
  entered.valueAsString = formatValue(entered, entered.value);
  delete store.interactiveData[variable];
}

export function getInteractionVariable<T>(store: ApplicationStore, variable: string, defaultValue: T):T {
  const entered = store.enteredData.find((d) => d.key === variable);
  if (!entered) {
    const v = store.interactiveData[variable];
    return v == null ? defaultValue : (<T><unknown>v);
  }
  return (<T><unknown>entered.value);
}
