import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import {IUIElementProps} from './interfaces';

export default class Unknown extends React.Component<IUIElementProps> {
  render() {
    return <Typography color="error">Unknown chart type: {this.props.type}</Typography>;
  }
}
