import VegaLite from './vega';
import * as React from 'react';
//import {bind} from 'decko';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import {IUIElementProps, resolveRendered} from './interfaces';

const styles = (_theme: Theme) => createStyles({

});

export interface IVegaWrapperProps extends WithStyles<typeof styles>, IUIElementProps {
  values?: any[];
  spec: object;
}

class VegaWrapper extends React.Component<IVegaWrapperProps> {
  render() {
    return resolveRendered(this.props, this.props.values!, (values) => {
      const data = {
        values
      };
      return <VegaLite data={data} spec={this.props.spec} />;
    });
  }
}

export default withStyles(styles)(VegaWrapper);
