import {IPrimitiveValue} from 'minizinc';

export function pretty(name: string) {
  const words = name.replace(/[-_\s]/gm, ' ').split(' ');

  return words.map((w) => `${w[0]!.toUpperCase()}${w.slice(1)}`).join(' ');
}

export function flat<T>(arr: T[][], rowNames: IPrimitiveValue[], colNames: IPrimitiveValue[]) {
  const flat: {v: T, x: IPrimitiveValue, y: IPrimitiveValue}[] = [];
  arr.forEach((row, i) => {
    const x = rowNames[i] || i.toString();
    row.forEach((v, j) => {
      flat.push({v, x, y: colNames[j] || j.toString()});
    });
  });
  return flat;
}

