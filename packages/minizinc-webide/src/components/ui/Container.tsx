import * as React from 'react';
import {uiElementByType} from './factories';
import {IUIElementProps} from './interfaces';
import {isEnumValue} from 'minizinc';


export interface IContainerProps extends IUIElementProps {
  display: 'displayBlock' | 'displayFlex' | 'displayFlexColumn' | 'displayInlineBlock';
  elements: any[];
}

export default class Container extends React.Component<IContainerProps> {
  render() {
    const {elements} = this.props;

    const display: string = isEnumValue(this.props.display as any) ? (this.props.display as any).e : this.props.display;

    const style: Partial<React.CSSProperties> = {};
    switch (display) {
      case 'displayBlock':
        style.display = 'block';
        break;
      case 'displayFlex':
        style.display = 'flex';
        break;
      case 'displayFlexColumn':
        style.display = 'flex';
        style.flexDirection = 'column';
        break;
      case 'displayInlineBlock':
        style.display = 'inline-block';
        break;
    }

    return <div style={style}>
      {this.renderChildren(elements)}
    </div>;
  }

  protected renderChildren(children: any[]) {
    const props = {
      _onClickSolution: this.props._onClickSolution,
      _selectedData: this.props._selectedData,
      _selectedDataIndex: this.props._selectedDataIndex,
      _selectedSolution: this.props._selectedSolution,
      _solutionsData: this.props._solutionsData
    };
    return (children || []).map((chart: any, i) => React.createElement(uiElementByType(chart.type), Object.assign({key: i}, props, chart)));
  }
}

export class PerSolutionContainer extends Container {
  protected renderChildren(children: any[]) {
    if (this.props._selectedData) {
      return super.renderChildren(children);
    }
    // multi
    const props = {
      _onClickSolution: this.props._onClickSolution,
      _solutionsData: this.props._solutionsData,
      _selectedSolution: this.props._selectedSolution,
    };
    return this.props._solutionsData.map((solution, i) =>
      <div key={i}>
        {(children || []).map((chart: any, i) => React.createElement(uiElementByType(chart.type), Object.assign({key: i, _selectedData: solution, _selectedDataIndex: i}, props, chart)))}
      </div>
    );
  }
}
