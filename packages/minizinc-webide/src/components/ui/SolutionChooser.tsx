
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../../stores/interfaces';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import {IUIElementProps} from './interfaces';


@inject('store')
@observer
export default class SolutionChooser extends React.Component<IWithStore & IUIElementProps> {
  @bind
  private handleChange(evt: React.ChangeEvent<HTMLElement>) {
    const store = this.props.store!;
    // since the li element that will be given
    const value = parseInt(evt.currentTarget.dataset.value!, 10);
    store.selectedSolution = value;
  }

  render() {
    const store = this.props.store!;
    const selected = store.selectedSolution;

    return <TextField
      label="Solution"
      value={selected}
      select
      helperText="Please select a solution"
      onChange={this.handleChange}
      margin="dense"
      variant="outlined"
    >
      <MenuItem key={-1} value={-1}>
        Select None
      </MenuItem>
      {this.props._solutionsData.map((_, i) => (
        <MenuItem key={i} value={i}>
          Solution {i + 1}
        </MenuItem>
      ))}
    </TextField>;
  }
}
