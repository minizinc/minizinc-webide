import Typography from '@material-ui/core/Typography';
import {ThemeStyle} from '@material-ui/core/styles/createTypography';
import * as React from 'react';
import {IUIElementProps, resolveRendered} from './interfaces';


export interface ITextProps extends IUIElementProps {
  data?: string;
  variant?: ThemeStyle;
}

export default class Text extends React.Component<ITextProps> {
  render() {
    return resolveRendered(this.props, this.props.data!, (data) => <Typography variant={this.props.variant}>{data}</Typography>);
  }
}


export interface IHTMLProps extends IUIElementProps {
  data?: string;
  element?: string;
}

export class HTML extends React.Component<IHTMLProps> {
  render() {
    const {element} = this.props;

    return resolveRendered(this.props, this.props.data!, (data) => <Typography>{React.createElement(element || 'div', {dangerouslySetInnerHTML: {__html: data}})}</Typography>);
 }
}

