import {setAsNamesArray, INameSet} from 'minizinc';
import * as React from 'react';
import {ResponsiveContainer, BarChart, Bar, XAxis, YAxis, Tooltip, Legend, Cell} from 'recharts';
import {IUIElementProps, resolveCompareData, resolveRendered} from './interfaces';
import {schemeCategory10} from 'd3-scale-chromatic';

export interface IBarChartProps extends IUIElementProps {
  data?: number[];
  names?: INameSet;
}


// -1 disable, -2 bar is a solution, else click on whole chart to select
function renderChart(data: {value: number, name: string}[], props: IUIElementProps, solution: number | -1 | -2 = -1, selected: boolean = false, syncId?: string) {
  const onBarClick = props._onClickSolution && solution === -2 ? (_: any, index: number) => props._onClickSolution!(index) : undefined;
  const onChartClick = props._onClickSolution && solution >= 0 ? () => props._onClickSolution!(solution) : undefined;

  return <ResponsiveContainer className={props.className} minHeight="8em">
    <BarChart data={data} onClick={onChartClick} className={onChartClick && selected ? 'selectedSolution' : undefined} syncId={syncId}>
      <Bar dataKey="value" fill="steelblue" onClick={onBarClick} strokeWidth={2} cursor={onBarClick ? 'pointer' : undefined}>
        {onBarClick && props._selectedSolution >= 0 ? data.map((_, i) => <Cell key={i} stroke={i === props._selectedSolution ? 'orange' : undefined} />) : null}
      </Bar>
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
    </BarChart>
  </ResponsiveContainer>;
}

function renderGroupedChart(groups: string[], data: any[], props: IUIElementProps, solution: number | -1 | -2 = -1, selected: boolean = false, syncId?: string) {
  const onBarClick = props._onClickSolution && solution === -2 ? (_: any, index: number) => props._onClickSolution!(index) : undefined;
  const onChartClick = props._onClickSolution && solution >= 0 ? () => props._onClickSolution!(solution) : undefined;

  return <ResponsiveContainer className={props.className} minHeight="8em">
    <BarChart data={data} onClick={onChartClick} className={onChartClick && selected ? 'selectedSolution' : undefined} syncId={syncId}>
      {groups.map((group, i) => <Bar key={group} dataKey={`v${group}`} name={group} fill={schemeCategory10[i]} onClick={onBarClick} strokeWidth={2} cursor={onBarClick ? 'pointer' : undefined}>
        {onBarClick && props._selectedSolution >= 0 ? data.map((_, i) => <Cell key={i} stroke={i === props._selectedSolution ? 'orange' : undefined} />) : null}
      </Bar>)}
      <XAxis dataKey="name" />
      <YAxis />
      <Legend />
      <Tooltip />
    </BarChart>
  </ResponsiveContainer>;
}


export default class SimpleBarChart extends React.Component<IBarChartProps> {
  render() {
    return resolveRendered(this.props, {data: this.props.data!, names: this.props.names}, (data, sol, selected) => {
      const n = data.names ? setAsNamesArray(data.names) : null;
      return renderChart(data.data.map((value, i) => ({value, name: String(n && n[i] ? n[i] : i)})), this.props, sol, selected, n ? n.join(',') : undefined);
    }, () => {
      if (!this.props.names) {
        return null;
      }
      // we can create a bar chart with dummy names
      const n = setAsNamesArray(this.props.names);
      return renderChart(n.map((name) => ({value: 0, name: String(name)})), this.props, -1, false, n.join(','));
    });
  }
}

export interface IGroupedBarChartProps extends IUIElementProps {
  data?: number[][];
  names?: INameSet;
  groupNames: string[];
}

export class GroupedBarChart extends React.Component<IGroupedBarChartProps> {
  render() {
    return resolveRendered(this.props, {data: this.props.data!, names: this.props.names}, (data, sol, selected) => {
      const n = data.names ? setAsNamesArray(data.names) : null;
      const dataLength = data.data[0]!.length;

      const barchart: any[] = [];
      for (let i = 0; i < dataLength; i++) {
        const entry: any = {name: n && n[i] ? n[i] : String(i)};
        this.props.groupNames.forEach((group, g) => {
          entry[`v${group}`] = (data.data[g] || [])[i] || 0;
        });
        barchart.push(entry);
      }
      return renderGroupedChart(this.props.groupNames, barchart, this.props, sol, selected, n ? n.join(',') : undefined);
    }, () => {
      if (!this.props.names) {
        return renderGroupedChart(this.props.groupNames, [], this.props);
      }
      // we can create a bar chart with dummy names
      const n = setAsNamesArray(this.props.names);
      const barchart: any[] = [];
      for (const name of n) {
        const entry: any = {name};
        for (const group of this.props.groupNames) {
          entry[`v${group}`] = 0;
        }
      }
      return renderGroupedChart(this.props.groupNames, barchart, this.props, -1, false, n.join(','));
    });
  }
}

export class CompareBarChart extends React.Component<IUIElementProps> {
  render() {
    const data = resolveCompareData<number>(this.props);

    return renderChart(data.map((value, i) => ({value, name: `Solution ${i + 1}`})), this.props, -2, false, 'solutions');
  }
}

export interface IGroupedCompareBarChartProps extends IUIElementProps {
  groupNames: string[];
}

export class GroupedCompareBarChart extends React.Component<IGroupedCompareBarChartProps> {
  render() {
    const groups = resolveCompareData<number[]>(this.props);

    const barchart = groups.map((row, i) => {
      const entry: any = {name: `Solution ${i + 1}`};
      this.props.groupNames.forEach((group, g) => {
        entry[`v${group}`] = row[g] || 0;
      });
      return entry;
    });
    return renderGroupedChart(this.props.groupNames, barchart, this.props, -2, false, 'solutions');
  }
}

  // return <VegaWrapper
  //   values={data.map((v, i) => ({v, name: n ? n[i] : i.toString()}))}
  //   spec={{
  //     '$schema': 'https://vega.github.io/schema/vega-lite/v3.json',
  //     mark: 'bar',
  //     encoding: {
  //       y: {
  //         field: 'name',
  //         type: 'ordinal',
  //         axis: {
  //           title: title || 'Bar Chart'
  //         }
  //       },
  //       x: {
  //         field: 'v',
  //         type: 'quantitative',
  //         axis: {
  //           title: 'Value'
  //         }
  //       }
  //     }
  //   }}
  // />;
