
import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../../stores/interfaces';
import Container, {IContainerProps} from './Container';
import Typography from '@material-ui/core/Typography';


export interface ISelectedSolutionProps extends IWithStore, IContainerProps {
  selectorsDisplay: 'displayBlock' | 'displayFlex' | 'displayFlexColumn' | 'displayInlineBlock';
  selectors: any[];
}

@inject('store')
@observer
export default class SelectedSolution extends React.Component<ISelectedSolutionProps> {
  render() {
    const store = this.props.store!;
    const selected = store.selectedSolution;

    if (selected < 0) {
      return <Typography>No solution selected</Typography>;
    }

    const solutions = this.props._solutionsData;
    if (selected >= solutions.length) {
      return <Typography color="error">Invalid solution selected</Typography>;
    }

    return <Container type="container" display={this.props.display} elements={this.props.elements}
      _selectedData={solutions[selected]!} _solutionsData={solutions}
      _selectedDataIndex={selected} _selectedSolution={selected}
    />;
  }
}
