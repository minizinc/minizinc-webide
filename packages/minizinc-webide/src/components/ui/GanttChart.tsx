import {INameSet, setAsNamesArray, setAsArray, IPrimitiveValue, ISet} from 'minizinc';
import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../../stores/interfaces';
import {ResponsiveContainer, BarChart, Bar, XAxis, YAxis, Legend, Cell} from 'recharts';
import {schemeCategory10} from 'd3-scale-chromatic';
import {ITupleInteraction, toggleSelection, isSelected} from './tupleInteraction';
import {IUIElementProps, resolveRendered} from './interfaces';


export interface IGanttChartProps extends IWithStore, IUIElementProps {
  data: {
    start?: number[][], // JOB x TASK -> start
    durations: number[][], // JOB x TASK -> duration
    assignments: IPrimitiveValue[][], // JOB x TASK -> maschine
  };
  machineNames: INameSet;
  jobNames: INameSet;
  taskNames: INameSet;

  interaction?: ITupleInteraction<number>;
}

@inject('store')
@observer
export default class GanttChart extends React.Component<IGanttChartProps> {
  private onBarClick(job: IPrimitiveValue, data: any) {
    const interaction = this.props.interaction;
    if (!interaction) {
      return;
    }
    const task = this.findTask(job, data.machine);
    const start: number = data[`job${job}`][0];
    return toggleSelection(interaction, this.props.store!, task.job, task.task, start);
  }

  private findTask(job: IPrimitiveValue, machine: any) {
    const {data, jobNames, taskNames} = this.props;

    const jobs = setAsArray(jobNames as ISet);
    const tasks = setAsNamesArray(taskNames);

    const i = jobs.indexOf(job);
    const assignments = data.assignments[i];
    const j = assignments.findIndex((d) => d === machine);

    return {
      job,
      jobIndex: i,
      task: tasks[j]!,
      taskIndex: j
    };
  }

  render() {
    const {data, jobNames, taskNames, machineNames} = this.props;

    const machines = setAsNamesArray(machineNames);
    const jobs = setAsNamesArray(jobNames);
    const tasks = setAsNamesArray(taskNames);

    return resolveRendered(this.props, data.start!, (start) => {

      // {machine: string, [key: string]: [number, number]}
      const byMachine = new Map<IPrimitiveValue, any>();

      let max = 0; // TODO global max

      jobs.forEach((job, i) => {
        const key = `job${job}`;
        const keySelected = `job${job}_s`;
        const starts = start[i];
        const durations = data.durations[i];
        const assignments = data.assignments[i];
        tasks.forEach((task, j) => {
          const machine = assignments[j];
          const bar = [starts[j], starts[j] + durations[j]];
          if (bar[1] > max) {
            max = bar[1];
          }
          if (byMachine.has(machine)) {
            byMachine.get(machine)[key] = bar;
            byMachine.get(machine)[keySelected] = isSelected(this.props.interaction, job, task);
          } else {
            byMachine.set(machine, {machine, [key]: bar, [keySelected]: isSelected(this.props.interaction, job, task)});
          }
        });
      });

      const barData = machines.map((machine) => byMachine.get(machine) || {machine});

      return <ResponsiveContainer className={this.props.className} minHeight="14em">
        <BarChart data={barData} layout="vertical">
          <YAxis label="Machine" dataKey="machine" />
          <XAxis label="Time" domain={[0, max]} type="number" />
          {jobs.map((job, i) => <Bar key={String(job)} name={`Job ${job}`} dataKey={`job${job}`} fill={schemeCategory10[i]} onClick={this.props.interaction ? this.onBarClick.bind(this, job) : undefined}>
            {barData.map((m, j) => <Cell key={j} stroke={m[`job${job}_s`] ? 'orange' : undefined} strokeWidth={2} />)}
          </Bar>)
          }
          <Legend />
        </BarChart>
      </ResponsiveContainer>;
    });
  }
}
