
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../../stores/interfaces';
import TextField from '@material-ui/core/TextField';
import {updateInteractionVariable, getInteractionVariable} from './tupleInteraction';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Slider from '@material-ui/lab/Slider';
import {IEnumValue, isEnumValue, INameSet, setAsNamesArray, isSetRange, createEnumValue} from 'minizinc';


export interface IInputProps extends IWithStore {
  type: 'input' | 'inputInt' | 'inputFloat';
  value: string | number;
  variable: string;
  label: string;

  className?: string;
}

@inject('store')
@observer
export default class Input extends React.Component<IInputProps> {

  @bind
  private handleChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const {variable, type} = this.props;
    const store = this.props.store!;

    const input = evt.currentTarget;
    const value = (type === 'input' ? input.value : input.valueAsNumber);
    updateInteractionVariable(store, variable, value);
  }

  render() {
    const {type, value, variable, label, className} = this.props;

    return <TextField
      className={className}
      id={variable}
      required
      label={label}
      value={value}
      type={type === 'input' ? 'text' : 'number'}
      onChange={this.handleChange}
      margin="dense"
      variant="outlined"
    />;
  }
}


export interface IInputBoolProps extends IWithStore {
  value: boolean;
  variable: string;
  label: string;

  className?: string;
}

@inject('store')
@observer
export class InputBool extends React.Component<IInputBoolProps> {

  @bind
  private handleChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const {variable} = this.props;
    const store = this.props.store!;

    const input = evt.currentTarget;
    const value = input.checked;
    updateInteractionVariable(store, variable, value);
  }

  render() {
    const {value, variable, label, className} = this.props;

    return <FormControl margin="dense" className={className}>
      <FormControlLabel
        control={
          <Switch checked={value} onChange={this.handleChange} id={variable} />
        }
        label={label}
        />
    </FormControl>;
  }
}


export interface IInputEnumProps extends IWithStore {
  value: IEnumValue | number;
  variable: string;
  label: string;
  domain: INameSet;

  className?: string;
}

@inject('store')
@observer
export class InputEnum extends React.Component<IInputEnumProps> {

  @bind
  private handleChange(evt: React.ChangeEvent<HTMLElement>) {
    const {variable, domain} = this.props;
    const store = this.props.store!;

    const input = evt.currentTarget;
    const value = input.dataset.value!;
    const vs = !isSetRange(domain) && isEnumValue(domain.set[0]) ? createEnumValue(value) : parseInt(value, 10);
    updateInteractionVariable(store, variable, vs);
  }

  @bind
  private handleSliderChange(_evt: any, value: number | number[]) {
    const {variable} = this.props;
    const store = this.props.store!;
    updateInteractionVariable(store, variable, value as number);
  }

  render() {
    const {variable, label, domain, className} = this.props;
    const value = getInteractionVariable(this.props.store!, variable, this.props.value);

    if (isSetRange(domain)) {
      return <FormControl margin="dense" className={className}>
      <FormControlLabel
        control={
          <Slider style={{width: '300px', padding: '0 10px'}}
            value={value as number}
            min={domain.set[0][0]}
            max={domain.set[0][1]}
            step={1}
            onChange={this.handleSliderChange}
          />
        }
        label={label}
        />
      </FormControl>;
    }
    const values = setAsNamesArray(domain);

    return <TextField
      className={className}
      id={variable}
      required
      label={label}
      value={isEnumValue(value) ? value.e : value}
      select
      onChange={this.handleChange}
      margin="dense"
      variant="outlined"
    >
      {values.map((value) => <MenuItem key={value} value={value}>{value}</MenuItem>)}
    </TextField>;
  }
}

