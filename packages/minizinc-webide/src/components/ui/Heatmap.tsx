import {INameSet, setAsNamesArray} from 'minizinc';
import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {ResponsiveContainer, ScatterChart, Scatter, XAxis, YAxis, Cell, Tooltip, ZAxis} from 'recharts';
import {ITupleInteraction, toggleSelection, isSelected} from './tupleInteraction';
import {IWithStore} from '../../stores/interfaces';
import {flat} from './utils';
import {scaleLinear} from 'd3-scale';
import {transpose} from 'd3-array';
import {schemeBlues} from 'd3-scale-chromatic';
import {bind} from 'decko';
import {IUIElementProps, resolvedRenderedData, resolveRendered} from './interfaces';


export interface IHeatmapProps extends IWithStore, IUIElementProps {
  data?: number[][];
  x?: INameSet;
  y?: INameSet;

  interaction?: ITupleInteraction<number>;
}

@inject('store')
@observer
export default class Heatmap extends React.Component<IHeatmapProps> {
  @bind
  private onClick(data: any) {
    data = data.payload;
    return toggleSelection(this.props.interaction, this.props.store!, data.x, data.y, data.v);
  }

  render() {

    const defaultValue = {
      x: this.props.x!,
      y: this.props.y!,
      data: this.props.data!
    };

    const allValues = resolvedRenderedData(this.props, defaultValue);

    const rows = setAsNamesArray(allValues[0]!.x);
    const cols = setAsNamesArray(allValues[0]!.y);

    let max = Number.NEGATIVE_INFINITY;
    for (const vs of allValues) {
      for (const v of vs.data) {
        for (const vi of v) {
          if (vi > max) {
            max = vi;
          }
        }
      }
    }
    const color = scaleLinear<string>().domain([0, max]).range(schemeBlues[3]);

    return resolveRendered(this.props, defaultValue, (data) => {
      const flatData = flat(transpose<number>(data.data), cols, rows);
      return <ResponsiveContainer className={this.props.className} minHeight="8em" minWidth="8em">
        <ScatterChart data={flatData}>
          <Scatter shape="square" onClick={this.onClick}>
            {flatData.map((d, i) => <Cell key={i} fill={color(d.v)} stroke={isSelected(this.props.interaction, d.x, d.y) ? 'orange' : undefined} strokeWidth={2} />)}
          </Scatter>
          <XAxis dataKey="x" type="category" allowDuplicatedCategory={false} />
          {/* wrong typings */}
          {React.createElement(YAxis, {dataKey: 'y', type: 'category', allowDuplicatedCategory: false} as any)}
          <ZAxis dataKey="v" range={[1000, 1000]} />
          <Tooltip />
        </ScatterChart>
      </ResponsiveContainer>;
    });
  }
}
