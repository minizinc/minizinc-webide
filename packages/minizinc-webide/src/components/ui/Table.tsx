import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import * as React from 'react';
import {resolveRendered, IUIElementProps} from './interfaces';


export interface ITableProps extends IUIElementProps {
  data?: any[];
}

export default class TableChart extends React.Component<ITableProps> {
  render() {
    return resolveRendered(this.props, this.props.data!, (data) => {
      return <Table className={this.props.className}>
        <TableBody>
          {Object.entries(data).map(([k, v]) => <TableRow key={k}>
            <TableCell>{k}</TableCell>
            <TableCell><pre>{JSON.stringify(v)}</pre></TableCell>
          </TableRow>)}
        </TableBody>
      </Table>;
    });
  }
}
