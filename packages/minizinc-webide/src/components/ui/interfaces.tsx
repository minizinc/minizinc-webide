import Typography from '@material-ui/core/Typography';
import * as React from 'react';

export interface IUIElementProps {
  type: string;
  dataRef?: string;

  className?: string;

  _selectedData?: any;
  _selectedDataIndex?: number;
  _selectedSolution: number;
  _solutionsData: any[];
  _onClickSolution?: (index: number) => void;

  [key: string]: any;
}

function mergeSmart<T>(a: T, b: T): T {
  if (typeof a === 'object' && !Array.isArray(a)) {
    return Object.assign({}, a, b);
  }
  return b == null ? a : b;
}

export function resolvedRenderedData<T>(props: IUIElementProps, defaultData: T): T[] {
  if (!props.dataRef) {
    return [defaultData];
  }
  if (props._selectedData) {
    const data = props._selectedData[props.dataRef];
    if (!data) {
      return [defaultData];
    }
    return [mergeSmart(defaultData, data)];
  }
  const datas = props._solutionsData.map((solution) => solution[props.dataRef!]).filter((d) => d != null);
  return datas.map((d) => mergeSmart(defaultData, d));
}

export function resolveCompareData<T>(props: IUIElementProps): T[] {
  return props._solutionsData.map((solution) => solution[props.dataRef!]).filter((d) => d != null);
}

export function resolveRendered<T>(props: IUIElementProps, defaultData: T, factory: (data: T, solution: number | -1, selected: boolean) => React.ReactElement, noSolution?: () => React.ReactElement | null) {
  if (!props.dataRef) {
    return factory(defaultData, -1, false);
  }

  if (props._selectedData) {
    // single solution case
    const data = props._selectedData[props.dataRef];
    if (!data) {
      return <Typography color="error">reference {props.dataRef} not found in solution data</Typography>;
    }
    return factory(mergeSmart(defaultData, data), props._selectedDataIndex == null ? -1 : props._selectedDataIndex, props._selectedDataIndex === props._selectedSolution);
  }
  // multi solution case
  if (props._solutionsData.length === 0) {
    const generated = noSolution ? noSolution() : null;
    return generated || <Typography>no solution to show</Typography>;
  }

  return <div>
    {props._solutionsData.map((solution, i) => {
      const data = solution[props.dataRef!];
      if (!data) {
        return <Typography key={i} color="error">reference {props.dataRef} not found in solution {i} data</Typography>;
      }
      return factory(mergeSmart(defaultData, data), i, i === props._selectedSolution);
    })}
  </div>;
}
