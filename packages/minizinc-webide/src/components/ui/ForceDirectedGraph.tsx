import {forceCenter, forceLink, forceManyBody, forceSimulation, SimulationLinkDatum, SimulationNodeDatum} from 'd3-force';
import {IPrimitiveValue, INameSet, setAsNamesArray} from 'minizinc';
import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {IWithStore} from '../../stores/interfaces';
import {ITupleInteraction, isSelected, toggleSelection} from './tupleInteraction';
import {debounce} from 'decko';
import {scaleLinear} from 'd3-scale';
import {max} from 'd3-array';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import classNames from 'classnames';
import {IUIElementProps, resolveRendered} from './interfaces';

const styles = (_theme: Theme) => createStyles({
  node: {
    fill: 'lightgray'
  },
  nodeText: {
    textAnchor: 'middle'
  },
  link: {
    stroke: 'black',
    cursor: 'pointer',
    strokeOpacity: 0.2
  },
  linkSelected: {
    stroke: 'orange',
    strokeWidth: 2,
    strokeOpacity: 1
  },
  linkClicked: {
    stroke: 'blue',
    strokeWidth: 2,
    strokeOpacity: 1
  }
});

interface INode extends SimulationNodeDatum {
  id: string;
  name: IPrimitiveValue;
}

interface ILink extends SimulationLinkDatum<INode> {
  distance: number;
}

interface IAForceDirectedWrapperProps extends WithStyles<typeof styles>, IWithStore {
  nodeNames: IPrimitiveValue[];
  path: IPrimitiveValue[];
  interaction?: ITupleInteraction<number>;

  width?: number;
  height?: number;
}

interface IForceDirectedWrapperState {
  nodes: INode[];
  links: ILink[];
}

abstract class AForceDirectedWrapper<T extends IAForceDirectedWrapperProps> extends React.Component<T, IForceDirectedWrapperState> {
  private readonly link = forceLink<INode, ILink>();
  private readonly forceCenter = forceCenter();
  private readonly sim = forceSimulation<INode, ILink>()
    .force('charge', forceManyBody())
    .force('link', this.link)
    .force('center', this.forceCenter)
    .on('tick', () => this.updateMe());

  @debounce(50)
  private updateMe() {
    this.forceUpdate();
  }

  private get dim() {
    return {width: this.props.width as number || 300, height: this.props.height as number || 300};
  }

  componentDidMount() {
    const dim = this.dim;
    this.forceCenter.x(dim.width / 2).y(dim.height / 2);
    // scale link distance to normalize
    const scale = scaleLinear().domain([0, max(this.state.links.map((d) => d.distance))!]).range([0, Math.min(dim.width, dim.height) * 0.7]);
    this.link.links(this.state.links).distance((d) => scale(d.distance));
    this.sim.nodes(this.state.nodes).restart();
  }

  componentWillUnmount() {
    this.sim.stop();
  }

  render() {
    const {nodes, links} = this.state;
    const {classes, interaction} = this.props;

    const pathPairs = new Set(this.props.path.slice(1).map((t, i) => `${this.props.path[i]}->${t}`));
    const node = (d: number | INode | string) => typeof d === 'number' ? nodes[d] : (d as INode);
    const dim = this.dim;
    return <svg width={dim.width} height={dim.height}>
      {links.map((d) => {
        const s = node(d.source);
        const t = node(d.target);
        const key=`${s.name}->${t.name}`;
        return <line
          className={classNames(classes.link, {[classes.linkSelected]: pathPairs.has(key), [classes.linkClicked]: isSelected(interaction, s.name, t.name)})}
          key={key} x1={s.x} y1={s.y} x2={t.x} y2={t.y}
          onClick={() => toggleSelection(interaction, this.props.store!, s.name, t.name, 1)} />;
      })}
      {nodes.map((d) => <g key={`node${d.id}`} transform={`translate(${d.x}, ${d.y})`}>
        <circle r={5} className={classes.node}></circle>
        <text className={classes.nodeText} dy={5}>{d.id}</text>
      </g>)}
    </svg>;
  }
}

interface IForceDirectedDistanceMatrixWrapperProps extends IAForceDirectedWrapperProps {
  distances: number[][];
}

class ForceDirectedDistanceMatrixWrapper extends AForceDirectedWrapper<IForceDirectedDistanceMatrixWrapperProps> {
  constructor(props: IForceDirectedDistanceMatrixWrapperProps) {
    super(props);

    this.state = this.convert(props);
  }

  private convert(props: IForceDirectedDistanceMatrixWrapperProps): IForceDirectedWrapperState {
    const nodes = props.nodeNames.map((d) => ({id: d.toString(), name: d}));
    const links: ILink[] = [];
    const l = nodes.length;
    for (let i = 0; i < l; i++) {
      for (let j = 0; j < l; j++) {
        const distance = props.distances[i][j];
        if (distance != null && distance > 0) {
          links.push({source: nodes[i], target: nodes[j], distance});
        }
      }
    }
    return {nodes, links};
  }
}


interface IForceDirectedWrapperProps extends IAForceDirectedWrapperProps {
  edges: [IPrimitiveValue, IPrimitiveValue][];
  distances: number[];
}

class ForceDirectedWrapper extends AForceDirectedWrapper<IForceDirectedWrapperProps> {
  constructor(props: IForceDirectedWrapperProps) {
    super(props);

    this.state = this.convert(props);
  }

  private convert(props: IForceDirectedWrapperProps): IForceDirectedWrapperState {
    const nodes = props.nodeNames.map((d) => ({id: d.toString(), name: d}));
    const nodeLookup = new Map(nodes.map((n) => [n.name, n] as [IPrimitiveValue, INode]));
    const links: ILink[] = [];
    props.edges.forEach((edge, i) => {
      const distance = props.distances[i] || 1;
      links.push({source: nodeLookup.get(edge[0])!, target: nodeLookup.get(edge[1])!, distance});
    });
    return {nodes, links};
  }
}

const DISTANCE_WRAPPER = withStyles(styles)(ForceDirectedDistanceMatrixWrapper);

const EDGE_WRAPPER = withStyles(styles)(ForceDirectedWrapper);

export interface IForceDirectedGraphDistanceMatrixProps extends IWithStore, IUIElementProps {
  data: {
    distanceMatrix: number[][],
    path?: IPrimitiveValue[];
  };
  nodeNames: INameSet;

  interaction?: ITupleInteraction<number>;
}

@inject('store')
@observer
export class ForceDirectedDistanceMatrixGraph extends React.Component<IForceDirectedGraphDistanceMatrixProps> {

  render() {
    const {data, nodeNames} = this.props;

    return resolveRendered(this.props, {path: data.path!}, (sub) => {
      return <DISTANCE_WRAPPER nodeNames={setAsNamesArray(nodeNames)} distances={data.distanceMatrix} path={sub.path} interaction={this.props.interaction} store={this.props.store!} />;
    });
  }
}


export interface IForceDirectedGraphProps extends IWithStore, IUIElementProps {
  data: {
    edges: [IPrimitiveValue, IPrimitiveValue][];
    distances: number[],
    path?: IPrimitiveValue[];
  };
  nodeNames: INameSet;

  interaction?: ITupleInteraction<number>;
}

@inject('store')
@observer
export default class ForceDirectedGraph extends React.Component<IForceDirectedGraphProps> {


  render() {
    const {data, nodeNames} = this.props;

    return resolveRendered(this.props, {path: data.path!}, (sub) => {
      return <EDGE_WRAPPER nodeNames={setAsNamesArray(nodeNames)} edges={data.edges} distances={data.distances} path={sub.path} interaction={this.props.interaction} store={this.props.store!} />;
    });
  }
}

