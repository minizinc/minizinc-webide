import {INameSet, setAsNamesArray, IPrimitiveValue} from 'minizinc';
import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {ITupleInteraction, toggleSelection} from './tupleInteraction';
import {IWithStore} from '../../stores/interfaces';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import {resolveRendered, IUIElementProps} from './interfaces';


export interface IMatrixProps extends IWithStore, IUIElementProps {
  data?: IPrimitiveValue[][];
  x?: INameSet;
  y?: INameSet;

  interaction?: ITupleInteraction<IPrimitiveValue>;
}

@inject('store')
@observer
export default class Matrix extends React.Component<IMatrixProps> {
  render() {
    return resolveRendered(this.props, {data: this.props.data!, x: this.props.x!, y: this.props.y!}, (data) => {
      const rows = setAsNamesArray(data.x);
      const cols = setAsNamesArray(data.y);

      return <Table className={this.props.className}>
        <TableHead>
          <TableCell></TableCell>
          {cols.map((v, j) => <TableCell key={j}>{v}</TableCell>)}
        </TableHead>
        <TableBody>
          {data.data.map((row, i) => <TableRow key={i}>
            <TableCell>{rows[i]}</TableCell>
            {row.map((v, j) => <TableCell key={j} onClick={() => toggleSelection(this.props.interaction, this.props.store!, rows[i], cols[j], v)}>{v}</TableCell>)}
          </TableRow>)}
        </TableBody>
      </Table>;
    });
  }
}
