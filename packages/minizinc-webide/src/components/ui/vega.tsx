import {asyncComponent} from 'react-async-component';

export interface IVegaProps {
  spec: object;
  data: {values: any[]};
  renderer?: string;
  autoResize?: boolean;
}

export default asyncComponent<IVegaProps>({
  resolve: () => import('react-vega-lite')
});
