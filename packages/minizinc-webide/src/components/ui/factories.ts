import BarChart, {CompareBarChart, GroupedBarChart, GroupedCompareBarChart} from './BarChart';
import Unknown from './Unknown';
import GanttChart from './GanttChart';
import Heatmap from './Heatmap';
import Matrix from './Matrix';
import Table from './Table';
import Text, {HTML} from './Text';
import VegaWrapper from './VegaWrapper';
import ForceDirectedGraph, {ForceDirectedDistanceMatrixGraph} from './ForceDirectedGraph';
import Container, {PerSolutionContainer} from './Container';
import Input, {InputEnum, InputBool} from './Input';
import SelectedSolution from './SelectedSolution';
import SolutionChooser from './SolutionChooser';
import {IUIElementProps} from './interfaces';

export function uiElementByType(type?: string): React.ElementType<IUIElementProps> {
  const UI_REACT_FACTORIES = {
    barchart: BarChart,
    ganttchart: GanttChart,
    heatmap: Heatmap,
    matrix: Matrix,
    table: Table,
    text: Text,
    html: HTML,
    forceDirected: ForceDirectedGraph,
    forceDirectedDistanceMatrix: ForceDirectedDistanceMatrixGraph,
    unknown: Unknown,
    vegaLite: VegaWrapper,
    container: Container,
    perSolution: PerSolutionContainer,
    input: Input,
    inputInt: Input,
    inputFloat: Input,
    inputBool: InputBool,
    inputEnum: InputEnum,
    compareBarchart: CompareBarChart,
    groupedBarchart: GroupedBarChart,
    groupedCompareBarchart: GroupedCompareBarChart,
    selectedSolution: SelectedSolution,
    solutionChooser: SolutionChooser
  };
  return (<any>UI_REACT_FACTORIES)[type || 'unknown'] || Unknown;
}

