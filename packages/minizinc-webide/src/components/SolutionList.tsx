
import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import Solution from './Solution';
import {Typography} from '@material-ui/core';


@inject('store')
@observer
export default class Solutions extends React.Component<IWithStore> {
  render() {
    const store = this.props.store!;
    const result = store.result;
    if (!result) {
      return null;
    }

    const subset = store.ui.solutionsUpTo && store.ui.solutionsUpTo < result.solutions.length;
    const solutions = subset ? result.solutions.slice(0, store.ui.solutionsUpTo) : result.solutions;
    return <>
      {result.extraHeader ? <Typography>{result.extraHeader}</Typography> : null}
      {solutions.map((solution, i) => <Solution solution={solution} key={i} index={i} />)}
    </>;
  }
}
