
import * as React from 'react';
//import {bind} from 'decko';
import {observer} from 'mobx-react';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import {ITypeInfo, IValue, ISet} from 'minizinc';


const styles = (_theme: Theme) => createStyles({
});

export interface IValueOutputProps extends WithStyles<typeof styles> {
  prop: ITypeInfo;
  value: IValue;
}

@observer
class ValueOutput extends React.Component<IValueOutputProps> {

  render() {
    const {prop, value} = this.props;

    if (value == null) {
      return 'NULL';
    }

    if (prop.set) {
      const sset = value as ISet;
      return `(${sset.set.join(', ')})`;
    }

    if (prop.dim === 0) {
      return String(value);
    }

    if (prop.dim === 1) {
      return (value as any[]).join(', ');
    }

    if (prop.dim === 2) {
      const vv = value as any[][];
      const strings = vv.map((row) => row.map(String));
      const longest = vv.reduce((a, b) => b.reduce((d, v) => Math.max(d, v.length), a), 0);
      return <pre>{strings.map((row) => row.map((v) => v.padStart(longest)).join(' ')).join('\n')}</pre>;
    }

    return JSON.stringify(value, null, ' ');
  }
}


export default withStyles(styles)(ValueOutput);
