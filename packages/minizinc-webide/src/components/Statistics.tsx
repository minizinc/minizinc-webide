import {createStyles, Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {IWithStore} from '../stores/interfaces';

const styles = (_theme: Theme) => createStyles({

});

export interface IStatisticsProps extends IWithStore, WithStyles<typeof styles> {
}

@inject('store')
@observer
class Statistics extends React.Component<IStatisticsProps> {
  render() {
    const store = this.props.store!;
    if (!store.result || !store.result.statistics) {
      return <Typography>No Statistics available</Typography>;
    }

    return <Table>
      <TableHead>
        <TableRow>
          <TableCell>Key</TableCell>
          <TableCell>Value</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {Object.entries(store.result.statistics).map(([k, v]) => <TableRow key={k}>
          <TableCell>{k}</TableCell>
          <TableCell>{v}</TableCell>
        </TableRow>)}
      </TableBody>
    </Table>;
  }
}

export default withStyles(styles)(Statistics);
