import * as React from 'react';
//import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Error from '@material-ui/icons/Error';


const styles = (theme: Theme) => createStyles({
  error: {
    backgroundColor: theme.palette.error.main
  }
});

export interface IErrorListProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
}

@inject('store')
@observer
class ErrorList extends React.Component<IErrorListProps> {
  render() {
    const store = this.props.store!;
    const classes = this.props.classes;
    return <List className={this.props.className}>
      {store.errors.map((error, i) =>
        <ListItem key={i}>
        <Avatar className={classes.error}>
          <Error />
        </Avatar>
        <ListItemText primary={error.type} secondary={error.message} />
      </ListItem>
      )}
  </List>;
  }
}

export default withStyles(styles)(ErrorList);
