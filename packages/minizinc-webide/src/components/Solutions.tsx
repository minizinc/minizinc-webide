
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import SolutionList from './SolutionList';
import UIOutput from './UIOutput';
import IconButton from '@material-ui/core/IconButton';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Button from '@material-ui/core/Button';
import Statistics from './Statistics';

const styles = (_theme: Theme) => createStyles({

});

export interface ISolutionsProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
  style?: React.CSSProperties;
}

@inject('store')
@observer
class Solutions extends React.Component<ISolutionsProps> {
  @bind
  private onSolve() {
    const store = this.props.store!;
    store.solve();
  }

  @bind
  private changeTab(_: any, value: number) {
    const store = this.props.store!;
    store.ui.solutionTab = value;
  }

  @bind
  private resetInteractiveValues() {
    const store = this.props.store!;
    store.interactiveData = {};
  }

  render() {
    const store = this.props.store!;

    const solve = <IconButton onClick={this.onSolve} color="inherit" title="Solve" disabled={store.running || !store.validInput}>
      <PlayArrow/>
    </IconButton>;


    const content = () => {
      return <>
        <Tabs value={store.ui.solutionTab} onChange={this.changeTab}>
          <Tab label="List" />
          <Tab label="UI" />
          <Tab label="Statistics" />
        </Tabs>
        {store.ui.solutionTab === 0 && <SolutionList />}
        {store.ui.solutionTab === 1 && <UIOutput />}
        {store.ui.solutionTab === 2 && <Statistics />}
      </>;
    };

    return <Paper className={this.props.className} style={this.props.style}>
      <Typography variant="h6" color="inherit">
        Solutions {Object.keys(store.interactiveData).length > 0 && <Button onClick={this.resetInteractiveValues}>Reset Interactive Values</Button>}
      </Typography>
      {!store.result ? solve : content()}
    </Paper>;
  }
}


export default withStyles(styles)(Solutions);
