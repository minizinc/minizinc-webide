
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';

import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Warning from '@material-ui/icons/Warning';
import ArrowForward from '@material-ui/icons/ArrowForward';
import Popover from '@material-ui/core/Popover';
import IconButton from '@material-ui/core/IconButton';
import Cancel from '@material-ui/icons/Cancel';
import {EStatus} from 'minizinc';
import ErrorList from './ErrorList';


const styles = (theme: Theme) => createStyles({
  error: {
    color: theme.palette.error.main
  },
  good: {
    color: theme.palette.primary.main
  },
  warning: {
    color: theme.palette.secondary.main
  },
  abort: {
    position: 'relative',
    left: '-2.7rem',
  }
});

export interface IResultInfoProps extends IWithStore, WithStyles<typeof styles> {

}

function niceName(status: EStatus) {
  switch (status) {
    case EStatus.ALL_SOLUTIONS:
      return 'All Solutions Found';
    case EStatus.ERROR:
      return 'Error occurred';
    case EStatus.OPTIMAL_SOLUTION:
      return 'Optimal Solution Found';
    default:
      return `${status.toString()[0]}${status.toString().slice(1).toLowerCase()}`;
  }
}

@inject('store')
@observer
class ResultInfo extends React.Component<IResultInfoProps> {
  @bind
  private openErrorList(evt: React.MouseEvent<HTMLElement>) {
    const store = this.props.store!.ui;
    store.visibleErrorAnchors = evt.currentTarget;
  }

  @bind
  private closeErrorList() {
    const store = this.props.store!.ui;
    store.visibleErrorAnchors = null;
  }

  @bind
  private onAbort() {
    const store = this.props.store!;
    store.abort();
  }

  render() {
    const store = this.props.store!;
    const classes = this.props.classes;

    if (!store.modelMetaData) {
      return <Typography variant="h6" color="inherit">
        Compile first <ArrowForward/>
      </Typography>;
    }

    if (store.running) {
      return <>
        <CircularProgress />
        <IconButton onClick={this.onAbort} className={classes.abort} color="inherit" title="Cancel" disabled={!store.canAbort}>
          <Cancel/>
        </IconButton>
      </>;
    }
    if (store.errors.length > 0) {
      return <>
        <Badge badgeContent={store.errors.length} color="error" onClick={this.openErrorList}>
          <Typography variant="h6" color="inherit" className={classes.error} onClick={this.openErrorList}>
            Errors found
          </Typography>
        </Badge>

        <Popover anchorEl={store.ui.visibleErrorAnchors}
          open={Boolean(store.ui.visibleErrorAnchors)}
          onClose={this.closeErrorList}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}>
          <ErrorList />
        </Popover>
      </>;
    }
    if (!store.validInput && store.modelMetaData) {
      return <Typography variant="h6" color="inherit" className={classes.warning}>
        <Warning/> Missing Input
      </Typography>;
    }
    if (store.result) {
      const result = store.result!;
      return <Badge badgeContent={result.solutions.length} color="primary">
        <Typography variant="h6" color="inherit" className={result.status === EStatus.ERROR || result.status === EStatus.UNSATISFIABLE ? classes.error : classes.good}>
          {niceName(result.status)}
        </Typography>
      </Badge>;
    }
    return null;
  }
}


export default withStyles(styles)(ResultInfo);
