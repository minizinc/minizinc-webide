
import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

const styles = (_theme: Theme) => createStyles({
  root: {

  }
});

export interface ISolverChooserProps extends IWithStore, WithStyles<typeof styles> {

}

@inject('store')
@observer
class SolverChooser extends React.Component<ISolverChooserProps> {
  @bind
  private handleClickListItem(event: React.MouseEvent<HTMLElement>) {
    const store = this.props.store!;
    store.ui.solverChooserAnchor = event.currentTarget;
  }

  @bind
  private handleMenuItemClick(solver: string) {
    const store = this.props.store!;
    store.ui.solverChooserAnchor = null;
    store.params.solver = solver;
  }

  @bind
  private handleClose() {
    const store = this.props.store!;
    store.ui.solverChooserAnchor = null;
  }

  render() {
    const store = this.props.store!;
    // const classes = this.props.classes;

    return  <>
      <List dense disablePadding>
        <ListItem
          button
          aria-haspopup="true"
          aria-controls="solver-menu"
          aria-label="Selected Solver"
          onClick={this.handleClickListItem}
        >
          <ListItemText
            primary="Selected Solver"
            secondary={store.params.solver ? store.solvers.find((s) => s.id === store.params.solver)!.name : 'Default'}
          />
        </ListItem>
      </List>
      <Menu
        id="solver-menu"
        anchorEl={store.ui.solverChooserAnchor}
        open={Boolean(store.ui.solverChooserAnchor)}
        onClose={this.handleClose}
      >
        {store.solvers.map((solver) => (
          <MenuItem
            key={solver.id}
            selected={solver.id === store.params.solver}
            onClick={() => this.handleMenuItemClick(solver.id)}
          >
            {solver.name}
          </MenuItem>
        ))}
      </Menu>
    </>;
  }
}


export default withStyles(styles)(SolverChooser);
