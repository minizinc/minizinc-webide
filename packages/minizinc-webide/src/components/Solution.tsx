import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import {ISolution} from 'minizinc';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Typography from '@material-ui/core/Typography';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ValueOutput from './ValueOutput';

const styles = (_theme: Theme) => createStyles({
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  charts: {
    display: 'flex',
    flexDirection: 'column'
  }
});

export interface ISolutionProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
  solution: ISolution;
  index: number;
}

@inject('store')
@observer
class Solution extends React.Component<ISolutionProps> {
  @bind
  private onChange() {
    const store = this.props.store!;
    const index = this.props.index;
    store.selectedSolution = store.selectedSolution === index ? -1 : index;
  }

  render() {
    const store = this.props.store!;
    const classes = this.props.classes;
    const {index, solution} = this.props;

    const renderImpl = () => {
      const validEntries = store.result!.outputs.filter((d) => solution.assignments[d.key] != null);
      return <>
        {validEntries.length < store.result!.outputs.length ? <Typography color="error">not all outputs could be parsed, did you call <code>output outputJSON();</code>?</Typography> : null}
        <Table>
          <TableBody>
            {validEntries.map((output) => <TableRow key={output.key}>
              <TableCell>{output.key}</TableCell>
              <TableCell>
                <ValueOutput prop={output} value={solution.assignments[output.key]} />
              </TableCell>
            </TableRow>)}
          </TableBody>
        </Table>
        {solution.extraOutput ? <pre>{solution.extraOutput}</pre> : null}
      </>;
    };

    return <ExpansionPanel expanded={index === store.selectedSolution} onChange={this.onChange}>
    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
      <Typography>Solution {index + 1} {solution.objective ? `(${solution.objective})` : ''}</Typography>
    </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.container}>
        {index === store.selectedSolution && renderImpl()}
    </ExpansionPanelDetails>
  </ExpansionPanel>;
  }
}

export default withStyles(styles)(Solution);
