
import * as React from 'react';
import {bind, debounce} from 'decko';
import {editor, initEditor, error2decoration} from '../mzn/editor';
import {IError} from 'minizinc';

initEditor('./editor.worker.js');

export interface IEditorWrapperProps {
  className?: string;
  style?: React.CSSProperties;
  value: string;
  errors: IError[];
  onChange(value: string): void;
}

export default class EditorWrapper extends React.Component<IEditorWrapperProps> {
  private elem: HTMLElement | null = null;
  private editor: editor.IStandaloneCodeEditor | null = null;
  private ignoreEvent = false;
  private lastValue = '';
  private activeErrors: string[] = [];

  @bind
  private assignRef(elem: HTMLElement | null) {
    if (elem === this.elem) {
      return;
    }

    this.elem = elem;
    if (!this.elem) {
      if (this.editor) {
        this.editor.dispose();
        this.editor = null;
      }
      return;
    }
    this.editor = editor.create(this.elem!, {
      value: this.lastValue = this.props.value,
      language: 'minizinc',
      glyphMargin: true,
      minimap: {
        enabled: false
      }
    });
    this.editor.onDidChangeModelContent(() => {
      if (this.ignoreEvent) {
        return;
      }
      this.triggerChange();
    });
    this.renderErrors();
  }

  @debounce(100)
  private triggerChange() {
    this.props.onChange(this.lastValue = this.editor!.getValue());
  }

  @bind
  private updateDimensions() {
    if (this.editor) {
      this.editor.layout();
    }
  }

  private renderErrors() {
    if (!this.editor) {
      return;
    }

    const errors = this.props.errors.map(error2decoration);

    this.activeErrors = this.editor.deltaDecorations(this.activeErrors, errors);
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    if (this.editor) {
      this.editor.dispose();
      this.editor = null;
    }
    window.removeEventListener('resize', this.updateDimensions);
  }

  render() {
    const value = this.props.value;
    if (value !== this.lastValue) {
      this.lastValue = value;
      if (this.editor) {
        console.log('set editor content');
        this.ignoreEvent = true;
        this.editor.setValue(value);
        this.ignoreEvent = false;
      }
    }
    this.renderErrors();
    return <div className={this.props.className} ref={this.assignRef} style={this.props.style}/>;
  }
}
