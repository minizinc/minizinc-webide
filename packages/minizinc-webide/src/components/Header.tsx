import * as React from 'react';
import {bind} from 'decko';
import {observer, inject} from 'mobx-react';
import {IWithStore} from '../stores/interfaces';
import ResultInfo from './ResultInfo';

import {withStyles, createStyles, Theme, WithStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Switch from '@material-ui/core/Switch';
import Menu from '@material-ui/icons/Menu';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Info from '@material-ui/icons/Info';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Sync from '@material-ui/icons/Sync';
import SyncDisabled from '@material-ui/icons/SyncDisabled';
import Save from '@material-ui/icons/Save';
import Create from '@material-ui/icons/Create';
import FolderOpen from '@material-ui/icons/FolderOpen';
import ViewQuilt from '@material-ui/icons/ViewQuilt';
import Cancel from '@material-ui/icons/Cancel';
import SolverChooser from './SolverChooser';
import OptionsMenu from './OptionsMenu';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import * as icon from '../assets/minizinc.svg';


const styles = (_theme: Theme) => createStyles({
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  logo: {
    background: `url(${icon})`,
    backgroundSize: '1.2em',
    backgroundPosition: 'left center',
    backgroundRepeat: 'no-repeat',
    paddingLeft: '1.4em'
  },
  spacer: {
    flexGrow: 1,
  },
  drawer: {
    width: 300,
  }
});

export interface IHeaderProps extends IWithStore, WithStyles<typeof styles> {
  className?: string;
}

@inject('store')
@observer
class Header extends React.Component<IHeaderProps> {
  @bind
  private onAnalyze() {
    const store = this.props.store!;
    store.compile();
  }

  @bind
  private onSolve() {
    const store = this.props.store!;
    store.solve();
  }

  @bind
  private onOpen() {
    const store = this.props.store!;
    store.open();
  }

  @bind
  private onOpenData() {
    const store = this.props.store!;
    store.openData();
  }

  @bind
  private onSave() {
    const store = this.props.store!;
    store.save();
  }

  @bind
  private onSaveAs() {
    const store = this.props.store!;
    store.saveAs();
  }

  @bind
  private onCreate() {
    const store = this.props.store!;
    store.createNew();
  }

  @bind
  private toggleAutoAnalyze() {
    const store = this.props.store!;
    store.autoAnalyze = !store.autoAnalyze;
  }

  @bind
  private handleDrawerOpen() {
    const store = this.props.store!;
    store.ui.drawerOpen = true;
  }

  @bind
  private toggleEditor() {
    const store = this.props.store!;
    store.ui.editorVisible = !store.ui.editorVisible;
  }

  @bind
  private toggleInputs() {
    const store = this.props.store!;
    store.ui.inputsVisible = !store.ui.inputsVisible;
  }

  @bind
  private handleDrawerClose() {
    const store = this.props.store!;
    store.ui.drawerOpen = false;
  }

  @bind
  private onAbort() {
    const store = this.props.store!;
    store.abort();
  }

  @bind
  private hideAbout() {
    const store = this.props.store!;
    store.ui.showAboutDialog = false;
  }

  @bind
  private showAbout() {
    const store = this.props.store!;
    store.ui.showAboutDialog = true;
  }

  render() {
    const store = this.props.store!;
    const classes = this.props.classes;

    return <>
      <AppBar position="absolute" color="default" className={this.props.className}>
        <Toolbar>
          <IconButton color="inherit" aria-label="Open drawer" onClick={this.handleDrawerOpen}
            className={classes.menuButton} >
            <Menu />
          </IconButton>
          <Typography variant="h6" color="inherit" noWrap className={classes.logo}>
            MiniZinc IDE: {store.fileName}{store.isDirtyState ? '*' : ''}
          </Typography>

          <div className={classes.spacer} />

          <SolverChooser />
          <OptionsMenu />

          <div className={classes.spacer} />

          <ResultInfo />
          {!store.autoAnalyze &&
            <IconButton onClick={this.onAnalyze} color="inherit" title="Analyze" disabled={store.running}>
              <CheckCircle/>
            </IconButton>
          }
          <IconButton onClick={this.toggleAutoAnalyze} color="inherit" title={store.autoAnalyze ? 'Disable Auto Analyze' : 'Enable Auto Analyze'}>
            {store.autoAnalyze ? <Sync /> : <SyncDisabled />}
          </IconButton>
          <IconButton onClick={this.onSolve} color="inherit" title="Solve" disabled={store.running || !store.validInput}>
            <PlayArrow/>
          </IconButton>
          <IconButton onClick={this.onSave} color="inherit" title={store.isDefaultFileName ? `Save As` : `Save "${store.fileName}"`} disabled={!store.isDirtyState}>
            <Save/>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer className={classes.drawer} anchor="left" open={store.ui.drawerOpen} onClose={this.handleDrawerClose}>
        <List subheader={<ListSubheader>File</ListSubheader>}>
          <ListItem button onClick={this.onCreate}>
            <ListItemIcon><Create /></ListItemIcon>
            <ListItemText primary="New File" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button onClick={this.onOpen}>
            <ListItemIcon><FolderOpen /></ListItemIcon>
            <ListItemText primary="Open File..." />
          </ListItem>
          <ListItem button onClick={this.onOpenData}>
            <ListItemIcon><FolderOpen /></ListItemIcon>
            <ListItemText primary="Open Data File..." />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button onClick={this.onSave} disabled={!store.isDirtyState}>
            <ListItemIcon><Save /></ListItemIcon>
            <ListItemText primary="Save File..." />
          </ListItem>
          <ListItem button onClick={this.onSaveAs}>
            <ListItemIcon><Save /></ListItemIcon>
            <ListItemText primary="Save File As..." />
          </ListItem>
        </List>
        <Divider />
        <List subheader={<ListSubheader>MiniZinc</ListSubheader>}>
          <ListItem button onClick={this.onAnalyze} disabled={store.running}>
            <ListItemIcon><CheckCircle /></ListItemIcon>
            <ListItemText primary="Analyze" />
          </ListItem>
          <ListItem>
            <ListItemIcon>{store.autoAnalyze ? <Sync /> : <SyncDisabled />}</ListItemIcon>
            <ListItemText primary="Auto Analyze" />
            <ListItemSecondaryAction>
              <Switch onChange={this.toggleAutoAnalyze}
                checked={store.autoAnalyze}
              />
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem button onClick={this.onSolve} disabled={store.running}>
            <ListItemIcon><PlayArrow /></ListItemIcon>
            <ListItemText primary="Solve" />
          </ListItem>
          <ListItem button onClick={this.onAbort} disabled={!store.canAbort}>
            <ListItemIcon><Cancel /></ListItemIcon>
            <ListItemText primary="Cancel" />
          </ListItem>
        </List>
        <Divider />
        <List subheader={<ListSubheader>Layout</ListSubheader>}>
          <ListItem>
            <ListItemIcon><ViewQuilt /></ListItemIcon>
            <ListItemText primary="Editor Visible" />
            <ListItemSecondaryAction>
              <Switch onChange={this.toggleEditor}
                checked={store.ui.editorVisible}
              />
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem>
            <ListItemIcon><ViewQuilt /></ListItemIcon>
            <ListItemText primary="Inputs Visible" />
            <ListItemSecondaryAction>
              <Switch onChange={this.toggleInputs}
                checked={store.ui.inputsVisible}
              />
            </ListItemSecondaryAction>
          </ListItem>
        </List>
        <Divider />
        <List subheader={<ListSubheader>Help</ListSubheader>}>
         <ListItem button onClick={this.showAbout}>
            <ListItemIcon><Info /></ListItemIcon>
            <ListItemText primary="About" />
          </ListItem>
        </List>
      </Drawer>
      <Dialog open={store.ui.showAboutDialog} onClose={this.hideAbout}>
        <DialogTitle>About</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {store.version}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </>;
  }
}

export default withStyles(styles)(Header);
