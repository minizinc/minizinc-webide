import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import App from './App';
import {ApplicationStore} from './stores';
import 'typeface-roboto';
import {IMiniZinc} from 'minizinc';
import './style.scss';
import {IUIAdapter} from './interfaces';

export * from './interfaces';
export * from './stores';

export default function launch(backend: IMiniZinc, adapter: IUIAdapter) {
  const store = new ApplicationStore(backend, adapter);

  ReactDOM.render((
    <Provider store={store}>
      <App />
    </Provider>
  ), document.getElementById('root')!);
}
