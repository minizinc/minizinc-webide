import {ApplicationStore} from './stores';
import {IDataObject} from 'minizinc';
import {IUIAdapter, IFile, cleanFileName} from './interfaces';


export default class WebUIAdapter implements IUIAdapter {
  store: ApplicationStore | null = null;

  openFile() {
    return new Promise<IFile>((resolve, reject) => {
      const input = document.createElement('input');
      input.style.display = 'none';
      input.type = 'file';
      input.accept = '.mzn';
      input.addEventListener('change', () => {
        const file = input.files ? input.files[0] : null;
        input.remove();

        if (!file) {
          return reject();
        }
        const fs = new FileReader();
        fs.addEventListener('load', () => {
          const content = <string>fs.result;
          resolve({fileName: cleanFileName(file.name), content});
        });
        fs.readAsText(file);
      });
      document.body.appendChild(input);
      input.click();
    });
  }

  openData() {
    return new Promise<IDataObject>((resolve, reject) => {
      const input = document.createElement('input');
      input.style.display = 'none';
      input.type = 'file';
      input.accept = '.json';
      input.addEventListener('change', () => {
        const file = input.files ? input.files[0] : null;
        input.remove();

        if (!file) {
          return reject();
        }
        const fs = new FileReader();
        fs.addEventListener('load', () => {
          const content = <string>fs.result;
          resolve(JSON.parse(content));
        });
        fs.readAsText(file);
      });
      document.body.appendChild(input);
      input.click();
    });
  }

  saveAs(code: string, data: IDataObject) {
    this.save('model', code, data);
    return 'model';
  }

  save(fileName: string | null, code: string, data: IDataObject) {
    this.downloadFile(code, fileName ? `${cleanFileName(fileName)}.mzn` : null, 'text/plain');
    if (Object.keys(data).length > 0) {
      this.downloadFile(JSON.stringify(data, null, 2), fileName ? `${cleanFileName(fileName)}.json` : null, 'application/json');
    }
  }

  private downloadFile(content: string, name: string | null, mimeType: string) {
    const url = URL.createObjectURL(new Blob([content], {
      type: mimeType
    }));

    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    if (name) {
      a.download = name;
    } else {
      a.target = '_blank';
    }
    document.body.appendChild(a);
    a.click();
    a.remove();

    URL.revokeObjectURL(url);
  }
}
