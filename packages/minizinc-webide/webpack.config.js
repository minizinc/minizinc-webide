const resolve = require('path').resolve;
const {createConfig, withHTMLPlugin, withCSSExtractPlugin, withMiniZincPolyfill} = require('../../webpack.base');

/**
 * generate a webpack configuration
 */
module.exports = (_env, options) => {
  return createConfig({
    mode: options.mode,
    pkg: require('./package.json')
  }, [{
    entry: {
      app: './src/start.ts',
      // Package each language's worker and give these filenames in `getWorkerUrl`
      'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
      // 'json.worker': 'monaco-editor/esm/vs/language/json/json.worker',
      // 'css.worker': 'monaco-editor/esm/vs/language/css/css.worker',
      // 'html.worker': 'monaco-editor/esm/vs/language/html/html.worker',
      // 'ts.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
    },
    output: {
      globalObject: 'self',
      path: resolve(__dirname, 'build')
    }
  },
  withCSSExtractPlugin(),
  withMiniZincPolyfill(),
  withHTMLPlugin({
    chunks: ['app'],
    template: resolve(__dirname, 'src/index.html'),
    title: 'MiniZinc IDE',
  })
  ]);
};
