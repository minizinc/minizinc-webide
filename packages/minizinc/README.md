minizinc
============
[![License: MIT][mit-image]][mit-url] [![NPM version][npm-image]][npm-url]

This is a common wrapper node package around [MiniZinc](https://www.minizinc.org) providing a common interface for MiniZinc CLI and Emscripten version. 

The common interface provides easy access to solve and analyze a model. 

```ts
export interface IMiniZinc {
  analyze(paramsOrCode: string | IModelParams, options?: Partial<IMiniZincOptions>): Promise<IModelMetaData>;

  solve(paramsOrCode: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveOptions>): Promise<IResult>;

  solveRaw(paramsOrCode: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveRawOptions>): Promise<string>;

  resolveSolvers(): Promise<ISolverConfiguration[]>;

  version(): Promise<string>;

  terminate(): Promise<boolean>;
}
```

see [src/interfaces.ts](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/interfaces.ts) for details. 

this package contains different implementations of this interface:

 * [`CLIMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/CLIMinizinc.ts) wraps an installed version of MiniZinc through its CLI. Thus requires an installed version of MiniZinc
 * [`EmbeddedMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/EmbeddedMiniZinc.ts) uses [minizinc-js](https://gitlab.com/minizinc/minizinc-js) and [gecode-js](https://gitlab.com/minizinc/gecode-js) to run MiniZinc in pure JavaScript
 * [`WebWorkerMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/WebWorkerMiniZinc.ts) similar to `EmbeddedMiniZinc` but uses Webworkers.
 * [`ForkWorkerMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/ForkWorkerMiniZinc.ts) similar to `WebWorkerMiniZinc` but uses `child_process.fork`.
 * [`RESTMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/RESTMiniZinc.ts) an wrapper around an REST service as provided by [minizinc-server](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc-server/README.md)
 * [`MessagingMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/MessagingMiniZinc.ts) communicates with its counterpart [`MessagingReceiver`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/MessagingReceiver.ts) via a channel, e.g. for communicating with an iframe.

Usage
-----

```ts
import CLIMiniZinc from 'minizinc/build/CLIMiniZinc';

const m = new CLIMiniZinc();

const model = `...`;

m.solve(model).then((result) => {
  console.log(result);
});
```


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[npm-image]: https://badge.fury.io/js/minizinc.svg
[npm-url]: https://npmjs.org/package/minizinc
