import WorkerMiniZinc, {ISolverWorkerFactory} from './internal/WorkerMiniZinc';
import MINIZINC, {IMiniZincWorkerClient, ISolverWorkerClient, ISolverModule} from 'minizinc-js';
import GECODE from 'gecode-js';
import {IMiniZincConstructorOptions} from './interfaces';
import {fork} from 'child_process';
import {resolve} from 'path';
import {IWorkerLike} from 'emscripten_wrapper';

function forkAdapter(module: string): IWorkerLike {
  const path = resolve(__dirname, 'internal', 'ForkModuleWorker.js');
  const forked = fork(path, [module]);
  return {
    addEventListener: (_type, listener) => forked.on('message', (data) => listener(<any>{type: 'message', data})),
    postMessage: (msg) => forked.send(msg),
    terminate: () => forked.kill()
  };
}

function createSolver(module: ISolverModule, moduleName: string): ISolverWorkerFactory {
  return {
    info: module,
    create: () => <ISolverWorkerClient>module.createWorkerClient(forkAdapter(moduleName))
  };
}

export default class ForkWorkerMiniZinc extends WorkerMiniZinc {
  constructor(options: Partial<IMiniZincConstructorOptions>) {
    super({
      mzn: () => <IMiniZincWorkerClient>MINIZINC.createWorkerClient(forkAdapter('minizinc-js')),
      solvers: [
        createSolver(GECODE, 'gecode-js')
      ],
      extraIncludeFiles: options.extraIncludeFiles
    });
  }
}
