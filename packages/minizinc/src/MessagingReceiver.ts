import {bind} from 'decko';
import {AbortSignal, IError, IMiniZinc, IMiniZincSolveOptions, IMiniZincSolveRawOptions} from '.';
import {EVENT_ABORT, EVENT_ANALYZE, EVENT_RESOLVE_SOLVERS, EVENT_SOLVE, EVENT_SOLVE_RAW, EVENT_SOLVE_RAW_CHUNK, EVENT_SOLVE_SOLUTION, EVENT_TERMINATE, EVENT_VERSION, IAnalyzeRequest, IAnalyzeResponse, IResolveSolversRequest, IResolveSolversResponse, ISolveRawRequest, ISolveRawResponse, ISolveRequest, ISolveResponse, ISolveSolutionResponse, ITerminateRequest, ITerminateResponse, IVersionRequest, IVersionResponse} from './internal/messages';

export interface IReplyer {
  reply(channel: string, arg?: any): void;
}

interface IChannelReceiveLike {
  once(channel: string, listener: (evt: IReplyer, arg: any) => void): void;
  on(channel: string, listener: (evt: IReplyer, arg: any) => void): void;
  removeListener(channel: string, listener: (evt: IReplyer, arg: any) => void): void;
}

export interface IMessagingReceiverOptions {
  channel: IChannelReceiveLike;
  window?: Window;
}

export function createWindowReceiverChannel(win?: Window): IChannelReceiveLike {
  const w = win || window;
  const onByChannel = new Map<string, Set<(evt: IReplyer, arg: any) => void>>();
  const onceByChannel = new Map<string, Set<(evt: IReplyer, arg: any) => void>>();

  const onMessage = (evt: MessageEvent) => {
    if (!evt.data || !evt.data.channel || !evt.source) {
      return;
    }
    const channel = evt.data.channel;
    const arg = evt.data.arg;
    const replyer: IReplyer = {
      reply: (channel: string, arg?: any) => {
        (<WindowProxy>evt.source!).postMessage({channel, arg}, evt.origin);
      }
    };
    if (onByChannel.has(channel)) {
      onByChannel.get(channel)!.forEach((l) => l(replyer, arg));
    }
    if (!onceByChannel.has(channel)) {
      return;
    }
    const r = onceByChannel.get(channel)!;
    onceByChannel.delete(channel);
    r.forEach((l) => l(replyer, arg));
  };

  return {
    on: (channel, listener) => {
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        w.addEventListener('message', onMessage);
      }
      if (!onByChannel.has(channel)) {
        onByChannel.set(channel, new Set());
      }
      onByChannel.get(channel)!.add(listener);
    },
    once: (channel, listener) => {
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        w.addEventListener('message', onMessage);
      }
      if (!onceByChannel.has(channel)) {
        onceByChannel.set(channel, new Set());
      }
      onceByChannel.get(channel)!.add(listener);
    },
    removeListener: (channel, listener) => {
      if (onByChannel.has(channel)) {
        const s = onByChannel.get(channel)!;
        s.delete(listener);
        if (s.size === 0) {
          onByChannel.delete(channel);
        }
      }
      if (onceByChannel.has(channel)) {
        const s = onceByChannel.get(channel)!;
        s.delete(listener);
        if (s.size === 0) {
          onceByChannel.delete(channel);
        }
      }
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        w.removeEventListener('message', onMessage);
      }
    }
  };
}

export default class MessagingReceiver {
  constructor(private readonly minizinc: IMiniZinc, private readonly channel: IChannelReceiveLike) {

  }

  private abortListener(evt: IReplyer, arg: IAnalyzeRequest | ISolveRequest | ISolveRawRequest) {
    const signal = new AbortSignal();
    const listener = () => {
      signal.abort();
    };
    this.channel.once(`${EVENT_ABORT}-${arg.key}`, listener);

    const reply = (channel: string, arg: any) => {
      this.channel.removeListener(`${EVENT_ABORT}-${arg.key}`, listener);
      if (!signal.aborted) {
        evt.reply(channel, arg);
        return;
      }
      evt.reply(channel, {
        error: [<IError>{
          type: 'unknown',
          message: 'The user aborted the request'
        }]
      });
    };

    const options = Object.assign({}, arg.options, {signal});

    return {reply, options};
  }

  @bind
  private onAnalyze(evt: IReplyer, arg: IAnalyzeRequest) {
    const {options, reply} = this.abortListener(evt, arg);
    const r = this.minizinc.analyze(arg.code, options);
    r.then((metaData) => reply(`${EVENT_ANALYZE}-${arg.key}`, <IAnalyzeResponse>{metaData}));
    r.catch((error) => reply(`${EVENT_ANALYZE}-${arg.key}`, <IAnalyzeResponse>{error: error.errors}));
  }

  @bind
  private onSolve(evt: IReplyer, arg: ISolveRequest) {
    const {options, reply} = this.abortListener(evt, arg);

    if (arg.streamSolutions) {
      (<IMiniZincSolveOptions>options).onPartialResult = (type, result) => evt.reply(`${EVENT_SOLVE_SOLUTION}-${arg.key}`, <ISolveSolutionResponse>{type, result});
    }

    const r = this.minizinc.solve(typeof arg.code === 'string' ? {model: arg.code} : arg.code, arg.data, options);
    r.then((result) => reply(`${EVENT_SOLVE}-${arg.key}`, <ISolveResponse>{result}));
    r.catch((error) => reply(`${EVENT_SOLVE}-${arg.key}`, <ISolveResponse>{error: error.errors}));
  }

  @bind
  private onSolveRaw(evt: IReplyer, arg: ISolveRawRequest) {
    const {options, reply} = this.abortListener(evt, arg);

    if (arg.streamChunks) {
      (<IMiniZincSolveRawOptions>options).onChunk = (chunk) => evt.reply(`${EVENT_SOLVE_RAW_CHUNK}-${arg.key}`, <ISolveRawResponse>chunk);
    }

    const r = this.minizinc.solveRaw(typeof arg.code === 'string' ? {model: arg.code} : arg.code, arg.data, options);
    r.then((result) => reply(`${EVENT_SOLVE_RAW}-${arg.key}`, <ISolveRawResponse>result));
    r.catch((error) => reply(`${EVENT_SOLVE_RAW}-${arg.key}`, <ISolveRawResponse>error.toString()));
  }

  @bind
  private onResolveSolvers(evt: IReplyer, arg: IResolveSolversRequest) {
    const r = this.minizinc.resolveSolvers();
    r.then((solvers) => evt.reply(`${EVENT_RESOLVE_SOLVERS}-${arg.key}`, <IResolveSolversResponse>{solvers}));
    r.catch((error) => evt.reply(`${EVENT_RESOLVE_SOLVERS}-${arg.key}`, <IResolveSolversResponse>{error: error.errors}));
  }

  @bind
  private onVersion(evt: IReplyer, arg: IVersionRequest) {
    const r = this.minizinc.version();
    r.then((version) => evt.reply(`${EVENT_VERSION}-${arg.key}`, <IVersionResponse>{version}));
    r.catch((error) => evt.reply(`${EVENT_VERSION}-${arg.key}`, <IVersionResponse>{error: error.errors}));
  }

  @bind
  private onTerminate(evt: IReplyer, arg: ITerminateRequest) {
    const r = this.minizinc.terminate();
    r.then((ok) => evt.reply(`${EVENT_TERMINATE}-${arg.key}`, <ITerminateResponse>{ok}));
    r.catch((error) => evt.reply(`${EVENT_TERMINATE}-${arg.key}`, <ITerminateResponse>{error: error.errors}));
  }

  register() {
    this.channel.on(EVENT_ANALYZE, this.onAnalyze);
    this.channel.on(EVENT_SOLVE, this.onSolve);
    this.channel.on(EVENT_SOLVE_RAW, this.onSolveRaw);
    this.channel.on(EVENT_RESOLVE_SOLVERS, this.onResolveSolvers);
    this.channel.on(EVENT_VERSION, this.onVersion);
    this.channel.on(EVENT_TERMINATE, this.onTerminate);
  }

  deregister() {
    this.channel.removeListener(EVENT_ANALYZE, this.onAnalyze);
    this.channel.removeListener(EVENT_SOLVE, this.onSolve);
    this.channel.removeListener(EVENT_SOLVE_RAW, this.onSolveRaw);
    this.channel.removeListener(EVENT_RESOLVE_SOLVERS, this.onResolveSolvers);
    this.channel.removeListener(EVENT_VERSION, this.onVersion);
    this.channel.removeListener(EVENT_TERMINATE, this.onTerminate);
  }
}
