
import {ITypeInfo, IValue, ISet, IEnumValue, EType, INameSet, IPrimitiveValue, isSet, createEnumValue, isSetRange, ISetRange, isEnumValue} from './interfaces';

function formatPrimitiveValue(prop: ITypeInfo, value: IPrimitiveValue) {
  if (prop.enum_type) {
    // enum value
    return (<IEnumValue>value).e;
  }
  return (<string | number | boolean>value).toString();
}

export function formatValue(prop: ITypeInfo, value: IValue | null) {
  if (value == null) {
    return '';
  }
  if (prop.dim === 0 && !prop.set) {
    return formatPrimitiveValue(prop, <IPrimitiveValue>value);
  }
  if (prop.set) {
    const vs = <ISet>value;
    return vs.set.map((v) => formatPrimitiveValue(prop, v)).join('\n');
  }
  if (prop.dim === 1) {
    return (<any[]>value).map((v) => formatPrimitiveValue(prop, v)).join('\n');
  }
  const formatArray = (arr: any, dim: number): string => {
    if (dim === 0) {
      return formatPrimitiveValue(prop, arr);
    }
    if (dim === 1) {
      return arr.map((v: any) => formatArray(v, dim - 1)).join(', ');
    }
    return (<any[]>arr).map((v: any) => formatArray(v, dim - 1)).join(`\n${'\n'.repeat(dim - 2)}`);
  };
  return formatArray(<any[]>value, prop.dim);
}

export function parseValue(prop: ITypeInfo, value: string): {v: IValue | null, error?: string} {
  const parsePrimitive = (value: string, prefix = ''): {v: IPrimitiveValue | null, error?: string} => {
    if (prop.enum_type) { // always a string
      value = value.trim();
      if (value.length === 0) {
        return {
          v: null,
          error: `${prefix} invalid enum`
        };
      }
      return {v: createEnumValue(value)};
    }
    switch (prop.type) {
      case EType.int:
        const v = parseInt(value, 10);
        if (value === '' || isNaN(v)) {
          return {
            v: null,
            error: `${prefix} invalid int`
          };
        }
        return {v};
      case EType.float:
        const vf = parseFloat(value);
        if (value === '' || isNaN(vf)) {
          return {
            v: null,
            error: `${prefix} invalid float`
          };
        }
        return {v: vf};
      case EType.bool:
        return {v: Boolean(value)};
      default:
        if (!value) {
          return {
            v: null,
            error: `${prefix} value required`
          };
        }
        return {v: value};
    }
  };

  if (prop.dim === 0 && !prop.set) {
    return parsePrimitive(value);
  }

  if (prop.set || prop.dim === 1) {
    const values = value.trim().split(/[ \n;,|]+/gm).map((d) => d.trim()).filter((d) => d.length > 0);
    if (values.length === 0) {
      return {
        v: null,
        error: 'at least one value is required'
      };
    }
    const elems = values.map((v, i) => parsePrimitive(v, `${i + 1}: `));
    const errors = elems.filter((v) => v.error).map((v) => v.error);
    if (errors.length > 0) {
      return {
        v: null,
        error: errors.join(', ')
      };
    }
    const data = elems.map((v) => v.v!);

    if (prop.set && (new Set(data)).size < data.length) {
      // verify unique values
      return {
        v: null,
        error: 'duplicate set entry'
      };
    }

    let v: IValue;

    if (prop.set) {
      v = <ISet>{
        set: data
      };
    } else {
      v = data;
    }
    return {v};
  }

  if (prop.dim > 2) {
    // TODO support 3D matrices
    return {
      v: null,
      error: 'not yet supported'
    };
  }

  const values = value.trim().split(/[\n;|]+/gm).filter((d) => d.trim().length > 0).map((d) => d.trim().split(/[ ,]+/gm));
  const rows = values.map((row, j) => row.map((v, i) => parsePrimitive(v, `${j}x${i}`)));

  const errors: string[] = [];
  for (const row of rows) {
    for(const entry of row) {
      if (entry.error) {
        errors.push(entry.error);
      }
    }
  }
  if (errors.length > 0) {
    return {
      v: null,
      error: errors.join(', ')
    };
  }

  const lengths = new Set(rows.map((d) => d.length));
  if (lengths.size > 1) {
    return {
      v: null,
      error: `different lengths: ${Array.from(lengths).sort((a, b) => a - b).join(', ')}`
    };
  }
  return {
    v: rows.map((row) => row.map((v) => v.v!))
  };
}

export function setAsArray(set: ISetRange): number[];
export function setAsArray<T extends IPrimitiveValue>(set: ISet<T>): T[];
export function setAsArray(set: any): any[] {
  if (!set || !isSet(set)) {
    return [];
  }
  const values = set.set;
  if (isSetRange(set)) {
    // range mode
    const start: number = set.set[0][0];
    const end: number = set.set[0][1];
    const r: number[] = [];
    for (let i = start; i <= end; i++) {
      r.push(i);
    }
    return r;
  }
  return values;
}

export function setAsNamesArray(set: INameSet): string[] {
  return (isSetRange(set) ? setAsArray(set) : setAsArray(set)).map((d) => isEnumValue(d) ? d.e : d.toString());
}
