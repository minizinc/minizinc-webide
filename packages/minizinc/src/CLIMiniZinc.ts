import {mkdtemp, remove} from 'fs-extra';
import {BackendException, IDataObject, IMiniZincOptions, IMiniZinc, IMiniZincSolveOptions, IModelParams, IResult, AbortException, IAbortSignal, IMiniZincConstructorOptions, IMiniZincSolveRawOptions} from './interfaces';
import {tmpdir} from 'os';
import {sep} from 'path';
import {toSolveArguments, toFileNames} from './internal/common';
import parseError from './output/parseError';
import {ResultBuilder} from './output/result';
import run, {ISpawnResultBuilder, runBuilder, simpleFileSystem} from './internal/cli';
import {resolveSolvers, version, analyzeImpl, createExtraIncludeFiles} from './internal/logic';

export {isCLIAvailable} from './internal/cli';


export declare type ICLIMiniZincOptions = IMiniZincConstructorOptions;
// export interface ICLIMiniZincOptions extends IMiniZincConstructorOptions {
//   // TODO
// }

export default class CLIMiniZinc implements IMiniZinc {
  constructor(private readonly options: Partial<ICLIMiniZincOptions> = {}) {

  }

  async solve(paramsOrCode: IModelParams | string, data?: IDataObject | string, options: Partial<IMiniZincSolveOptions> = {}): Promise<IResult> {
    const params = typeof paramsOrCode === 'string' ? {model: paramsOrCode} : paramsOrCode;
    let tmpDir: string = '';

    try {
      tmpDir = await mkdtemp(`${tmpdir()}${sep}`);

      if (options.signal && options.signal.aborted) {
        throw new AbortException();
      }

      const {modelFile, dataFile} = toFileNames(options.fileName, tmpDir, sep, data);
      const {args, model, filteredData} = toSolveArguments(params, modelFile, dataFile, true, data);

      await simpleFileSystem.ensureDir(tmpDir);

      await simpleFileSystem.writeTextFile(modelFile, model);

      const {includePath} = await createExtraIncludeFiles(tmpDir, sep, simpleFileSystem, Object.assign({}, this.options.extraIncludeFiles || {}, options.extraIncludeFiles || {}));
      if (includePath) {
        args.unshift('-I', includePath);
      }

      if (filteredData) {
        await simpleFileSystem.writeTextFile(dataFile, filteredData);
      }

      if (options.signal && options.signal.aborted) {
        throw new AbortException();
      }

      const analyzed = await this.analyzeImpl(modelFile, params, includePath, options.signal);

      const builder = new ResultBuilder(analyzed, params, options.onPartialResult);
      let stderr = '';
      const builderHelper: ISpawnResultBuilder<IResult> = {
        pushSTDOut: (chunk) => builder.push(chunk),
        pushSTDErr: (chunk) => stderr += chunk,
        build: (code) => {
          if (code) {
            throw new BackendException(parseError(stderr, modelFile, dataFile));
          }
          return builder.build();
        }
      };
      return await runBuilder(args, builderHelper, undefined, options.signal);
    } catch (e) {
      console.warn('error caught', e);
      throw e;
    } finally {
      if (tmpDir) {
        await remove(tmpDir);
      }
    }
  }

  async solveRaw(paramsOrCode: string | IModelParams, data?: IDataObject | string, options: Partial<IMiniZincSolveRawOptions> = {}) {
    const params = typeof paramsOrCode === 'string' ? {model: paramsOrCode} : paramsOrCode;
    let tmpDir: string = '';

    try {
      tmpDir = await mkdtemp(`${tmpdir()}${sep}`);

      if (options.signal && options.signal.aborted) {
        throw new AbortException();
      }

      const {modelFile, dataFile} = toFileNames(options.fileName, tmpDir, sep, data);
      const {args, model, filteredData} = toSolveArguments(params, modelFile, dataFile, options.stats !== false, data);

      await simpleFileSystem.ensureDir(tmpDir);

      await simpleFileSystem.writeTextFile(modelFile, model);

      const {includePath} = await createExtraIncludeFiles(tmpDir, sep, simpleFileSystem, Object.assign({}, this.options.extraIncludeFiles || {}, options.extraIncludeFiles || {}));
      if (includePath) {
        args.unshift('-I', includePath);
      }

      if (filteredData) {
        await simpleFileSystem.writeTextFile(dataFile, filteredData);
      }

      if (options.signal && options.signal.aborted) {
        throw new AbortException();
      }

      let stdout = '';
      const onChunk = (chunk: string) => {
        stdout += chunk;
        if (options.onChunk) {
          options.onChunk(chunk.toString());
        }
      };
      const builderHelper: ISpawnResultBuilder<string> = {
        pushSTDOut: onChunk,
        pushSTDErr: onChunk,
        build: () => stdout
      };
      return await runBuilder(args, builderHelper, undefined, options.signal);
    } catch (e) {
      console.warn('error caught', e);
      throw e;
    } finally {
      if (tmpDir) {
        await remove(tmpDir);
      }
    }
  }

  private analyzeImpl(modelFile: string, params: IModelParams, includePath: string | null, signal?: IAbortSignal) {
    return analyzeImpl({
      run: (args, stdin) => run(args, stdin, signal)
    }, params, includePath ? ['-I', includePath] : [], modelFile, signal);
  }

  async analyze(paramsOrCode: string | IModelParams, options: Partial<IMiniZincOptions> = {}) {
    const params = typeof paramsOrCode === 'string' ? {model: paramsOrCode} : paramsOrCode;
    let tmpDir = '';
    try {
      tmpDir = await mkdtemp(`${tmpdir()}${sep}`);

      const {modelFile} = toFileNames(options.fileName, tmpDir, sep);

      await simpleFileSystem.ensureDir(tmpDir);
      await simpleFileSystem.writeTextFile(modelFile, params.model);

      const {includePath} = await createExtraIncludeFiles(tmpDir, sep, simpleFileSystem, Object.assign({}, this.options.extraIncludeFiles || {}, options.extraIncludeFiles || {}));

      if (options.signal && options.signal.aborted) {
        throw new AbortException();
      }

      return await this.analyzeImpl(modelFile, params, includePath, options.signal);
    } catch (e) {
      console.warn('error caught', e);
      throw e;
    } finally {
      if (tmpDir) {
        await remove(tmpDir);
      }
    }
  }

  resolveSolvers() {
    return resolveSolvers({run});
  }

  version() {
    return version({run});
  }

  terminate() {
    return Promise.resolve(true);
  }
}
