import {IAbortSignal} from './internal/abort';

export {IAbortSignal, default as AbortSignal, AbortException} from './internal/abort';


export enum EType {
  unknown = 'unknown',
  int = 'int',
  float = 'float',
  bool = 'bool',
  string = 'string',
  ann = 'ann'
}


export interface ISet<T extends IPrimitiveValue = IPrimitiveValue> {
  set: T[];
}

export interface ISetRange {
  set: [[number, number]];
}

export interface IEnumValue {
  e: string;
}

export declare type IPrimitiveValue = string | boolean | number | IEnumValue;

export declare type IValue =  IPrimitiveValue | IPrimitiveValue[] | IPrimitiveValue[][] | ISet | ISetRange;

export interface IDataObject {
  [key: string]: IValue;
}

export function isEnumValue(v: IValue): v is IEnumValue {
  return v != null && typeof (<IEnumValue>v).e === 'string';
}

export function isSet(v: IValue): v is ISet {
  return v != null && Array.isArray((<ISet>v).set);
}

export function isSetRange(v: IValue): v is ISetRange {
  return v != null && Array.isArray((<ISetRange>v).set) && (<ISetRange>v).set.length === 1 && Array.isArray((<ISetRange>v).set[0]) && typeof (<ISetRange>v).set[0][0] === 'number';
}

export declare type INameSet = ISet<IEnumValue | number> | ISetRange;

class EnumValue {
  constructor(public readonly e: string) {

  }

  toString() {
    return this.e;
  }
}

export function createEnumValue(value: string): IEnumValue {
  return new EnumValue(value);
}

export interface ITypeInfo {
  key: string;
  type: EType;
  dim: number;
  set?: boolean;
  // type of the dimension sets, e.g. int or enum keys
  dims?: string[];

  // which enum type this variable is based on
  enum_type?: string;

  optional?: boolean;
}

export interface IHasTypeInfo {
  _prop: ITypeInfo;
}

export declare type ITypedValue = (IPrimitiveValue | IPrimitiveValue[] | IPrimitiveValue[][]) & IHasTypeInfo;

export interface ITypedDataObject {
  [key: string]: ITypedValue;
}

export function injectTypeInfo<T>(v: T, prop: ITypeInfo): T & IHasTypeInfo {
  const vany: any = v;
  if (typeof v === 'number' || typeof v === 'boolean') {
    return vany;
  }
  vany._prop = prop;
  return vany;
}

export function hasTypeInfo<T>(v: T): v is T & IHasTypeInfo {
  return (<any>v)._prop != null;
}

export function asTypedValue(v: IValue, prop: ITypeInfo): ITypedValue {
  if (prop.set) {
    return injectTypeInfo<any>((<ISet>v).set, prop);
  }
  return injectTypeInfo<any>(v, prop);
}

export interface IEnteredValue extends ITypeInfo {
  valueAsString: string | number | boolean;
  error?: string;
  value: IValue | null;
}

export interface ILocation {
  line: number;
  columns: [number, number];
}

export interface IError {
  type: string;
  message: string;
  location?: ILocation;
  inputs?: ITypeInfo[];
}

export enum EMethod {
  SATISFY = 'SATISFY',
  MINIMIZE = 'MINIMIZE',
  MAXIMIZE = 'MAXIMIZE',
}

export interface IModelMetaData {
  inputs: ITypeInfo[];
  outputs: ITypeInfo[];
  vars: ITypeInfo[];
  enums: string[];
  method: EMethod;
}

export interface IModelParams {
  model: string;
  solver?: string;

  random_seed?: number;
  all_solutions?: boolean;
  free_search?: boolean;
  processes?: number;
  nr_solutions?: number;
}

export interface ISolution {
  assignments: IDataObject;
  objective?: number;
  extraOutput?: string;
}

export enum EStatus {
  ERROR = 'ERROR',
  UNKNOWN = 'UNKNOWN',
  UNBOUNDED = 'UNBOUNDED',
  UNSATISFIABLE = 'UNSATISFIABLE',
  SATISFIED = 'SATISFIED',
  ALL_SOLUTIONS = 'ALL_SOLUTIONS',
  OPTIMAL_SOLUTION = 'OPTIMAL_SOLUTION'
}

export interface IResult {
  status: EStatus;
  complete: boolean;
  outputs: ITypeInfo[];
  objective?: number;
  header: {[key: string]: any};
  extraHeader?: string;
  solutions: ISolution[];
  statistics?: IStatistics;
}

export interface IStatistics {
  initTime: number;
  solveTime: number;
  solutions: number;
  variables: number;
  propagators: number;
  propagations: number;
  nodes: number;
  failures: number;
  restarts: number;
  peakDepth: number;
}

export interface IMiniZincOptions {
  fileName: string;
  signal: IAbortSignal;

  /**
   * extra files to add and include in a virtual extra include directory
   */
  extraIncludeFiles: {
    [filename: string]: string;
  };
}

export interface IMiniZincSolveOptions extends IMiniZincOptions {
  onPartialResult(type: 'solution' | 'header', result: IResult): void;
}

export interface IMiniZincSolveRawOptions extends IMiniZincOptions {
  onChunk(chunk: string): void;
  stats: boolean;
}

export interface ISolverConfiguration {
  id: string;
  description: string;
  name: string;
  tags: string[];
  version: string;
}

export interface IMiniZinc {
  analyze(paramsOrCode: string | IModelParams, options?: Partial<IMiniZincOptions>): Promise<IModelMetaData>;

  solve(paramsOrCode: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveOptions>): Promise<IResult>;

  solveRaw(paramsOrCode: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveRawOptions>): Promise<string>;

  resolveSolvers(): Promise<ISolverConfiguration[]>;

  version(): Promise<string>;

  terminate(): Promise<boolean>;
}

export interface IMiniZincConstructorOptions {
  /**
   * extra files to add and include in a virtual extra include directory
   */
  extraIncludeFiles: {
    [filename: string]: string;
  };
}

export interface IMiniZincConstructor {
  new(options: Partial<IMiniZincConstructorOptions>): IMiniZinc;
}

export class ProxyMiniZinc implements IMiniZinc {
  private _instance: Promise<IMiniZinc> | null = null;

  constructor(private readonly loader: () => Promise<{default: IMiniZincConstructor}>, private readonly options: Partial<IMiniZincConstructorOptions> = {}) {
  }

  private get instance() {
    if (this._instance) {
      return this._instance;
    }
    return this._instance = this.loader().then((module) => new module.default(this.options));
  }

  analyze(code: IModelParams | string, options?: Partial<IMiniZincOptions>) {
    return this.instance.then((i) => i.analyze(code, options));
  }

  solve(params: IModelParams | string, data?: IDataObject | string, options?: Partial<IMiniZincSolveOptions>) {
    return this.instance.then((i) => i.solve(params, data, options));
  }

  solveRaw(params: IModelParams | string, data?: IDataObject | string, options?: Partial<IMiniZincSolveRawOptions>) {
    return this.instance.then((i) => i.solveRaw(params, data, options));
  }

  resolveSolvers(): Promise<ISolverConfiguration[]> {
    return this.instance.then((i) => i.resolveSolvers());
  }

  version() {
    return this.instance.then((i) => i.version());
  }

  terminate() {
    if (!this._instance) { // nothing to terminate
      return Promise.resolve(true);
    }
    return this.instance.then((i) => i.terminate());
  }
}


export class BackendException extends Error {
  constructor(public readonly errors: IError[]) {
    super(errors[0].message);
  }

  get error() {
    return this.errors[0];
  }
}


export function defaultValue(prop: ITypeInfo): IValue | null {
  if (prop.dim > 0) {
    return [];
  }
  if (prop.set) {
    return {
      set: []
    };
  }
  if (prop.enum_type) { // single enum value
    return null;
  }
  switch(prop.type) {
    case EType.int:
      return 0;
    case EType.float:
      return 0.0;
    case EType.bool:
      return false;
    case EType.string:
      return '';
    default:
      return null;
  }
}
