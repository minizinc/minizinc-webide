import GECODE from 'gecode-js';
import MZN2FZN, {IMiniZincModule, ISolverModule, IMiniZincSyncModule} from 'minizinc-js';
import {IDataObject, IMiniZinc, IMiniZincOptions, IMiniZincSolveOptions, IModelParams, IResult, IMiniZincConstructorOptions, IMiniZincSolveRawOptions} from './interfaces';
import {AbortException} from './internal/abort';
import {analyzeEmbedded, solveEmbedded, solveRawEmbedded, IMZNRunner, IFSSolverRunner} from './internal/embedded';
import {resolveSolvers, version} from './internal/logic';


export interface IEmbeddedMiniZincOptions extends IMiniZincConstructorOptions {
  /**
   * load the underlying compiler modules as soon as possible and not when they are used first
   * @default false
   */
  eagerLoad: boolean;

  /**
   * minizinc module to use
   * @default wasm version
   */
  mzn: IMiniZincModule;

  /**
   * gecode module to use
   * @default wasm version
   */
  solvers: ISolverModule[];
}

class MZNWrapper implements IMZNRunner {
  constructor(private readonly mod: IMiniZincModule, private readonly instance: IMiniZincSyncModule) {

  }

  chooseSolver(solvers: IFSSolverRunner[], solver?: string) {
    return this.mod.chooseSolver(solvers, solver);
  }

  get simpleFileSystem() {
    return this.instance.simpleFileSystem;
  }

  get stdout() {
    return this.instance.stdout;
  }

  get stderr() {
    return this.instance.stderr;
  }

  run(args: string[], stdin?: string) {
    return this.instance.run(args, stdin);
  }

  kill() {
    this.instance.kill();
  }
}

export default class EmbeddedMiniZinc implements IMiniZinc {
  private readonly mzn: IMiniZincModule;
  private readonly solvers: ISolverModule[];

  constructor(private readonly options: Partial<IEmbeddedMiniZincOptions> = {}) {
    this.mzn = options.mzn || MZN2FZN;
    this.solvers = options.solvers && options.solvers.length > 0 ? options.solvers : [GECODE];
    for (const s of this.solvers) {
      this.mzn.registerSolver(s);
    }

    if (!options.eagerLoad) {
      return;
    }
    // start loading by calling the sync metho
    this.mzn.sync();
    for (const s of this.solvers) {
      s.sync();
    }
  }

  async solve(paramsOrCode: IModelParams | string, data?: IDataObject | string, options: Partial<IMiniZincSolveOptions> = {}): Promise<IResult> {
    const mzn = await this.mzn.sync();

    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }

    return solveEmbedded(new MZNWrapper(this.mzn, mzn), this.solvers, paramsOrCode, data, options, this.options);
  }

  async solveRaw(paramsOrCode: string | IModelParams, data?: IDataObject | string, options: Partial<IMiniZincSolveRawOptions> = {}): Promise<string> {
    const mzn = await this.mzn.sync();

    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }

    return solveRawEmbedded(new MZNWrapper(this.mzn, mzn), this.solvers, paramsOrCode, data, options, this.options);
  }

  async analyze(paramsOrCode: string | IModelParams, options: Partial<IMiniZincOptions> = {}) {
    const mzn = await this.mzn.sync();

    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }

    return analyzeEmbedded(mzn, this.solvers, paramsOrCode, options, this.options);
  }

  protected runImpl(args: string[], stdin?: string) {
    return this.mzn.run(args, stdin);
  }

  resolveSolvers() {
    return resolveSolvers(this.mzn);
  }

  version() {
    return version(this.mzn);
  }

  terminate() {
    return Promise.resolve(true);
  }
}
