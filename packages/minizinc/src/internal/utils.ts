import {IMiniZincOptions, IMiniZincConstructorOptions} from '..';

export function normalize(str: string) {
  return str.replace(/\\/gm, '/').replace(/\r/gm, '\n').replace(/\n\n/gm, '\n').trim();
}

export function parseMultiObjects(value: string) {
  const values = value.split(/\}\s*\{/);

  if (values.length === 0) {
    return values;
  }

  return values.map((d, i) => `${i > 0 ? '{' : ''}${d}${i < values.length - 1 ? '}' : ''}`);
}

export function randomBaseDir() {
  return `/src${Math.random().toString(36).slice(3)}`;
}

export function buildOptions(options: Partial<IMiniZincOptions>, coptions: Partial<IMiniZincConstructorOptions>) {
  const copy = Object.assign({}, options);
  delete copy.signal;
  delete (<any>copy).onPartialResult;
  delete (<any>copy).onChunk;
  if (!coptions.extraIncludeFiles) {
    return copy;
  }
  if (copy.extraIncludeFiles) {
    copy.extraIncludeFiles = Object.assign({}, coptions.extraIncludeFiles, copy.extraIncludeFiles);
  } else {
    copy.extraIncludeFiles = coptions.extraIncludeFiles;
  }
  return copy;
}
