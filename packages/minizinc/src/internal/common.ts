import {EMethod, IModelParams, IDataObject, ITypeInfo, IModelMetaData, IEnumValue, isEnumValue, isSet, isSetRange} from '../interfaces';
import {normalize, parseMultiObjects} from './utils';
import {basename} from 'path';
import {ISolverInfo} from 'minizinc-js';

export function parseMethod(method: string) {
  switch (method) {
    case 'min': return EMethod.MINIMIZE;
    case 'max': return EMethod.MAXIMIZE;
    default: return EMethod.SATISFY;
  }
}

export function toFileNames(fileName: string | undefined, tmpDir: string, sep: string, data?: IDataObject | string) {
  const name = basename(fileName || 'model', '.mzn');
  const modelFile = `${tmpDir}${sep}${name}.mzn`;
  const dataFile = `${tmpDir}${sep}${name}.${typeof data === 'string' ? 'dzn' : 'json'}`;
  const oznFile = `${tmpDir}${sep}${name}.ozn`;
  const fznFile = `${tmpDir}${sep}${name}.fzn`;
  return {modelFile, dataFile, oznFile, fznFile};
}

export function toSolveArguments(params: IModelParams, modelFile: string, dataFile: string, stats: boolean, data?: IDataObject | string) {
  const args: string[] = [
    '--allow-multiple-assignments'
  ];
  if (stats) {
    args.push('--solver-statistics');
  }

  if (params.all_solutions) {
    args.push('-a');
  } else if (params.nr_solutions != null) {
    args.push('-n', params.nr_solutions.toString());
  }
  if (params.processes != null) {
    args.push('-p', params.processes.toString());
  }
  if (params.random_seed != null) {
    args.push('-r', params.random_seed.toString());
  }
  if (params.free_search) {
    args.push('-f');
  }
  if (params.solver) {
    args.push('--solver', params.solver);
  }

  let code = params.model;


  args.push(modelFile);
  let filteredData: string = '';

  if (typeof data === 'string') {
    args.push(dataFile);
    filteredData = data;
  } else if (data) {
    data = Object.assign({}, data); // copy
    for (const [k, v] of Object.entries(data)) {
      if (isEnumValue(v)) {
        // enum value are not supported in json format yet
        code += `${k} = ${v.e};\n`;
        delete data[k];
      } else if (isSetRange(v)) {
        // range syntax
        code += `${k} = ${v.set[0][0]}..${v.set[0][1]};\n`;
        delete data[k];
      } else if (isSet(v) && v.set.length > 0 && isEnumValue(v.set[0]!)) {
        // set of enums
        code += `${k} = {${v.set.map((vi) => (<IEnumValue>vi).e).join(', ')}};\n`;
        delete data[k];
      }
    }

    if (Object.keys(data).length > 0) {
      args.push(dataFile);
      filteredData = JSON.stringify(data, null, 2);
    }
  }

  return {
    args,
    model: code,
    filteredData
  };
}

export function toAnalyzeArguments(modelFile: string, params: IModelParams) {
  const args: string[] = ['--model-interface-only', '--model-types-only', modelFile];
  if (params.solver) {
    args.push('--solver', params.solver);
  }
  return {
    args
  };
}

function checkIsFlag(stdFlags: string[]) {
  return (flag: string) => stdFlags.includes(flag);
}

export function toSolverArguments(params: IModelParams, info: ISolverInfo, stats: boolean = true) {
  const args: string[] = [];
  const isValid = typeof info.SOLVER_MSC_FILE === 'string' || !info.SOLVER_MSC_FILE.stdFlags ? () => true : checkIsFlag(info.SOLVER_MSC_FILE.stdFlags);

  if (params.all_solutions && isValid('-a')) {
    args.push('-a');
  } else if (params.nr_solutions != null && isValid('-n')) {
    args.push('-n', params.nr_solutions.toString());
  }
  if (params.processes != null && isValid('-p')) {
    args.push('-p', params.processes.toString());
  }
  if (params.random_seed != null && isValid('-r')) {
    args.push('-r', params.random_seed.toString());
  }
  if (params.free_search && isValid('-f')) {
    args.push('-f');
  }
  if (stats && isValid('-s')) {
    args.push('-s'); // stats
  }
  args.push('-'); // use stdin
  return args;
}

export function parseAnalyzeOutput(stdout: string): IModelMetaData {
  const output = normalize(stdout);
  const outputs = parseMultiObjects(output);

  const interfaceDesc = JSON.parse(outputs[0]);
  const modelTypes = JSON.parse(outputs[1]);
  const varTypes = modelTypes.var_types;


  const vars: ITypeInfo[] = Object.entries(varTypes.vars).map(([key, v]) => Object.assign({key, dim: 0}, <any>v));

  const byVar = new Map(vars.map((v) => <[string, ITypeInfo]>[v.key, v]));

  return {
    vars,
    inputs: Object.entries(interfaceDesc.input).map(([key, v]) => Object.assign({key, dim: 0}, byVar.get(key) || {}, <any>v)),
    outputs: Object.entries(interfaceDesc.output).map(([key, v]) => Object.assign({key, dim: 0}, byVar.get(key) || {}, <any>v)),
    enums: varTypes.enums || [],
    method: parseMethod(interfaceDesc.method)
  };
}
