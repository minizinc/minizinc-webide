
export function streamBody<T>(r: Response, parseChunk: (value: string, ended: boolean)=>string, onFinish: () => T): Promise<T> {
  const reader = r.body!.getReader();

  const decoder = new TextDecoder();

  let output = '';
  const readBlock = ({done, value}: ReadableStreamReadResult<Uint8Array>): T | Promise<T> => {
    if (done) {
      parseChunk(output, true);
      return onFinish();
    }

    output = parseChunk(output + decoder.decode(value!), false);

    return reader.read().then(readBlock);
  };
  return reader.read().then(readBlock);
}


export function parseNDJSON<T>(r: Response, onChunk: (obj: any)=>void, onFinish: () => T): Promise<T> {
  const parseChunk = (value: string, ended: boolean) => {
    let start = 0;
    let end = -1;
    do {
      end = value.indexOf('\n', start);
      if (end < 0 && ended) {
        end = value.length;
      }
      if (end < 0) {
        break;
      }
      const line = value.slice(start, end);
      start = end + 1;
      if (line.trim().length > 0) {
        const obj = JSON.parse(line);
        onChunk(obj);
      }
    } while (start < value.length);

    return start < value.length ? value.slice(start) : '';
  };
  return streamBody(r, parseChunk, onFinish);
}
