import {spawn} from 'child_process';
import {statSync, existsSync, ensureDir, outputFile, readFile} from 'fs-extra';
import {AbortException, IAbortSignal} from './abort';
import {delimiter, resolve} from 'path';
import {IRunResult, ISimpleFS} from 'emscripten_wrapper';


let miniZincPath: string | null | undefined = null;

function findMiniZinc() {
  const paths = [
    './',
    './minizinc/',
    '../',
    '../minizinc/'
  ].concat(process.env.PATH!.split(delimiter));

  let suffix = '';
  if (process.platform === 'win32' || process.platform === 'cygwin') {
    suffix = '.exe';
    paths.unshift(
      'c:/Program Files/MiniZinc',
      'c:/Program Files/MiniZinc IDE (bundled)',
      'c:/Program Files (x86)/MiniZinc',
      'c:/Program Files (x86)/MiniZinc IDE (bundled)'
    );
  } else if (process.platform === 'darwin') {
    paths.unshift(
      '/Applications/MiniZincIDE.app/Contents/Resources',
      resolve('~/Applications/MiniZincIDE.app/Contents/Resources')
    );
  }

  return paths.map((d) => resolve(d, `minizinc${suffix}`)).find((d) => existsSync(d) && statSync(d).isFile());
}

export interface ISpawnResultBuilder<T> {
  pushSTDOut(chunk: string): void;
  pushSTDErr(chunk: string): void;
  build(code: number): T;
}

export class DefaultSpawnResultBuilder implements ISpawnResultBuilder<IRunResult> {
  private stdout: string = '';
  private stderr: string = '';

  pushSTDOut(chunk: string) {
    this.stdout += chunk;
  }
  pushSTDErr(chunk: string) {
    this.stderr += chunk;
  }
  build(code: number) {
    return {
      exitCode: code,
      stdout: this.stdout,
      stderr: this.stderr
    };
  }
}

export function isCLIAvailable() {
  if (miniZincPath === null) {
    miniZincPath = findMiniZinc();
  }
  return miniZincPath != null;
}

export function runBuilder<T>(args: string[], builder: ISpawnResultBuilder<T>, stdin?: string, signal?: IAbortSignal): Promise<T> {
  if (miniZincPath === null) {
    miniZincPath = findMiniZinc();
  }
  if (!miniZincPath) {
    builder.pushSTDErr('MiniZinc not found');
    return Promise.resolve(builder.build(1));
  }

  return new Promise<T>((resolve, reject) => {
    const c = spawn(miniZincPath!, args);
    if (stdin) {
      c.stdin.write(stdin);
    }
    let wasAborted = false;
    c.stdout.on('data', (chunk) => {
      // console.log('stdout', chunk.toString());
      builder.pushSTDOut(chunk);
    });
    c.stderr.on('data', (chunk) => {
      // console.log('stderr', chunk.toString());
      builder.pushSTDErr(chunk);
    });
    c.on('error', (error) => reject(error));
    c.on('exit', (code) => {
      if (wasAborted) {
        return;
      }
      try {
        resolve(builder.build(code || 0));
      } catch (error) {
        reject(error);
      }
    });
    if (!signal) {
      return;
    }
    signal.on('abort', () => {
      wasAborted = true;
      c.kill();
      console.log('abort detected, reject with exception');
      reject(new AbortException());
    });
  });
}

export default function run(args: string[], stdin?: string, signal?: IAbortSignal) {
  return runBuilder(args, new DefaultSpawnResultBuilder(), stdin, signal);
}

export const simpleFileSystem: ISimpleFS = {
  ensureDir: (dir) => ensureDir(dir).then(() => <true>true),
  readBinaryFile: (path) => readFile(path),
  readTextFile: (path) => readFile(path, {encoding: 'utf-8'}).then((p) => p.toString()),
  writeBinaryFile: (path, content) => outputFile(path, content).then(() => <true>true),
  writeTextFile: (path, content) => outputFile(path, content, {encoding: 'utf-8'}).then(() => <true>true),
};
