import {IRunResult, ISimpleFS} from 'emscripten_wrapper';
import {BackendException, ISolverConfiguration, IModelParams} from '../interfaces';
import parseError from '../output/parseError';
import {AbortException, IAbortSignal} from './abort';
import {parseAnalyzeOutput, toAnalyzeArguments} from './common';
import {normalize} from './utils';

export interface IRunner {
  run(args: string[], stdin?: string): Promise<IRunResult> | IRunResult;
}

export async function resolveSolvers(mzn: IRunner): Promise<ISolverConfiguration[]> {
  const r = await mzn.run(['--solvers-json']);

    if (r.exitCode) {
      // error case
      throw new BackendException([{
        type: 'unknown',
        message: r.stderr
      }]);
    }
    const output = normalize(r.stdout);

    return JSON.parse(output);
}

export function version(mzn: IRunner) {
  return Promise.resolve(mzn.run(['--version'])).then((out) => out.stdout);
}

export async function analyzeImpl(mzn: IRunner, params: IModelParams, extraArgs: string[], modelFile: string, signal?: IAbortSignal) {
  const {args} = toAnalyzeArguments(modelFile, params);
  const r = await mzn.run(args.concat(extraArgs));

  if (signal && signal.aborted) {
    throw new AbortException();
  }

  if (r.exitCode) {
    // error case
    throw new BackendException(parseError(r.stderr, modelFile));
  }
  return parseAnalyzeOutput(r.stdout);
}

export async function createExtraIncludeFiles(tmpDir: string, sep: string, simpleFileSystem: ISimpleFS, extraIncludeFiles: {[name: string]: string}) {
  const fileNames = Object.keys(extraIncludeFiles);
  if (fileNames.length === 0) {
    return Promise.resolve({
      includePath: null,
      cleanup: null
    });
  }

  const dir = `${tmpDir}${sep}include`;

  await simpleFileSystem.ensureDir(dir);
  await Promise.all(fileNames.map((d) => simpleFileSystem.writeTextFile(`${dir}${sep}${d}`, extraIncludeFiles[d])));

  return {
    includePath: dir,
    cleanup: () => {
      return Promise.all(fileNames.map((d) => simpleFileSystem.writeTextFile(`${dir}${sep}${d}`, '')));
    }
  };
}
