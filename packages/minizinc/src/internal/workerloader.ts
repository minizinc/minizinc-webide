import {IMiniZincConstructorOptions} from '../interfaces';
import WebWorkerMiniZinc from '../WebWorkerMiniZinc';
import MZN, {IMiniZincWorkerClient, ISolverWorkerClient} from 'minizinc-js/bundle';
import GECODE from 'gecode-js/bundle';
import MiniZincWorker from 'worker-loader?name=minizinc.worker.js!minizinc-js/worker';
import GecodeWorker from 'worker-loader?name=gecode.worker.js!gecode-js/worker';
import {ISolverWorkerFactory} from './WorkerMiniZinc';

export interface IWorkerLoaderOptions extends IMiniZincConstructorOptions {
  solvers: string[];
}

export default function createMiniZinc(options: Partial<IWorkerLoaderOptions> = {}) {
  const solvers: ISolverWorkerFactory[] = [];
  const include = new Set(options.solvers || []);
  if (include.has(GECODE.SOLVER_ID) || include.has('gecode') || include.size === 0) {
    solvers.push({
      info: GECODE,
      create: () => <ISolverWorkerClient>GECODE.createWorkerClient(new GecodeWorker())
    });
  }
  // TODO support more
  if (solvers.length === 0) {
    console.error('no solver loaded');
  }
  return new WebWorkerMiniZinc({
    mzn: () => <IMiniZincWorkerClient>MZN.createWorkerClient(new MiniZincWorker()),
    ...options,
    solvers
  });
}

// REST version
// import RESTMiniZinc from 'minizinc/build/RESTMiniZinc';
// export const minizinc = new RESTMiniZinc({
//   ...common
// });

// Embedded version
// import EmbeddedMiniZinc from 'minizinc/build/EmbeddedMiniZinc';
// import MZN from 'minizinc-js/bundle';
// import GECODE from 'gecode-js/bundle';

// export const minizinc = new EmbeddedMiniZinc({
//   solvers: [GECODE],
//   mzn: MZN,
//   ...common
// });

// WebWorker Embedded version
