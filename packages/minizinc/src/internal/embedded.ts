import {ISolverInfo} from 'minizinc-js';
import {IModelParams, IMiniZincOptions, IModelMetaData, IDataObject, IMiniZincSolveOptions, IResult, BackendException, IMiniZincConstructorOptions, IMiniZincSolveRawOptions} from '../interfaces';
import {analyzeImpl, IRunner, createExtraIncludeFiles} from './logic';
import {AbortException} from './abort';
import {randomBaseDir} from './utils';
import {ISimpleFS, IEMWOutStream} from 'emscripten_wrapper';
import {toFileNames, toSolveArguments, toSolverArguments} from './common';
import parseError from '../output/parseError';
import {ResultBuilder, findOutputSeparator} from '../output/result';

export interface IFSRunner extends IRunner {
  simpleFileSystem: ISimpleFS;
  stdout: IEMWOutStream;
  stderr: IEMWOutStream;

  threaded?: boolean;
}

export interface IMZNRunner extends IFSRunner {
  chooseSolver(solvers: IFSSolverRunner[], solver?: string): IFSSolverRunner | null;
}

export declare type IFSSolverRunner = IFSRunner & ISolverInfo;

export async function analyzeEmbedded(mzn: IFSRunner, solvers: ISolverInfo[], paramsOrCode: string | IModelParams, options: Partial<IMiniZincOptions> = {}, coptions: Partial<IMiniZincConstructorOptions> = {}): Promise<IModelMetaData> {
  const params = typeof paramsOrCode === 'string' ? {model: paramsOrCode} : paramsOrCode;

  if (!params.solver && solvers.length > 0) {
    // default solver if available
    params.solver = solvers[0]!.SOLVER_ID;
  }

  const tmpDir = randomBaseDir();

  const {modelFile} = toFileNames(options.fileName, tmpDir, '/');

  let cleanUpExtraIncludeFiles: (() => void) | null = null;
  try {
    await mzn.simpleFileSystem.ensureDir(tmpDir);
    await mzn.simpleFileSystem.writeTextFile(modelFile, params.model);

    const {includePath, cleanup} = await createExtraIncludeFiles(tmpDir, '/', mzn.simpleFileSystem, Object.assign({}, coptions.extraIncludeFiles || {}, options.extraIncludeFiles || {}));

    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }
    const extraArgs: string[] = [];
    if (includePath) {
      extraArgs.push('-I', includePath);
      cleanUpExtraIncludeFiles = cleanup;
    }
    return analyzeImpl(mzn, params, extraArgs, modelFile, options.signal);
  } finally {
    // cleanup file system tho there is no delete so write with 0
    await mzn.simpleFileSystem.writeTextFile(modelFile, '');
    if (cleanUpExtraIncludeFiles) {
      await cleanUpExtraIncludeFiles();
    }
  }
}

interface IEmbeddedSolveBuilder<T> {
  streamResults: boolean;
  onCompileError(stderr: string): T | never;
  onSolveError(stderr: string): T | never;
  onOutputError(stderr: string): T | never;
  build(): T;
  stdout(chunk: string): void;
  stderr(chunk: string): void;
}

const DEBOUNCE_PARSE = 500; // debounce for 500ms before a partial parse attempt

interface IEmbeddedSolveBuilderFactory<T> {
  (params: IModelParams, includePath: string | null, modelFile: string, dataFile: string): Promise<IEmbeddedSolveBuilder<T>>;
}


class ReturnException extends Error {
  constructor(public readonly out: any) {
    super();
  }
}

interface IEmbeddedArgs {
  mzn: IMZNRunner;
  solvers: IFSSolverRunner[];
  paramsOrCode: IModelParams | string;
  data?: IDataObject | string;
  options: Partial<IMiniZincOptions>;
  coptions: Partial<IMiniZincConstructorOptions>;
  stats: boolean;
}

function nextParseChunk(out: string) {
  const seps = findOutputSeparator(out, 0, true);
  if (seps.index < 0) {
    return;
  }
  return out.slice(0, seps.index + seps.sep.length);
}


async function solveCommonEmbedded<T>(createBuilder: IEmbeddedSolveBuilderFactory<T>, {mzn, solvers, paramsOrCode, data, options, coptions, stats}: IEmbeddedArgs): Promise<T> {
  const params = typeof paramsOrCode === 'string' ? {model: paramsOrCode} : paramsOrCode;

  if (!params.solver && solvers.length > 0) {
    params.solver = solvers[0]!.SOLVER_ID;
  }
  const solver = mzn.chooseSolver(solvers, params.solver);
  // if solver == null it means it is an embedded one otherwise an external one

  const tmpDir = randomBaseDir();

  const {modelFile, dataFile, oznFile, fznFile} = toFileNames(options.fileName, tmpDir, '/', data);
  const {args, model, filteredData} = toSolveArguments(params, modelFile, dataFile, stats, data);

  // console.log(modelFile, dataFile, oznFile, fznFile, args, model, filteredData, gecodeArgs);

  if (options.signal && options.signal.aborted) {
    throw new AbortException();
  }

  //console.log('write file');

  await mzn.simpleFileSystem.ensureDir(tmpDir);

  await mzn.simpleFileSystem.writeTextFile(modelFile, model);
  if (filteredData) {
    await mzn.simpleFileSystem.writeTextFile(dataFile, filteredData);
  }

  const {includePath, cleanup} = await createExtraIncludeFiles(tmpDir, '/', mzn.simpleFileSystem, Object.assign({}, coptions.extraIncludeFiles || {}, options.extraIncludeFiles || {}));

  if (options.signal && options.signal.aborted) {
    throw new AbortException();
  }

  //console.log('analyzse file');
  if (includePath) {
    args.push('-I', includePath);
  }

  const builder: IEmbeddedSolveBuilder<T> = await createBuilder(params, includePath, modelFile, dataFile);

  if (!solver) {
    // it is an embedded solver, so do the shortcut
    try {
      mzn.stdout.on('data', builder.stdout);
      mzn.stderr.on('data', builder.stderr);
      const output = await mzn.run(args);
      if (output.exitCode) {
        return builder.onSolveError(output.stderr);
      }
      return builder.build();
    } catch (error) {
      if (error instanceof BackendException) {
        throw error;
      }
      return builder.onSolveError(String(error));
    } finally {
      mzn.stdout.off('data', builder.stdout);
      mzn.stderr.off('data', builder.stderr);
      // cleanup file system tho there is no delete so write with 0
      await Promise.all(<Promise<any>[]>[
        mzn.simpleFileSystem.writeTextFile(modelFile, ''),
        filteredData ? mzn.simpleFileSystem.writeTextFile(dataFile, '') : Promise.resolve(null),
        cleanup ? cleanup() : Promise.resolve(null),
      ]);
    }
  }

  // mzn 2 fzn

  let fznContent: string = '';
  try {
    args.push('--compile');
    args.push('--output-fzn-to-file', fznFile);
    args.push('--output-ozn-to-file', oznFile);
    const compiled = await mzn.run(args);
    if (compiled.exitCode) {
      return builder.onCompileError(compiled.stderr);
    }
    builder.stdout(compiled.stdout);
    builder.stderr(compiled.stderr);

    fznContent = await mzn.simpleFileSystem.readTextFile(fznFile);
  } catch (error) {
    if (error instanceof BackendException) {
      throw error;
    }
    return builder.onCompileError(String(error));
  } finally {
    // cleanup file system tho there is no delete so write with 0
    await Promise.all(<Promise<any>[]>[
      mzn.simpleFileSystem.writeTextFile(modelFile, ''),
      filteredData ? mzn.simpleFileSystem.writeTextFile(dataFile, '') : Promise.resolve(null),
      cleanup ? cleanup() : Promise.resolve(null),
      mzn.simpleFileSystem.writeTextFile(fznFile, '')
    ]);
  }

  /**
   * parses the given part of the output into a solution
   * (with optional retry)
   */
  const parseOutput = (output: string, last: boolean): Promise<T | null> => {
    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }
    const exe = mzn.run(['--solver', params.solver!, '--ozn-file', oznFile], output);
    return Promise.resolve(exe).then((r) => {
      if (r.exitCode) {
        return builder.onOutputError(r.stderr);
      }
      return last ? builder.build() : null;
    }).catch((error) => {
      if (error instanceof BackendException) {
        throw error;
      }
      return builder.onOutputError(String(error));
    });
  };

  const solve = (solverInstance: IFSSolverRunner, stdout: (chunk: string) => void): Promise<null | T> => {
    const solverArgs = toSolverArguments(params, solver, stats);
    solverInstance.stdout.on('data', stdout);
    // run Gecode
    const exe = solverInstance.run(solverArgs, fznContent);
    return Promise.resolve(exe).then((r) => {
      solverInstance.stdout.off('data', stdout);
      if (r.exitCode) {
        return builder.onSolveError(r.stderr);
      }
      builder.stderr(r.stderr);
      return null;
    }).catch((error) => {
      solverInstance.stdout.off('data', stdout);

      if (error instanceof ReturnException) {
        return <T>error.out;
      }
      if (error instanceof BackendException) {
        throw error;
      }
      return builder.onSolveError(String(error));
    });
  };

  try {
    if (options.signal && options.signal.aborted) {
      throw new AbortException();
    }

    if (!builder.streamResults || !mzn.threaded) {
      // keep is simple execute without streaming attempt
      let acc: string = '';
      const r = await solve(solver, (chunk) => acc += chunk);
      if (r) {
        return r;
      }
      mzn.stdout.on('data', builder.stdout);
      mzn.stderr.on('data', builder.stderr);
      return (await parseOutput(acc, true))!;
    }


    let solutionStream: string = '';
    let debouncePartialParsing: number = -1; // -1 no waiting
    let partialParseRunning: Promise<any> | null = null;
    let partialParseError: any | null = null;

    const parsePartial = (anotherRound: ()=>void) => {
      debouncePartialParsing = -1;
      const chunk = nextParseChunk(solutionStream);
      if (!chunk || partialParseRunning) {
        return;
      }

      // can partially parse the output
      // remove chunk
      solutionStream = solutionStream.slice(chunk.length);
      // parse sub
      const r = parseOutput(chunk, false);
      partialParseRunning = r;

      r.then((out) => {
        partialParseRunning = null;

        if (out) {
          partialParseError = new ReturnException(out);
          return;
        }

        anotherRound();
      });
      r.catch((error) => {
        partialParseRunning = null;
        partialParseError = error;
      });
    };

    const parsePartialDebounced = () => {
      if (!solutionStream || !nextParseChunk(solutionStream) || partialParseRunning) {
        return;
      }
      // have a chunk, debounce
      // clear old for debouncing
      if (debouncePartialParsing >= 0) {
        clearTimeout(debouncePartialParsing);
        debouncePartialParsing = -1;
      }
      debouncePartialParsing = self.setTimeout(parsePartial, DEBOUNCE_PARSE, parsePartialDebounced);
    };

    const collectSolutionStream = (chunk: string) => {
      if (partialParseError) {
        throw partialParseError;
      }

      solutionStream += chunk;
      parsePartialDebounced();
    };

    mzn.stdout.on('data', builder.stdout);
    mzn.stderr.on('data', builder.stderr);
    // solve and parse partial
    const r = await solve(solver, collectSolutionStream);
    if (r) {
      return r;
    }
    // solved but maybe not all yet parsed

    // get rest
    const rest = solutionStream;
    solutionStream = '';

    // stop sheduled
    if (debouncePartialParsing >= 0) {
      clearTimeout(debouncePartialParsing);
      debouncePartialParsing = -1;
    }

    // wait to be completed
    if (partialParseRunning) {
      await partialParseRunning;
    }

    if (partialParseError instanceof ReturnException) {
      return <T>partialParseError.out;
    }
    if (partialParseError) {
      throw partialParseError;
    }

    if (!rest) {
      return builder.build();
    }

    // have to run it one more time
    return (await parseOutput(rest, true))!;
  } finally {
    mzn.stdout.off('data', builder.stdout);
    mzn.stderr.off('data', builder.stderr);
    await mzn.simpleFileSystem.writeTextFile(oznFile, '');
  }
}

export async function solveEmbedded(mzn: IMZNRunner, solvers: IFSSolverRunner[], paramsOrCode: IModelParams | string, data?: IDataObject | string, options: Partial<IMiniZincSolveOptions> = {}, coptions: Partial<IMiniZincConstructorOptions> = {}): Promise<IResult> {
  const createBuilder: IEmbeddedSolveBuilderFactory<IResult> = async (params, includePath, modelFile, dataFile) => {
    const extraArgs: string[] = [];
    if (includePath) {
      extraArgs.push('-I', includePath);
    }
    const analyzed = await analyzeImpl(mzn, params, extraArgs, modelFile, options.signal);

    const builder = new ResultBuilder(analyzed, params, options.onPartialResult);
    let stderr = '';
    return {
      streamResults: options.onPartialResult != null,
      stdout: (chunk: string) => builder.push(chunk),
      stderr: (chunk: string) => stderr += chunk,
      onCompileError: (stderr: string) => {
        throw new BackendException(parseError(stderr, modelFile, dataFile));
      },
      onSolveError: (stderr: string) => {
        throw new BackendException(parseError(stderr, modelFile, dataFile));
      },
      onOutputError: (stderr: string) => {
        throw new BackendException(parseError(stderr, modelFile, dataFile));
      },
      build: builder.build.bind(builder)
    };
  };

  return solveCommonEmbedded(createBuilder, {mzn, solvers, paramsOrCode, data, options, coptions, stats: true});
}

export async function solveRawEmbedded(mzn: IMZNRunner, solvers: IFSSolverRunner[], paramsOrCode: IModelParams | string, data?: IDataObject | string, options: Partial<IMiniZincSolveRawOptions> = {}, coptions: Partial<IMiniZincConstructorOptions> = {}): Promise<string> {
  const createBuilder: IEmbeddedSolveBuilderFactory<string> = () => {
    let stdout = '';
    const onChunk = (chunk: string) => {
      stdout += chunk;
      if (options.onChunk && chunk) {
        options.onChunk(chunk);
      }
    };
    return Promise.resolve({
      streamResults: options.onChunk != null,
      stdout: onChunk,
      stderr: (chunk: string) => stdout += chunk,
      onCompileError: (stderr: string) => {
        return stderr;
      },
      onSolveError: (stderr: string) => {
        return stderr;
      },
      onOutputError: (stderr: string) => {
        return stderr;
      },
      build: () => stdout
    });
  };

  return solveCommonEmbedded(createBuilder, {mzn, solvers, paramsOrCode, data, options, coptions, stats: options.stats !== false});
}
