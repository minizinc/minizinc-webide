import {EventEmitter} from 'events';

export interface IAbortSignal {
  readonly aborted: boolean;

  on(event: 'abort', listener: () => void): void;
  once(event: 'abort', listener: () => void): void;
  off(event: 'abort', listener: () => void): void;
}


export default class AbortSignal extends EventEmitter implements IAbortSignal {
  aborted: boolean = false;

  abort() {
    if (this.aborted) {
      return;
    }
    this.aborted = true;
    this.emit('abort');
  }
}

export class AbortException extends Error {
  constructor(message = 'aborted') {
    super(message);
  }
}
