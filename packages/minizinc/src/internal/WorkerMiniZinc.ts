import {IMiniZincWorkerClient, ISolverWorkerClient, ISolverInfo} from 'minizinc-js';
import {IDataObject, IMiniZinc, IMiniZincOptions, IMiniZincSolveOptions, IModelMetaData, IModelParams, IResult, IMiniZincConstructorOptions, IMiniZincSolveRawOptions} from '../interfaces';
import {analyzeEmbedded, solveEmbedded, solveRawEmbedded, IMZNRunner, IFSRunner, IFSSolverRunner} from '../internal/embedded';
import {resolveSolvers, version} from '../internal/logic';
import {IAbortSignal, AbortException} from './abort';
import {bind} from 'decko';

export interface ISolverWorkerFactory {
  info: ISolverInfo;
  create(): ISolverWorkerClient;
}

export interface IWorkerMiniZincOptions extends Partial<IMiniZincConstructorOptions> {
  mzn: () => IMiniZincWorkerClient;
  solvers: ISolverWorkerFactory[];
}

class AbortAbleWorkerWrapper<T extends IMiniZincWorkerClient | ISolverWorkerClient> implements IFSRunner  {
  readonly threaded: boolean = true;

  constructor(protected _instance: T | null, protected readonly factory: () => T, protected readonly destroy: () => T|null, private readonly signal?: IAbortSignal) {

  }

  protected get instance() {
    if (this._instance) {
      return this._instance;
    }
    return this._instance = this.factory();
  }

  get simpleFileSystem() {
    return this.instance.simpleFileSystem;
  }

  get stdout() {
    return this.instance.stdout;
  }

  get stderr() {
    return this.instance.stderr;
  }

  run(args: string[], stdin?: string) {
    if (!this.signal) {
      return this.instance.run(args, stdin);
    }
    if (this.signal.aborted) {
      throw new AbortException();
    }
    const listener = () => {
      this._instance = this.destroy();
      throw new AbortException();
    };
    try {
      this.signal.on('abort', listener);
      return this.instance.run(args, stdin);
    } finally {
      this.signal.off('abort', listener);
    }
  }

  kill() {
    if (this._instance) {
      this._instance = this.destroy();
    }
  }
}

class AbortAbleMZNWorker extends AbortAbleWorkerWrapper<IMiniZincWorkerClient> implements IMZNRunner {
  chooseSolver(solvers: IFSSolverRunner[], solver?: string) {
    return this.instance.chooseSolver(solvers, solver);
  }
}
class AbortAbleSolverWorker extends AbortAbleWorkerWrapper<ISolverWorkerClient> implements IFSSolverRunner {
  constructor(private readonly info: ISolverInfo, _instance: ISolverWorkerClient | null, factory: () => ISolverWorkerClient, destroy: () => ISolverWorkerClient|null, signal?: IAbortSignal) {
    super(_instance, factory, destroy, signal);
  }

  get SOLVER_ID() {
    return this.info.SOLVER_ID;
  }
  get SOLVER_MSC_FILE() {
    return this.info.SOLVER_MSC_FILE;
  }
}

export default class WorkerMiniZinc implements IMiniZinc {
  protected mzn: IMiniZincWorkerClient;
  protected solvers: (ISolverWorkerClient|null)[];

  constructor(private readonly options: IWorkerMiniZincOptions) {
    this.mzn = options.mzn();
    this.solvers = options.solvers.map(() => null); // don't instantiate if we don't need them
    // register them anyhow
    for (const s of options.solvers) {
      this.mzn.registerSolver(s.info);
    }
  }

  private abortAbleMzn(signal?: IAbortSignal) {
    if (!signal) {
      return this.mzn;
    }
    return new AbortAbleMZNWorker(this.mzn, this.recreateMzn, this.recreateMzn, signal);
  }

  @bind
  private recreateMzn() {
    this.mzn.kill();
    this.mzn = this.options.mzn();
    for (const s of this.options.solvers) {
      this.mzn.registerSolver(s.info);
    }
    return this.mzn;
  }

  private abortAbleSolvers(signal?: IAbortSignal): IFSSolverRunner[] {
    return this.options.solvers.map((info, index) => {
      const createSolver = () => {
        if (this.solvers[index]) {
          this.solvers[index]!.kill();
        }
        return this.solvers[index] = info.create();
      };
      const destroySolver = () => {
        if (this.solvers[index]) {
          this.solvers[index]!.kill();
        }
        return this.solvers[index] = null; // don't create yet
      };
      return new AbortAbleSolverWorker(info.info, this.solvers[index], createSolver, destroySolver, signal);
    });
  }

  solve(paramsOrCode: IModelParams | string, data?: IDataObject | string, options: Partial<IMiniZincSolveOptions> = {}): Promise<IResult> {
    return solveEmbedded(this.abortAbleMzn(options.signal), this.abortAbleSolvers(options.signal), paramsOrCode, data, options, this.options);
  }

  solveRaw(paramsOrCode: string | IModelParams, data?: IDataObject | string, options: Partial<IMiniZincSolveRawOptions> = {}): Promise<string> {
    return solveRawEmbedded(this.abortAbleMzn(options.signal), this.abortAbleSolvers(options.signal), paramsOrCode, data, options, this.options);
  }

  analyze(paramsOrCode: string | IModelParams, options: Partial<IMiniZincOptions> = {}): Promise<IModelMetaData> {
    return analyzeEmbedded(this.abortAbleMzn(options.signal), this.options.solvers.map(((s) => s.info)), paramsOrCode, options, this.options);
  }

  resolveSolvers() {
    return resolveSolvers(this.mzn);
  }

  version() {
    return version(this.mzn);
  }

  terminate() {
    this.mzn.kill();
    for (const s of this.solvers) {
      if (s) {
        s.kill();
      }
    }
    return Promise.resolve(true);
  }
}
