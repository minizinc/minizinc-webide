import {IModelMetaData, IError, IModelParams, IDataObject, IResult, IMiniZincOptions, ISolverConfiguration, IMiniZincSolveOptions, IMiniZincSolveRawOptions} from '../interfaces';

export const EVENT_ANALYZE = 'analyze';
export const EVENT_ABORT = 'abort';
export const EVENT_VERSION = 'version';
export const EVENT_TERMINATE = 'terminate';
export const EVENT_SOLVE_SOLUTION = 'solveSolution';
export const EVENT_SOLVE = 'solve';
export const EVENT_SOLVE_RAW = 'solveRaw';
export const EVENT_SOLVE_RAW_CHUNK = 'solveRawChunk';
export const EVENT_RESOLVE_SOLVERS = 'resolveSolvers';

export interface IVersionRequest {
  key: string;
}

export interface IVersionResponse {
  error?: IError[];
  version?: string;
}

export interface ITerminateRequest {
  key: string;
}

export interface ITerminateResponse {
  error?: IError[];
  ok?: boolean;
}

export interface IAnalyzeRequest {
  key: string;
  code: string | IModelParams;
  options?: IMiniZincOptions;
}

export interface IAnalyzeResponse {
  error?: IError[];
  metaData?: IModelMetaData;
}


export interface ISolveRequest {
  key: string;
  code: string | IModelParams;
  data?: IDataObject;
  options?: IMiniZincSolveOptions;
  streamSolutions?: boolean;
}

export interface ISolveResponse {
  error?: IError[];
  result: IResult;
}

export interface ISolveSolutionResponse {
  type: 'solution' | 'header';
  result: IResult;
}

export interface ISolveRawRequest {
  key: string;
  code: string | IModelParams;
  data?: IDataObject;
  options?: IMiniZincSolveRawOptions;
  streamChunks?: boolean;
}

export declare type ISolveRawResponse = string;

export interface IResolveSolversRequest {
  key: string;
}

export interface IResolveSolversResponse {
  error?: IError[];
  solvers: ISolverConfiguration[];
}
