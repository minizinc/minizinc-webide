import {ISolverModule} from 'minizinc-js';

// first real argument (node, bootmodule) is the module to load
// tslint:disable-next-line: no-var-requires
const BASE = require(process.argv[2]);

(<ISolverModule>BASE).createWorker({
  addEventListener: (_type, listener) => {
    process.on('message', (m) => {
      listener(m, (msg) => process.send!(msg));
    });
  },
  postMessage: (msg) => {
    process.send!(msg);
  }
});
