import {IMiniZinc, ProxyMiniZinc, IMiniZincConstructorOptions} from './interfaces';

// detect environment
const ENVIRONMENT_IS_WEB = typeof window === 'object';
declare function importScripts(): void;
const ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
// const ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function' && !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_WORKER;


export default function createDriver(options: Partial<IMiniZincConstructorOptions> = {}): IMiniZinc {
  if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
    // console.log('web environment detected use embedded version');
    return new ProxyMiniZinc(() => import('./EmbeddedMiniZinc'), options);
  }
  // node environment thus CLI might be available, test async
  // console.log('node environment detected');
  return new ProxyMiniZinc(async () => {
    const cli = await import('./CLIMiniZinc');
    if (cli.isCLIAvailable()) {
      // console.log('found minizinc cli');
      return cli;
    }
    // fall back to forked version
    // console.log('fork worker fallback');
    return import('./ForkWorkerMiniZinc');
  }, options);
}
