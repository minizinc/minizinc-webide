
import createDriver from './driver';
import yargs, {Argv, Arguments} from 'yargs';
import {readFile} from 'fs-extra';
import {IModelParams, IDataObject} from './interfaces';

const driver = createDriver();


interface ISolveOptions {
  model?: string;
  data?: string;
  solver?: string;
  n?: number;
  f?: boolean;
  a?: boolean;
  parallel?: number;
  r?: number;

  raw?: boolean;
}

interface IAnalyzeOptions {
  model?: string;
  solver?: string;
}

async function solve(args: Arguments<ISolveOptions>) {
  const model = await readFile(args.model!).then((r) => r.toString());
  const params: IModelParams = {
    model
  };
  let data: IDataObject | string | undefined = undefined;

  if (args.data) {
    const r = await readFile(args.data).then((r) => r.toString());
    data = args.data.endsWith('.json') ? JSON.parse(r) : r;
  }
  if (args.solver) {
    params.solver = args.solver;
  }
  if (args.n != null) {
    params.nr_solutions = args.n;
  }
  if (args.f) {
    params.free_search = true;
  }
  if (args.a) {
    params.all_solutions = true;
  }
  if (args.parallel != null) {
    params.processes = args.parallel;
  }
  if (args.r != null) {
    params.random_seed = args.r;
  }

  if (args.raw) {
    const r = await driver.solveRaw(params, data);
    console.log(r);
  } else {
    const r = await driver.solve(params);
    console.log(JSON.stringify(r, null, 2));
  }
}

async function analyze(args: Arguments<ISolveOptions>) {
  const model = await readFile(args.model!).then((r) => r.toString());
  const params: IModelParams = {
    model
  };
  if (args.solver) {
    params.solver = args.solver;
  }

  const r = await driver.analyze(model);
  console.log(JSON.stringify(r, null, 2));
}

async function version() {
  console.log(await driver.version());
}

async function resolveSolvers() {
  console.log(await driver.resolveSolvers());
}

// tslint:disable-next-line: no-unused-expression
(<Argv<{}>>yargs(process.argv))
  .scriptName('minizinc-js')
  .usage('$0 <cmd> [args]')
  .command<ISolveOptions>('solve [model] [data]', 'solve a given problem', (yargs) => {
    return yargs.positional('model', {
      type: 'string',
      describe: 'the mzn model file'
    }).positional('data', {
      type: 'string',
      describe: 'optional data (mzn or json) data file'
    }).option('raw', {
      type: 'boolean',
      describe: 'output the raw output not the parsed one'
    }).option('r', {
      type: 'number',
      alias: 'random-seed',
      describe: 'specify random seed'
    }).option('a', {
      type: 'boolean',
      alias: 'all-solutions',
      describe: 'output all solutions'
    }).option('f', {
      type: 'boolean',
      alias: 'free-search',
      describe: 'do a free search'
    }).option('solver', {
      type: 'string',
      alias: 's',
      describe: 'specify the solver to use'
    }).option('parallel', {
      type: 'number',
      alias: 'p',
      describe: 'run with n parallel threads'
    }).option('n', {
      type: 'number',
      alias: 'num-solutions',
      describe: 'stop after reporting N solutions'
    });
  }, solve)
  .command<IAnalyzeOptions>('analyze [model]', 'analyze the given model', (yargs) => {
    return yargs.positional('model', {
      type: 'string',
      describe: 'the mzn model file'
    }).option('solver', {
      type: 'string',
      alias: 's',
      describe: 'specify the solver to use'
    });
  }, analyze)
  .command<{}>('resolveSolvers', 'resolves the available solvers', (yargs) => {
    return yargs;
  }, resolveSolvers)
  .command<{}>('version', 'shows version information', (yargs) => {
    return yargs;
  }, version)
  .help()
  .argv;
