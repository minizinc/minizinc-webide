import WorkerMiniZinc, {ISolverWorkerFactory} from './internal/WorkerMiniZinc';
import MINIZINC, {IMiniZincWorkerClient, ISolverWorkerClient, ISolverModule} from 'minizinc-js';
import GECODE from 'gecode-js';
import {IMiniZincConstructorOptions} from './interfaces';

export interface IWebWorkerMiniZincOptions extends IMiniZincConstructorOptions {
  /**
   * MiniZinc worker or script url
   */
  mzn: string | URL | (() => IMiniZincWorkerClient);
  /**
   * solver worker instances
   */
  solvers: ISolverWorkerFactory[];
}

function createSolver(module: ISolverModule, moduleName: string): ISolverWorkerFactory {
  return {
    info: module,
    create: () => <ISolverWorkerClient>module.createWorkerClient(new Worker(moduleName))
  };
}


export default class WebWorkerMiniZinc extends WorkerMiniZinc {
  constructor(options: Partial<IWebWorkerMiniZincOptions>) {
    super({
      mzn: typeof options.mzn === 'function' ? options.mzn : () => <IMiniZincWorkerClient>MINIZINC.createWorkerClient(new Worker((<string>options.mzn) || 'minizinc.worker.js')),
      solvers: options.solvers && options.solvers.length > 0 ? options.solvers : [
        createSolver(GECODE, 'gecode.worker.js')
      ],
      extraIncludeFiles: options.extraIncludeFiles
    });
  }
}
