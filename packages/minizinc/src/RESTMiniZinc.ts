import {IMiniZinc, IModelMetaData, IDataObject, IModelParams, BackendException, IMiniZincOptions, IMiniZincSolveOptions, IAbortSignal, IResult, EStatus, ISolverConfiguration, IMiniZincConstructorOptions, IMiniZincSolveRawOptions} from './interfaces';
import {parseNDJSON, streamBody} from './internal/ndjson';
import {buildOptions} from './internal/utils';

function handleError(r: Response, isJSON: boolean = true) {
  if (r.ok) {
    return (isJSON ? r.json() : r.text()).catch(handleGenericError);
  }
  return r.text().then((t) => {
    let result: any = t;
    if (isJSON) {
      try {
        result = JSON.parse(t);
      } catch {
        // dummy
      }
    }
    if (typeof result === 'string') {
      throw new BackendException([{
        type: 'unknown',
        message: result
      }]);
    }
    throw new BackendException(Array.isArray(result) ? result : [result]);
  });
}

function handleStream(onPartialResult: (type: 'solution' | 'header', result: IResult) => void) {
  return (r: Response) => {
    if (!r.ok) {
      return handleError(r);
    }

    const result: IResult = {
      outputs: [],
      header: {},
      solutions: [],
      complete: false,
      status: EStatus.UNKNOWN
    };

    const onChunk = (chunk: any) => {
      if (chunk.header) {
        Object.assign(result.header, chunk.header);
        onPartialResult('header', result);
      } else if (chunk.solution) {
        result.solutions.push(chunk.solution);
        onPartialResult('solution', result);
      } else if (chunk.result) {
        Object.assign(result, chunk.result, {solutions: result.solutions});
      }
    };

    return parseNDJSON<IResult>(r, onChunk, () => result).catch(handleGenericError);
  };
}


function handleTextStream(onChunk: (chunk: string) => void) {
  return (r: Response) => {
    if (!r.ok) {
      return handleError(r);
    }

    let acc: string = '';
    const onChunkImpl = (value: string) => {
      acc += value;
      onChunk(value);
      return '';
    };

    return streamBody<string>(r, onChunkImpl, () => acc).catch(handleGenericError);
  };
}

function handleGenericError(error: any) {
  if (error instanceof BackendException || Array.isArray(error.errors)) {
    throw error;
  }
  throw new BackendException([{
    type: 'unknown',
    message: String(error)
  }]);
}

function fetchAbort(signal?: IAbortSignal) {
  if (!signal) {
    return undefined;
  }
  const controller = new AbortController();
  signal.on('abort', () => controller.abort());
  return controller.signal;
}

export interface IRESTMiniZincOptions extends IMiniZincConstructorOptions {
  baseUrl: string;
}

export default class RESTMiniZinc implements IMiniZinc {
  constructor(private readonly options: Partial<IRESTMiniZincOptions>) {

  }

  private get baseUrl() {
    return this.options.baseUrl || `/v1.0`;
  }

  private buildOptions(options: Partial<IMiniZincOptions>) {
    return buildOptions(options, this.options);
  }

  analyze(code: string | IModelParams, options: Partial<IMiniZincOptions> = {}): Promise<IModelMetaData> {
    return fetch(`${this.baseUrl}/analyze`, {
      method: 'POST',
      body: JSON.stringify(Object.assign({}, typeof code === 'string' ? {model: code} : code, {
        options: this.buildOptions(options)
      }), null, 2),
      headers: {
        'Content-Type': 'application/json'
      },
      signal: fetchAbort(options.signal)
    }).then(handleError).catch(handleGenericError);
  }

  solve(code: string | IModelParams, data?: IDataObject | string, options: Partial<IMiniZincSolveOptions> = {}): Promise<IResult> {
    return fetch(`${this.baseUrl}/solve${options.onPartialResult ? '/stream' : ''}`, {
      method: 'POST',
      body: JSON.stringify(Object.assign({}, typeof code === 'string' ? {model: code} : code, {
        data,
        options: this.buildOptions(options)
      }), null, 2),
      headers: {
        'Content-Type': 'application/json'
      },
      signal: fetchAbort(options.signal)
    }).then(options.onPartialResult ? handleStream(options.onPartialResult) : handleError).catch(handleGenericError);
  }

  solveRaw(code: string | IModelParams, data?: IDataObject | string, options: Partial<IMiniZincSolveRawOptions> = {}): Promise<string> {
    return fetch(`${this.baseUrl}/solve/raw`, {
      method: 'POST',
      body: JSON.stringify(Object.assign({}, typeof code === 'string' ? {model: code} : code, {
        data,
        options: this.buildOptions(options)
      }), null, 2),
      headers: {
        'Content-Type': 'application/json'
      },
      signal: fetchAbort(options.signal),

    }).then(options.onChunk ? handleTextStream(options.onChunk) : (r) => handleError(r, false)).catch(handleGenericError);
  }

  resolveSolvers(): Promise<ISolverConfiguration[]> {
    return fetch(`${this.baseUrl}/resolveSolvers`, {
      method: 'GET'
    }).then(handleError).catch(handleGenericError);
  }

  version(): Promise<string> {
    return fetch(`${this.baseUrl}/version`, {
      method: 'GET'
    }).then((r) => handleError(r, false)).catch(handleGenericError);
  }

  terminate(): Promise<boolean> {
    return fetch(`${this.baseUrl}/terminate`, {
      method: 'POST'
    }).then((r) => handleError(r, false)).catch(handleGenericError);
  }
}
