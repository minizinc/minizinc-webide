import {IMiniZinc, IMiniZincOptions, IModelMetaData, BackendException, IModelParams, IDataObject, IMiniZincSolveOptions, IResult, IMiniZincSolveRawOptions, ISolverConfiguration, IMiniZincConstructorOptions} from '.';
import {EVENT_ANALYZE, IAnalyzeResponse, IAnalyzeRequest, EVENT_ABORT, ISolveSolutionResponse, EVENT_SOLVE, ISolveResponse, EVENT_SOLVE_SOLUTION, ISolveRequest, ISolveRawResponse, EVENT_SOLVE_RAW, EVENT_SOLVE_RAW_CHUNK, ISolveRawRequest, EVENT_RESOLVE_SOLVERS, IResolveSolversResponse, IResolveSolversRequest, EVENT_VERSION, IVersionResponse, IVersionRequest, EVENT_TERMINATE, ITerminateResponse, ITerminateRequest} from './internal/messages';
import {buildOptions} from './internal/utils';

export interface IChannelLike {
  send(channel: string, msg?: any): void;
  once(channel: string, listener: (evt: any, arg: any) => void): void;
  on(channel: string, listener: (evt: any, arg: any) => void): void;
  removeListener(channel: string, listener: (evt: any, arg: any) => void): void;
}

export interface IMessagingMiniZincOptions extends IMiniZincConstructorOptions {
  channel: IChannelLike;
  window?: Window;
}

export function createWindowChannel(receiver: Window, sender?: Window): IChannelLike {
  const s = sender || window;
  const onByChannel = new Map<string, Set<(evt: any, arg: any) => void>>();
  const onceByChannel = new Map<string, Set<(evt: any, arg: any) => void>>();

  const onMessage = (evt: MessageEvent) => {
    if (!evt.data || !evt.data.channel || !evt.source || evt.source !== receiver) {
      return;
    }
    const channel = evt.data.channel;
    const arg = evt.data.arg;
    if (onByChannel.has(channel)) {
      onByChannel.get(channel)!.forEach((l) => l(evt, arg));
    }
    if (!onceByChannel.has(channel)) {
      return;
    }
    const r = onceByChannel.get(channel)!;
    onceByChannel.delete(channel);
    r.forEach((l) => l(evt, arg));
  };

  return {
    send: (channel, msg) => {
      receiver.postMessage({channel, arg: msg}, '*');
    },
    on: (channel, listener) => {
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        s.addEventListener('message', onMessage);
      }
      if (!onByChannel.has(channel)) {
        onByChannel.set(channel, new Set());
      }
      onByChannel.get(channel)!.add(listener);
    },
    once: (channel, listener) => {
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        s.addEventListener('message', onMessage);
      }
      if (!onceByChannel.has(channel)) {
        onceByChannel.set(channel, new Set());
      }
      onceByChannel.get(channel)!.add(listener);
    },
    removeListener: (channel, listener) => {
      if (onByChannel.has(channel)) {
        const s = onByChannel.get(channel)!;
        s.delete(listener);
        if (s.size === 0) {
          onByChannel.delete(channel);
        }
      }
      if (onceByChannel.has(channel)) {
        const s = onceByChannel.get(channel)!;
        s.delete(listener);
        if (s.size === 0) {
          onceByChannel.delete(channel);
        }
      }
      if (onByChannel.size === 0 && onceByChannel.size === 0) {
        s.removeEventListener('message', onMessage);
      }
    }
  };
}

export default class MessagingMiniZinc implements IMiniZinc {
  private id = 0;

  constructor(private readonly channel: IChannelLike, private readonly options: Partial<IMiniZincConstructorOptions> = {}) {
  }

  analyze(code: string | IModelParams, options: Partial<IMiniZincOptions> = {}): Promise<IModelMetaData> {
    return new Promise<IModelMetaData>((resolve, reject) => {
      const key = `key${this.id++}`;
      this.channel.once(`${EVENT_ANALYZE}-${key}`, (_evt: any, arg: IAnalyzeResponse) => {
        if (arg.error) {
          reject(new BackendException(arg.error));
        } else {
          resolve(arg.metaData!);
        }
      });
      const safeOptions = buildOptions(options, this.options);
      this.channel.send(EVENT_ANALYZE, <IAnalyzeRequest>{key, code, options: safeOptions});

      if (!options.signal) {
        return;
      }
      options.signal.on('abort', () => this.channel.send(`${EVENT_ABORT}-${key}`));
    });
  }

  solve(code: string | IModelParams, data?: IDataObject, options: Partial<IMiniZincSolveOptions> = {}): Promise<IResult> {
    return new Promise<IResult>((resolve, reject) => {
      const key = `key${this.id++}`;
      const partialResultListener = (_evt: any, arg: ISolveSolutionResponse) => {
        if (options.onPartialResult) {
          options.onPartialResult(arg.type, arg.result);
        }
      };
      this.channel.once(`${EVENT_SOLVE}-${key}`, (_evt: any, arg: ISolveResponse) => {
        this.channel.removeListener(`${EVENT_SOLVE_SOLUTION}-${key}`, partialResultListener);

        if (arg.error) {
          reject(new BackendException(arg.error));
        } else {
          resolve(arg.result!);
        }
      });
      const safeOptions = buildOptions(options, this.options);
      if (options.onPartialResult) {
        this.channel.on(`${EVENT_SOLVE_SOLUTION}-${key}`, partialResultListener);
      }
      this.channel.send(EVENT_SOLVE, <ISolveRequest>{key, code, data, options: safeOptions, streamSolutions: options.onPartialResult != null});

      if (!options.signal) {
        return;
      }
      options.signal.on('abort', () => this.channel.send(`${EVENT_ABORT}-${key}`));
    });
  }

  solveRaw(code: string | IModelParams, data?: IDataObject, options: Partial<IMiniZincSolveRawOptions> = {}): Promise<string> {
    return new Promise<string>((resolve) => {
      const key = `key${this.id++}`;
      const chunkListener = (_evt: any, arg: ISolveRawResponse) => {
        if (options.onChunk) {
          options.onChunk(arg);
        }
      };
      this.channel.once(`${EVENT_SOLVE_RAW}-${key}`, (_evt: any, arg: ISolveRawResponse) => {
        this.channel.removeListener(`${EVENT_SOLVE_RAW_CHUNK}-${key}`, chunkListener);
        resolve(arg);
      });
      const safeOptions = buildOptions(options, this.options);
      if (options.onChunk) {
        this.channel.on(`${EVENT_SOLVE_RAW_CHUNK}-${key}`, chunkListener);
      }
      this.channel.send(EVENT_SOLVE_RAW, <ISolveRawRequest>{key, code, data, options: safeOptions, streamChunks: options.onChunk != null});

      if (!options.signal) {
        return;
      }
      options.signal.on('abort', () => this.channel.send(`${EVENT_ABORT}-${key}`));
    });
  }

  resolveSolvers(): Promise<ISolverConfiguration[]> {
    return new Promise<ISolverConfiguration[]>((resolve, reject) => {
      const key = `key${this.id++}`;
      this.channel.once(`${EVENT_RESOLVE_SOLVERS}-${key}`, (_evt: any, arg: IResolveSolversResponse) => {
        if (arg.error) {
          reject(new BackendException(arg.error));
        } else {
          resolve(arg.solvers!);
        }
      });
      this.channel.send(EVENT_RESOLVE_SOLVERS, <IResolveSolversRequest>{key});
    });
  }

  version(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const key = `key${this.id++}`;
      this.channel.once(`${EVENT_VERSION}-${key}`, (_evt: any, arg: IVersionResponse) => {
        if (arg.error) {
          reject(new BackendException(arg.error));
        } else {
          resolve(arg.version!);
        }
      });
      this.channel.send(EVENT_VERSION, <IVersionRequest>{key});
    });
  }

  terminate(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const key = `key${this.id++}`;
      this.channel.once(`${EVENT_TERMINATE}-${key}`, (_evt: any, arg: ITerminateResponse) => {
        if (arg.error) {
          reject(new BackendException(arg.error));
        } else {
          resolve(arg.ok!);
        }
      });
      this.channel.send(EVENT_TERMINATE, <ITerminateRequest>{key});
    });
  }
}
