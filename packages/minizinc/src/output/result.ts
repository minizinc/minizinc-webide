import {EMethod, EStatus, IModelMetaData, IModelParams, IResult, ISolution, IStatistics} from '../interfaces';
import {normalize, parseMultiObjects} from '../internal/utils';

function parseStatus(output: string, method: EMethod) {
  if (output.includes('=ERROR=')) {
    return EStatus.ERROR;
  }
  if (output.includes('=UNSATISFIABLE=')) {
    return EStatus.UNSATISFIABLE;
  }
  if (output.includes('=UNSATorUNBOUNDED=') || output.includes('=UNBOUNDED=')) {
    return EStatus.UNBOUNDED;
  }
  if (output.includes('==========')) {
    return method === EMethod.SATISFY ? EStatus.ALL_SOLUTIONS : EStatus.OPTIMAL_SOLUTION;
  }
  if (output.includes('----------')) {
    return method === EMethod.SATISFY ? EStatus.OPTIMAL_SOLUTION : EStatus.SATISFIED;
  }
  return EStatus.UNKNOWN;
}

function isComplete(status: EStatus, solutions: ISolution[], params: IModelParams, method: EMethod) {
  if (method === EMethod.SATISFY) {
    if (params.all_solutions) {
      return status === EStatus.ALL_SOLUTIONS;
    }
    if (params.nr_solutions) {
      return solutions.length === params.nr_solutions;
    }
    return status === EStatus.SATISFIED;
  }
  return status === EStatus.OPTIMAL_SOLUTION;
}


function parsePartialObject(line: string) {
  line = line.trim();
  if (line.length === 0) {
    return {r: null, line};
  }
  if (!line.startsWith('{')) {
    return {r: null, line};
  }
  const objects = parseMultiObjects(line);
  const r: any = {};
  let unparsed = '';
  for (const obj of objects) {
    try {
      Object.assign(r, JSON.parse(obj));
    } catch (error) {
      console.warn('cannot parse line: ', obj);
      unparsed = `${unparsed}${unparsed.length > 0 ? '\n' : ''}${obj}`;
    }
  }
  return {r, line: unparsed};
}

export function findOutputSeparator(output: string, start: number, findLast: boolean = false) {
  const separators = ['%'.repeat(10), '-'.repeat(10), '='.repeat(10)];

  if (findLast) {
    const rsep = separators.slice().reverse();
    for (const sep of rsep) {
      const index = output.lastIndexOf(sep, start);
      if (index >= 0) {
        return {index, sep, isHeader: sep === separators[0]};
      }
    }
  } else {
    for (const sep of separators) {
      const index = output.indexOf(sep, start);
      if (index >= 0) {
        return {index, sep, isHeader: sep === separators[0]};
      }
    }
  }
  return {index: -1, sep: separators[separators.length - 1], isHeader: false};
}


export class ResultBuilder {
  private header: object = {};
  private extraHeader: string = '';
  private output: string = '';
  private solutions: ISolution[] = [];
  private status: EStatus = EStatus.UNKNOWN;
  private statistics: IStatistics | undefined = undefined;

  constructor(private readonly meta: IModelMetaData, private readonly params: IModelParams, private readonly callback?: (type: 'header' | 'solution', result: IResult)=>void) {

  }

  push(output: string) {
    const bak = this.output + output;
    this.output = this.parsePartial(bak);
    // console.log(`partial: "${bak}" after: "${this.output}"`);
  }

  build(): IResult {
    // rest
    this.parsePartial(this.output, true);

    return this.buildResult();
  }

  private buildResult() {
    return {
      status: this.status,
      solutions: this.solutions,
      header: this.header,
      extraHeader: this.extraHeader,
      objective: this.solutions.length > 0 ? this.solutions[this.solutions.length - 1].objective : undefined,
      outputs: this.meta.outputs,
      complete: isComplete(this.status, this.solutions, this.params, this.meta.method),
      statistics: this.statistics
    };
  }

  private parseStats(output: string) {
    const lines = output.split('\n').filter((d) => d.trim().length > 0);
    const rest: string[] = [];
    const stat: any = {};
    for (const line of lines) {
      if (line.startsWith('%%%mzn-stat-end')) {
        continue;
      }
      // don't reuse regex to avoid multi line thing
      const re = /^%%%mzn-stat:? (.+)=([\d,.]+)$/gm;
      const match = re.exec(line);
      if (!match) {
        rest.push(line);
        continue;
      }
      stat[match[1]] = parseFloat(match[2]);
    }
    if (Object.keys(stat).length > 0) {
      this.statistics = Object.assign({}, stat);
    }
    return rest.join('\n');
  }

  private parsePartial(output: string, isAll: boolean = false) {
    const wasEndingWithNewLine = output.endsWith('\n');
    output = normalize(output);

    if (this.status === EStatus.UNKNOWN) {
      this.status = parseStatus(output, this.meta.method);
    } else if (this.meta.method === EMethod.SATISFY && this.status === EStatus.OPTIMAL_SOLUTION && parseStatus(output, this.meta.method) === EStatus.ALL_SOLUTIONS) {
      // better
      this.status = EStatus.ALL_SOLUTIONS;
    } else if (this.meta.method !== EMethod.SATISFY && this.status === EStatus.SATISFIED && parseStatus(output, this.meta.method) === EStatus.OPTIMAL_SOLUTION) {
      // better
      this.status = EStatus.OPTIMAL_SOLUTION;
    }


    let start = 0;
    let end = -1;
    do {
      const rf = findOutputSeparator(output, start);
      end = rf.index;
      if (end < 0 && isAll) {
        end = output.length;
      }
      if (end < 0) {
        break;
      }
      const line = output.slice(start, end);
      const {r: obj, line: unparsed} = parsePartialObject(line);
      if (obj && rf.isHeader) {
        // header case
        Object.assign(this.header, obj);
        this.extraHeader += unparsed;
        if (this.callback) {
          this.callback('header', this.buildResult());
        }
      } else if (obj) {
        // solution case
        const objective = obj._objective;
        delete obj._objective;
        const solution: ISolution = {
          objective,
          assignments: obj,
          extraOutput: unparsed
        };
        this.solutions.push(solution);
        if (this.callback) {
          this.callback('solution', this.buildResult());
        }
      } else {
        // couldn't parse maybe statistics or an unparseable solution
        const rest = this.parseStats(line);
        if (rest) {
          // a kind of solution but we cannot parse it properly
          const solution: ISolution = {
            assignments: {},
            extraOutput: rest
          };
          this.solutions.push(solution);
          if (this.callback) {
            this.callback('solution', this.buildResult());
          }
        }
      }
      start = end + rf.sep.length;
    } while (start < output.length);

    // keep new line
    return start > output.length ? '' : `${output.slice(start)}${wasEndingWithNewLine ? '\n' : ''}`;
  }
}

export function parseResult(output: string, meta: IModelMetaData, params: IModelParams): IResult {
  const builder = new ResultBuilder(meta, params);

  builder.push(output);

  return builder.build();
}


