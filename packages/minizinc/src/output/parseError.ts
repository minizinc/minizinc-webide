import {normalize} from '../internal/utils';
import {IError} from '../interfaces';

export default function parseError(str: string, modelFile: string, dataFile?: string): IError[] {
  str = normalize(str);
  modelFile = normalize(modelFile);
  if (dataFile) {
    dataFile = normalize(dataFile);
  }
  console.log(str, modelFile, dataFile);

  const errors: IError[] = [];

  const modelErrors = str.split(modelFile);

  let areEvaluationErrors = false;

  for (let modelError of modelErrors) {
    modelError = modelError.trim();
    if (!modelError) {
      continue;
    }

    if (modelError === 'MiniZinc: evaluation error:') {
      areEvaluationErrors = true;
      continue;
    }

    const lineNumbers = /^:(\d+)(\.(\d+)(-(\d+))?)?/gm.exec(modelError);
    const error: IError = {
      message: modelError,
      type: 'MiniZincModelError'
    };
    if (lineNumbers) {
      const col = lineNumbers.length > 1 ? parseInt(lineNumbers[3], 10) : 0;
      const endCol = lineNumbers[5] != null ? parseInt(lineNumbers[5], 10) : col;
      error.location = {
        line: parseInt(lineNumbers[1], 10),
        columns: [col, endCol]
      };
      const messageStart = modelError.indexOf(':', 2); // since it start with :
      if (messageStart > 0) {
        error.message = modelError = modelError.slice(messageStart + 1); // rest
      }
    }
    if (areEvaluationErrors) {
      error.type = 'MiniZincEvaluationError';
      errors.push(error);
      continue;
    }

    const syntaxKey = 'Error: syntax error, ';
    const start = modelError.indexOf(syntaxKey);
    if (start >= 0) {
      error.type = 'MiniZincSyntaxError';
      const end = modelError.indexOf('\n', start);
      error.message = modelError.slice(start + syntaxKey.length, end >= 0 ? end : undefined);
    }
    errors.push(error);
  }

  if (errors.length === 0) {
    errors.push({
      message: str,
      type: 'Generic'
    });
  }

  return errors;
}
