export interface IEmbeddingOptions {
  /**
   * parse out the statistics off the output stream
   *
   * alternative:
   *   * `data-no-stats` in the tag to enable
   *   * `data-stats` to the tag to disable
   *   * `minizinc-embedded-option-no-stats` CSS class to tag to enable
   *   * `minizinc-embedded-option-stats` CSS class to tag to disable
   *
   * @default false
   */
  noStats: boolean;

  /**
   * simplified toolbar allowing no options (solver selection, all solutions, free search)
   *
   * alternative:
   *   * `data-no-options` in the tag to enable
   *   * `data-options` to the tag to disable
   *   * `minizinc-embedded-option-no-options` CSS class to tag to enable
   *   * `minizinc-embedded-option-options` CSS class to tag to disable
   *
   * @default false
   */
  noOptions: boolean;

  /**
   * reaonly mode
   *
   * alternative:
   *   * `data-readonly` in the tag to enable
   *   * `data-editable` to the tag to disable
   *   * `minizinc-embedded-option-readonly` CSS class to tag to enable
   *   * `minizinc-embedded-option-editable` CSS class to tag to disable
   *
   * @default false
   */
  readonly: boolean;

  /**
   * use the native representation by default and add an edit button
   *
   * alternative:
   *   * `data-native` in the tag to enable
   *   * `data-wrapper` to the tag to disable
   *   * `minizinc-embedded-option-native` CSS class to tag to enable
   *   * `minizinc-embedded-option-wrapper` CSS class to tag to disable
   *
   * @default false
   */
  native: boolean;

  /**
   * preselect solver
   *
   * alternative:
   *   * `data-solver="<value>"` in the tagto set
   *   * `minizinc-embedded-option-solver-<value>` CSS class to tag to set
   *
   * @default undefined
   */
  solver: string;

  /**
   * enable free search checkbox
   *
   * alternative:
   *   * `data-free-search` in the tag to enable
   *   * `data-no-free-search` to the tag to disable
   *   * `minizinc-embedded-option-free-search` CSS class to tag to enable
   *   * `minizinc-embedded-option-no-free-search` CSS class to tag to disable
   *
   * @default false
   */
  freeSearch: boolean;

  /**
   * enable all solutions checkbox
   *
   * alternative:
   *   * `data-all-solutions` in the tag to enable
   *   * `data-single-solution` to the tag to disable
   *   * `minizinc-embedded-option-all-solutions` CSS class to tag to enable
   *   * `minizinc-embedded-option-single-solution` CSS class to tag to disable
   *
   * @default false
   */
  allSolutions: boolean;
  /**
   * selector to find the data for this embedding.
   * 'next' is a special one supported only the 'init' method. In this case the next matching embedding is actually the data and should not be embedded
   *
   * alternative:
   *   * `data-data="<value>"` in the tag to set
   *   * `minizinc-embedded-option-data-next` CSS class to tag to set to next
   *
   * @default undefined
   */
  data: string | 'next' | HTMLElement;

  /**
   * enable syntax highlighting by loading the Monaco editor
   *
   * alternative:
   *   * `data-highlight` in the tag to enable
   *   * `data-no-highlight` to the tag to disable
   *   * `minizinc-embedded-option-highlight` CSS class to tag to enable
   *   * `minizinc-embedded-option-no-highlight` CSS class to tag to disable
   *
   * @default false
   */
  highlight: boolean;
}


export interface IInitOptions extends IEmbeddingOptions {
  /**
   * selector to look for embedding definitions
   *
   * alternative:
   *  * `data-selector` in the script tag
   *
   * @default .minizinc-embedding
   */
  selector: string;

  /**
   * set the webpack base url explicitly to some value
   */
  baseUrl: string;

  /**
   * use an iframe wrapper
   *
   * alternative:
   *   * `data-iframe` in the script tag to enable
   *   * `data-direct` to the script tag to disable
   *   * `minizinc-embedded-option-iframe` CSS class to script tag to enable
   *   * `minizinc-embedded-option-iframe` CSS class to script tag to disable
   * @default true
   */
  iframe: boolean;

  /**
   * comma separated list of solvers to load by default only GeCode will be loaed
   *
   * alternative:
   *   * `data-solvers` in the script tag to enable
   * @default gecode
   */
  solvers: string;


  /**
   * instead of a local worker version, use the provided REST service that follows the RESTMiniZinc API
   */
  restService: string;
}

export const cssPrefix = 'minizinc-embedded';
export const cssOptionPrefix = `${cssPrefix}-option`;


export function handleBooleanOption<T>(o: Partial<T>, node: HTMLElement, key: keyof T, negateKey: string) {

  const toCSSKey = (str: string) =>  str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);

  const skey = <string>key;
  if (node.dataset[skey] != null || node.classList.contains(`${cssOptionPrefix}-${toCSSKey(skey)}`)) {
    (<any>o)[key] = true;
  }
  if ((negateKey && node.dataset[negateKey] != null) || node.dataset[skey] === 'false' || node.classList.contains(`${cssOptionPrefix}-${toCSSKey(negateKey)}`)) {
    (<any>o)[key] = false;
  }
}

export function parseElemOptions<T extends IEmbeddingOptions = IEmbeddingOptions>(node: HTMLElement) {
  const o: Partial<T> = {};

  handleBooleanOption(o, node, 'noStats', 'stats');
  handleBooleanOption(o, node, 'noOptions', 'options');
  handleBooleanOption(o, node, 'readonly', 'editable');
  handleBooleanOption(o, node, 'native', 'wrapper');
  handleBooleanOption(o, node, 'freeSearch', 'noFreeSearch');
  handleBooleanOption(o, node, 'allSolutions', 'singleSolution');
  handleBooleanOption(o, node, 'highlight', 'noHighlight');

  if (node.dataset.solver) {
    o.solver = node.dataset.solver;
  } else {
    const solverClass = Array.from(node.classList).find((d) => d.startsWith(`${cssOptionPrefix}-solver-`));
    if (solverClass) {
      o.solver = solverClass.substring(`${cssOptionPrefix}-solver-`.length);
    }
  }

  if (node.dataset.data) {
    o.data = node.dataset.data;
  } else {
    const dataClass = Array.from(node.classList).find((d) => d.startsWith(`${cssOptionPrefix}-data-`));
    if (dataClass) {
      o.data = dataClass.substring(`${cssOptionPrefix}-data-`.length);
    }
  }

  return o;
}
