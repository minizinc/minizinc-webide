function basename(path: string) {
  const i = path.lastIndexOf('/');
  return i < 0 ? path : path.slice(0, i + 1);
}

// set the base public url based on my script location
__webpack_public_path__ = document && document.currentScript ? basename((<HTMLScriptElement>document.currentScript).src) : './';

