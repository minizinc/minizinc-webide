import {AbortSignal, IMiniZinc, ISolverConfiguration, BackendException, IError, IModelParams} from 'minizinc';
import {cssPrefix, IEmbeddingOptions} from './options';
import MiniZincEditor from './editor';

function clean(str: string) {
  return str.split('\n').map((s) => s.trim()).join('\n');
}

export default class Embedding {
  private readonly modelWrapper: HTMLElement | null;
  private modelArea: HTMLTextAreaElement | MiniZincEditor;
  private readonly solversElem: HTMLSelectElement;
  private readonly dataWrapper: HTMLElement | null;
  private dataArea: HTMLTextAreaElement | MiniZincEditor;
  private readonly outputArea: HTMLTextAreaElement;
  private signal: AbortSignal | null = null;

  constructor(public readonly root: HTMLElement, private readonly minizinc: IMiniZinc, solvers: Promise<ISolverConfiguration[]>, private readonly options: Partial<IEmbeddingOptions> = {}) {
    (<any>root).__embedding__ = this;

    const {model, data, modelField, dataField} = this.extractValues();

    this.root.classList.add(`${cssPrefix}-container`);

    this.root.innerHTML = `
      <div class="${cssPrefix}-loading"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      <form>
        <div class="${cssPrefix}-toolbar ${options.noOptions ? `${cssPrefix}-simple` : ''}">
          <select name="solver" class="${cssPrefix}-hidden">
            <option value="">Selected solver...</option>
          </select>
          <label><input type="checkbox" name="all_solutions" value="true" ${options.allSolutions ? 'checked' : ''}>All solutions</label>
          <label><input type="checkbox" name="free_search" value="true" ${options.freeSearch ? 'checked' : ''}>Free Search</label>
          <button class="${cssPrefix}-edit ${options.readonly || !options.native ? `${cssPrefix}-hidden` : ''}" type="button">Edit</button>
          <button class="${cssPrefix}-abort" type="button">Abort</button>
          <button class="${cssPrefix}-solve" type="submit">Solve</button>
        </div>
        <textarea class="${cssPrefix}-model" name="model" placeholder="The MiniZinc Model"></textarea>
        <textarea class="${cssPrefix}-data" name="data" placeholder="The MiniZinc Parameter Data"></textarea>
        <textarea class="${cssPrefix}-output ${cssPrefix}-hidden" name="output" readonly></textarea>
      </form>
    `;

    this.root.querySelector<HTMLButtonElement>(`.${cssPrefix}-abort`)!.addEventListener('click', () => this.abort());

    const simpleModel = this.modelArea = this.root.querySelector<HTMLTextAreaElement>(`.${cssPrefix}-model`)!;
    this.modelArea.value = clean(model);
    this.modelArea.readOnly = Boolean(options.readonly);
    if (options.native) {
      this.modelWrapper = modelField;
      this.modelArea.insertAdjacentElement('beforebegin', this.modelWrapper);
      this.modelArea.classList.add(`${cssPrefix}-hidden`);
      this.modelArea.style.height = `${modelField.clientHeight}px`;
    } else {
      this.modelWrapper = null;
    }

    const simpleData = this.dataArea = this.root.querySelector<HTMLTextAreaElement>(`.${cssPrefix}-data`)!;
    this.dataArea.value = clean(data);
    this.dataArea.readOnly = Boolean(options.readonly);
    if (dataField && options.native) {
      this.dataWrapper = dataField;
      this.dataArea.insertAdjacentElement('beforebegin', this.dataWrapper);
      this.dataArea.classList.add(`${cssPrefix}-hidden`);
      this.dataArea.style.height = `${dataField.clientHeight}px`;
    } else {
      this.dataWrapper = null;
    }

    if (!data && (options.readonly || options.native)) {
      this.dataArea.classList.add(`${cssPrefix}-hidden`);
    }

    if (options.highlight && !(options.native && options.readonly)) {
      import('./editor').then((mod) => {
        {
          const base = simpleModel.ownerDocument!.createElement('div');
          base.classList.add(`${cssPrefix}-model`, `${cssPrefix}-editor`);
          base.style.height = `${simpleModel.clientHeight}px`;
          simpleModel.replaceWith(base);
          this.modelArea = new mod.default(base, simpleModel.value || `%${simpleModel.placeholder}\n`, Boolean(options.readonly));
        }
        {
          const base = simpleData.ownerDocument!.createElement('div');
          base.classList.add(`${cssPrefix}-data`, `${cssPrefix}-editor`);
          base.style.height = `${simpleData.clientHeight}px`;
          simpleData.replaceWith(base);
          this.dataArea = new mod.default(base, simpleData.value || `%${simpleData.placeholder}\n`, Boolean(options.readonly));
        }
      });
    }


    // edit mode
    const edit = this.root.querySelector<HTMLButtonElement>(`.${cssPrefix}-edit`)!;
    edit.addEventListener('click', () => {
      edit.classList.add(`${cssPrefix}-hidden`);
      if (this.modelWrapper) {
        this.modelWrapper.classList.add(`${cssPrefix}-hidden`);
      }
      this.modelArea.classList.remove(`${cssPrefix}-hidden`);
      if (this.dataWrapper) {
        this.dataWrapper.classList.add(`${cssPrefix}-hidden`);
      }
      this.dataArea.classList.remove(`${cssPrefix}-hidden`);
    });

    this.outputArea = this.root.querySelector<HTMLTextAreaElement>(`.${cssPrefix}-output`)!;
    this.solversElem = this.root.querySelector<HTMLSelectElement>('[name="solver"]')!;

    solvers.then((solvers) => {
      this.solversElem.innerHTML = `
      <option value="">Selected solver...</option>
      ${solvers.map((s) => `<option value="${s.id}">${s.name}</option>`).join()}
      `;
      this.solversElem.value = options.solver || '';
      // no need to select
      this.solversElem.classList.toggle(`${cssPrefix}-hidden`, solvers.length <= 1);
    });

    this.root.querySelector('form')!.onsubmit = (evt) => {
      evt.preventDefault();
      evt.stopPropagation();
      this.solve();
    };
  }

  private extractValues() {
    let modelField = this.root.querySelector<HTMLElement>(`[data-type=mzn], [type="text/x-minizinc"], .${cssPrefix}-model`);
    if (!modelField) {
      // use root
      modelField = <HTMLElement>this.root.cloneNode(true);
    }

    let dataField: HTMLElement | null = null;
    if (this.options.data && this.options.data === 'next') {
      console.warn('direct embedding does not support "next"');
    } else if (this.options.data) {
      dataField = typeof this.options.data === 'string' ? this.root.ownerDocument!.querySelector(this.options.data) : this.options.data;
    } else {
      dataField = this.root.querySelector<HTMLElement>(`[data-type=dzn], [type="text/x-dzn"], .${cssPrefix}-data`);
    }

    return {
      model: modelField.innerText.trim(),
      data: dataField ? dataField.innerText.trim() : '',
      modelField,
      dataField
    };
  }

  get model() {
    return this.modelArea.value;
  }

  set model(value: string) {
    this.modelArea.value = value;
    if (this.modelWrapper) {
      this.modelWrapper.textContent = value;
    }
  }

  get data() {
    return this.dataArea.value;
  }

  set data(value: string) {
    this.dataArea.value = value;
    if (this.dataWrapper) {
      this.dataWrapper.textContent = value;
    }
    this.dataArea.classList.toggle(`${cssPrefix}-hidden`, this.options.readonly && !value);
  }

  get allSolutions() {
    return this.root.querySelector<HTMLInputElement>('[name="all_solutions"]')!.checked;
  }

  set allSolutions(value: boolean) {
    this.root.querySelector<HTMLInputElement>('[name="all_solutions"]')!.checked = value;
  }

  get freeSearch() {
    return this.root.querySelector<HTMLInputElement>('[name="free_search"]')!.checked;
  }

  set freeSearch(value: boolean) {
    this.root.querySelector<HTMLInputElement>('[name="free_search"]')!.checked = value;
  }

  get solver() {
    return this.solversElem.value;
  }

  set solver(value: string) {
    this.solversElem.value = value;
  }

  get output() {
    return this.outputArea.value;
  }

  set output(value: string) {
    this.outputArea.value = value;
    this.outputArea.classList.toggle(`${cssPrefix}-hidden`, !value);
  }

  abort() {
    if (this.signal) {
      this.signal.abort();
    }
  }

  get isRunning() {
    return this.root.classList.contains(`${cssPrefix}-running`);
  }

  private setInEditor(errors: IError[]) {
    if (!(this.modelArea instanceof MiniZincEditor)) {
      return;
    }
    this.modelArea.setErrors(errors);
  }

  get params(): IModelParams {
    const s = this.solver || undefined;

    return {
      model: this.model,
      all_solutions: this.allSolutions,
      free_search: this.freeSearch,
      solver: s
    };
  }

  solve() {
    const infoOutput = 'Solving...';
    this.output = infoOutput;
    this.signal = new AbortSignal();
    this.root.classList.add(`${cssPrefix}-running`);
    const options = {
      stats: !this.options.noStats,
      onChunk: (chunk: string) => {
        if (this.output === 'Solving...') {
          this.output = chunk;
        } else {
          this.output += chunk;
        }
      },
      signal: this.signal
    };


    return this.minizinc.solveRaw(this.params, this.data || undefined, options).then((output) => {
        this.setInEditor([]);
      return this.output = String(output);
    }).catch((error) => {
      if (error instanceof BackendException) {
        this.setInEditor(error.errors);
        return this.output = error.message;
      }
      return this.output = String(error);
    }).finally(() => {
      this.root.classList.remove(`${cssPrefix}-running`);
    });
  }

  analyze() {
    const infoOutput = 'Analyzing...';
    this.output = infoOutput;
    this.signal = new AbortSignal();
    this.root.classList.add(`${cssPrefix}-running`);
    return this.minizinc.analyze(this.params, {signal: this.signal}).then((output) => {
      return this.output = JSON.stringify(output);
    }).catch((error) => {
      if (error instanceof BackendException) {
        this.setInEditor(error.errors);
        return this.output = error.message;
      }
      return this.output = String(error);
    }).finally(() => {
      this.root.classList.remove(`${cssPrefix}-running`);
    });
  }
}

export function getEmbedding(root: HTMLElement): Embedding | undefined {
  return (<any>root).__embedding__;
}
