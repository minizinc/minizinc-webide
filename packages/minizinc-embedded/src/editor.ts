import './setup';
import {editor, initEditor, error2decoration} from 'minizinc-webide/src/mzn/editor';
import EditorWorker from 'worker-loader?inline=true!monaco-editor/esm/vs/editor/editor.worker.js';
import {IError} from 'minizinc';

initEditor(EditorWorker);

export declare type IEditorConstructionOptions = editor.IEditorConstructionOptions;

export default class MiniZincEditor {
  public readonly instance: editor.IStandaloneCodeEditor;
  private activeErrors: string[] = [];

  constructor(elem: HTMLElement, value: string, readOnly: boolean, options: IEditorConstructionOptions = {}, public onValueChanged?: () => void) {
    this.instance = editor.create(elem, Object.assign({
      value,
      language: 'minizinc',
      glyphMargin: true,
      minimap: {
        enabled: false
      },
      readOnly
    }, options));
    this.instance.onDidChangeModelContent(() => {
      if (onValueChanged) {
        onValueChanged();
      }
    });
  }

  get classList() {
    return this.instance.getDomNode()!.classList;
  }

  get value() {
    return this.instance.getValue();
  }

  set value(value: string) {
    this.instance.setValue(value);
  }

  setErrors(errors: IError[]) {
    this.activeErrors = this.instance.deltaDecorations(this.activeErrors, errors.map(error2decoration));
  }

  dispose() {
    this.instance.dispose();
  }
}
