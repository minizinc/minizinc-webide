import './setup';
import createMiniZinc from 'minizinc/build/internal/workerloader';
import MessagingReceiver, {createWindowReceiverChannel} from 'minizinc/build/MessagingReceiver';

const search = new URLSearchParams(location.search);
// join and split to support single solvers instance with multiple ,
const solversToLoad = (search.getAll('solvers').join(',')).split(',').map((d) => d.trim()).filter((d) => d.length > 0);
const r = new MessagingReceiver(createMiniZinc({solvers: solversToLoad}), createWindowReceiverChannel(window));
r.register();
