import './setup';
import './style.scss';
import {IMiniZinc, ISolverConfiguration, IModelParams, IDataObject, IMiniZincOptions, IMiniZincSolveOptions, IMiniZincSolveRawOptions} from 'minizinc';
import MessagingMiniZinc, {createWindowChannel} from 'minizinc/build/MessagingMiniZinc';
import RESTMiniZinc from 'minizinc/build/RESTMiniZinc';
import Embedding from './embedding';
import {IInitOptions, IEmbeddingOptions, parseElemOptions, handleBooleanOption, cssPrefix} from './options';
import MiniZincEditor, {IEditorConstructionOptions} from './editor';

export {getEmbedding, default as Embedding} from './embedding';
export * from './options';

let instance: Promise<IMiniZinc> | null = null;
let rootOptions: Partial<IInitOptions> = {};

const onReadyPromise = document.readyState !== 'loading' ? Promise.resolve() : new Promise<void>((resolve) => document.addEventListener('DOMContentLoaded', () => resolve()));

export function getInstance() {
  if (instance) {
    return instance;
  }

  if (rootOptions.restService) {
    return instance = Promise.resolve(new RESTMiniZinc({
      baseUrl: rootOptions.restService
    }));
  }

  const solvers = rootOptions.solvers ? rootOptions.solvers.split(',') : undefined;

  if (!rootOptions.iframe) {
    return instance = import('minizinc/build/internal/workerloader').then((m) => m.default({solvers}));
  }
  return onReadyPromise.then(() => new Promise<IMiniZinc>((resolve) => {
    const elem = document.createElement('iframe');

    elem.addEventListener('load', () => {
      const m = new MessagingMiniZinc(createWindowChannel(elem.contentWindow!, window));
      resolve(m);
    });
    elem.classList.add('minizinc-embedded-iframe');
    // send list of solvers as request argument
    elem.src = `${__webpack_public_path__}iframe.html${solvers ? `?solvers=${solvers.join(',')}` : ''}`;
    document.body.appendChild(elem);
  }));
}


function initImpl(instance: IMiniZinc) {
  const solvers = instance.resolveSolvers().catch((error) => {
    console.warn('cannot resolve available solvers', error);
    return <ISolverConfiguration[]>[];
  });
  const elems = Array.from(document.querySelectorAll<HTMLElement>(rootOptions.selector || '.minizinc-embedding'));
  const embeddings: Embedding[] = [];

  while (elems.length > 0) {
    const node = elems.shift()!;
    const nodeOptions = parseElemOptions(node);
    const options = Object.assign({}, rootOptions, nodeOptions);
    // handle magic next case
    if (options.data === 'next' && elems.length > 0) {
      options.data = elems.shift()!;
    }
    embeddings.push(new Embedding(node, instance, solvers, options));
  }

  return embeddings;
}

export function init(options: Partial<IInitOptions> = {}) {
  if (options.baseUrl) {
    __webpack_public_path__ = options.baseUrl;
  }
  rootOptions = options;

  const instance = getInstance();

  return Promise.all([instance, onReadyPromise]).then(([i]) => {
    return initImpl(i);
  });
}

export function createEmbedding(root: string | HTMLElement, options: Partial<IEmbeddingOptions> = {}) {
  return getInstance().then((instance) => {
    const solvers = instance.resolveSolvers();
    const node = typeof root === 'string' ? document.querySelector<HTMLElement>(root)! : root;
    const nodeOptions = parseElemOptions(node);
    return new Embedding(node, instance, solvers, Object.assign({}, rootOptions, nodeOptions, options));
  });
}

export function analyze(code: string | IModelParams, options?: Partial<IMiniZincOptions>) {
  return getInstance().then((i) => i.analyze(code, options));
}

export function solve(code: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveOptions>) {
  return getInstance().then((i) => i.solve(code, data, options));
}

export function solveRaw(code: string | IModelParams, data?: IDataObject | string, options?: Partial<IMiniZincSolveRawOptions>) {
  return getInstance().then((i) => i.solveRaw(code, data, options));
}

export function resolveSolvers() {
  return getInstance().then((i) => i.resolveSolvers());
}

export function version() {
  return getInstance().then((m) => m.version());
}

export function terminate() {
  return getInstance().then((i) => i.terminate());
}

export function createEditor(elem: HTMLElement, value: string, readOnly: boolean, options: IEditorConstructionOptions = {}, onValueChanged?: () => void): Promise<MiniZincEditor> {
  return import('./editor').then((mod) => {
    return new mod.default(elem, value, readOnly, options, onValueChanged);
  });
}

if (document && document.currentScript) {
  const cs = <HTMLScriptElement>document.currentScript;

  const o: Partial<IInitOptions> = parseElemOptions<IInitOptions>(cs);
  if (cs.dataset.selector) {
    o.selector = cs.dataset.selector;
  }
  if (cs.dataset.solvers) {
    o.solvers = cs.dataset.solvers;
  }
  if (cs.dataset.restService) {
    o.restService = cs.dataset.restService;
  }
  o.iframe = true;
  handleBooleanOption(o, cs, 'iframe', 'direct');


  if (document.currentScript.dataset.init != null || document.currentScript.classList.contains(`${cssPrefix}-init`)) {
    init(o);
  } else {
    rootOptions = o;
  }
}
