const resolve = require('path').resolve;
const {createConfig, withHTMLPlugin, withCSSExtractPlugin, withMiniZincPolyfill}  = require('../../webpack.base');

/**
 * generate a webpack configuration
 */
module.exports = (_env, options) => {
  return createConfig({
    mode: options.mode,
    pkg: require('./package.json'),
  }, [{
      entry: {
        minizinc: './src/index.ts',
        iframe: './src/iframe.ts'
      },
      output: {
        path: resolve(__dirname, 'build'),
        crossOriginLoading: 'anonymous',
        library: 'MiniZincEmbedded',
        libraryTarget: 'umd',
        globalObject: 'this'
      },
      devServer: {
        contentBase: resolve(__dirname, 'demo')
      }
    },
    withMiniZincPolyfill(),
    withCSSExtractPlugin(),
    withHTMLPlugin({
      chunks: ['iframe'],
      filename: 'iframe.html',
      title: 'MiniZinc IFrame Helper'
    })
  ]
  );
}
