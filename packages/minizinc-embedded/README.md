Embedded MiniZinc
=================
[![License: MIT][mit-image]][mit-url] [![NPM version][npm-image]][npm-url]

This is a utility project to create simple embedded MiniZinc snippets that can be solved.

Basic Example
-------------

```html
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://unpkg.com/minizinc-embedded/build/minizinc.css">
  <script src="https://unpkg.com/minizinc-embedded/build/minizinc.js" data-init></script>
</head>
<body>
  <pre class="minizinc-embedding">
    int: n = 4; % The number of queens.

    array [1..n] of var 1..n: q;

    include "alldifferent.mzn";

    constraint alldifferent(q);
    constraint alldifferent(i in 1..n)(q[i] + i);
    constraint alldifferent(i in 1..n)(q[i] - i);

    solve satisfy;
  </pre>
</body>
</html>
```

will wrap the given embedding and add a `Solve` button to the top right.

Usage
-----

Include in your website: 

```html
<link rel="stylesheet" href="https://unpkg.com/minizinc-embedded/build/minizinc.css">
<script src="https://unpkg.com/minizinc-embedded/build/minizinc.js"></script>
```

To initialize the embedding one can run the following JavaScript or add some `data-` attributes or CSS classes to the script tag. 

```js
MiniZincEmbedded.init({
  //root options go here
});
```

has the same effect as:

```html
<script src="https://unpkg.com/minizinc-embedded/build/minizinc.js" data-init></script>
```

Options
-------

The script tag as well as each individual embedding provide a range of customization options. See [./src/options.ts](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc-embedded/src/options.ts) for a whole list of options. Most options can be defined using a CSS class or a `data-` attribute. For example: 

```html
<div class="minizinc-embedded-option-no-stats" data-no-stats></div>
```

are aquivalent. Moreover, all options on an embedding element supersedes the script tag options. 

An important root options (only available in the script tag or during `.init({...})`) is `selector`. This option specifies the CSS selector to use for determing the set of embeddings. By default it is set to `.minizinc-embedding` matching all elements having the `.minizinc-embedding` class. 


Model and Data Definition
-------------------------

In order to determine which part of the snippet is the model and which is the data file the following strategy is used: any element within the embedding matching the CSS selector `[data-type=mzn], [type="text/x-minizinc"], .minizinc-embedded-model` is considered the element containg the model. The model itself is retrieved using the `.textContent` property. 
The data file is similarly determined based on the CSS selector `[data-type=dzn], [type="text/x-dzn"], .minizinc-embedded-data`. As an alternative one can specify the global selector as part of the `data` option. In addition, the special `data-data="next"` syntax is supported. 
In case no model element can be found the embedding root element itself is used as input.

Example
```html
<div class="minizinc-embedding">
  <pre data-type="mzn">
    int: n; % The number of queens.

    array [1..n] of var 1..n: q;

    include "alldifferent.mzn";

    constraint alldifferent(q);
    constraint alldifferent(i in 1..n)(q[i] + i);
    constraint alldifferent(i in 1..n)(q[i] - i);

    solve satisfy;
  </pre>
  <pre data-type="dzn">
    n = 4;
  </pre>
</div>
```

Advanced Usage
--------------

The globally exported `MiniZincEmbedded` object has the same interface as any other [`IMiniZinc`](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/interfaces.ts) object, thus allowing to solve simple models directly. 

```js
MiniZincEmbedded.solve(`int i = 4;`).then((result) => {
  console.log(result);
});
MiniZincEmbedded.solveRaw(`int i = 4;`).then((output) => {
  console.log(output);
});
```

Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[npm-image]: https://badge.fury.io/js/minizinc-embedded.svg
[npm-url]: https://npmjs.org/package/minizinc-embedded
