minizinc-sheets-addon
=====================
[![License: MIT][mit-image]][mit-url] [![clasp](https://img.shields.io/badge/built%20with-clasp-4285f4.svg)](https://github.com/google/clasp)

TODO

test google sheet: https://docs.google.com/spreadsheets/d/1c_73bcB0jReTv8ZdiC9bep4EXn-JoP9h3y0hzGBU9m8/edit?usp=sharing

see https://developers.google.com/apps-script/guides/clasp

```
Deploy a published project

To deploy a project with clasp, first create an immutable version of the Apps Script project. A version is a "snapshot" of a script project and is similar to a read-only branched release.

clasp version [description]

This command displays the newly created version number. Using that number, you can deploy and undeploy instances of your project:

clasp deploy [version] [description]
clasp undeploy <deploymentId>

This command updates an existing deployment with a new version and description:

clasp redeploy <deploymentId> <version> <description>
```

```
flag cells
https://developers.google.com/apps-script/reference/spreadsheet/range#addDeveloperMetadata(String)
```

Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-sheets-addon.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
