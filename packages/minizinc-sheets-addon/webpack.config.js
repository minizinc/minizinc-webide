const resolve = require('path').resolve;
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const {createConfig, withHTMLPlugin, withCSSExtractPlugin, withMiniZincPolyfill}  = require('../../webpack.base');


/**
 * generate a webpack configuration
 */
module.exports = (_env, options) => {
  return createConfig({
    mode: options.mode,
    pkg: require('./package.json'),
  }, [{
    entry: {
      index: './src/index.ts',
      editor: './src/editor.ts',
      solve: './src/solve.ts'
    },
      output: {
        globalObject: 'self',
        path: resolve(__dirname, 'build')
      },
      devServer: {
        contentBase: resolve(__dirname, 'demo')
      }
    },
    withMiniZincPolyfill(),
    withCSSExtractPlugin(),
    withHTMLPlugin({
      chunks: ['index'],
      template: './src/index.html',
      filename: 'index.html',
      title: 'MiniZinc Solver',
      inlineSource: '.(js|css)$' // embed all javascript and css inline
    }),
    withHTMLPlugin({
      chunks: ['editor'],
      template: './src/editor.html',
      filename: 'editor.html',
      title: 'MiniZinc Editor',
      inlineSource: '.(js|css)$' // embed all javascript and css inline
    }),
    withHTMLPlugin({
      chunks: ['solve'],
      template: './src/solve.html',
      filename: 'solve.html',
      title: 'MiniZinc Solver',
      inlineSource: '.(js|css)$' // embed all javascript and css inline
    }),
    {
      plugins: [
        new HtmlWebpackInlineSourcePlugin()
      ]
    }
  ]
  );
};
