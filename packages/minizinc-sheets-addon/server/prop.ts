// tslint:disable-next-line: no-reference
/// <reference path="../node_modules/@types/google-apps-script/index.d.ts" />

export function getProp_<T>(key: string, defaultValue: T) {
  const props = PropertiesService.getDocumentProperties();
  const old = props.getProperty(key);
  return old ? <T>JSON.parse(old) : defaultValue;
}

export function setProp_<T>(key: string, value: T) {
  const props = PropertiesService.getDocumentProperties();
  const old = props.getProperty(key);
  const newValue = JSON.stringify(value);
  if (old === newValue) {
    return false;
  }
  props.setProperty(key, newValue);
  return true;
}

export function adaptProp_<T>(key: string, defaultValue: T, changer: (v: T) => T = (v) => v) {
  const props = PropertiesService.getDocumentProperties();
  const old = props.getProperty(key);
  const oldValue: T = old ? JSON.parse(old) : defaultValue;
  const v = changer(oldValue);
  const serialized = JSON.stringify(v);
  if (old !== serialized) {
    props.setProperty(key, serialized);
  }
  return v;
}
