// tslint:disable-next-line: no-reference
/// <reference path="../node_modules/@types/google-apps-script/index.d.ts" />

declare const exports: typeof import('./event') & typeof import('./prop');

export function getModel() {
  const range = getModelRange();
  const sheet = SpreadsheetApp.getActiveSpreadsheet();
  const r = sheet.getRange(range);
  return String(r.getValue());
}

export function setModel(model: string, senderID?: string) {
  const range = getModelRange();
  const sheet = SpreadsheetApp.getActiveSpreadsheet();
  const r = sheet.getRange(range);
  const old = String(r.getValue());
  if (old === model) {
    return false;
  }
  r.setValue(model);
  exports.pushEvent_({type: 'modelChanged', model, range}, senderID);
  return true;
}

export function getModelRange() {
  return exports.getProp_('modelRange', 'MiniZincModel');
}

export function setModelRange(range: string, senderID?: string) {
  const changed = exports.setProp_('modelRange', range);
  if (changed) {
    exports.pushEvent_({type: 'modelChanged', model: getModel(), range}, senderID);
  }
}

export function getTemplateSheetName() {
  return exports.getProp_('templateSheet', 'TemplateSheet');
}

export function setTemplateSheetName(name: string, senderID?: string) {
  const changed = exports.setProp_('templateSheet', name);
  if (changed) {
    exports.pushEvent_({type: 'templateSheetChanged', name}, senderID);
  }
}

export function setOptions(modelRange: string, templateSheetName: string, senderID?: string) {
  setModelRange(modelRange, senderID);
  setTemplateSheetName(templateSheetName, senderID);

  return getModel();
}
