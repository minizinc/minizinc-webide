// tslint:disable-next-line: no-reference
/// <reference path="../node_modules/@types/google-apps-script/index.d.ts" />
import {IProperty} from '../src/interfaces';
import {IDataObject} from 'minizinc';

declare const exports: typeof import('./state');

export function getMiniZincURL() {
  return 'https://unpkg.com/minizinc-embedded@2.0.1-alpha.79';
}

const DEV_DATA_MINIZINC = 'miniZincOutput';

export function clearOutputFlags_(range: GoogleAppsScript.Spreadsheet.Range) {
  // delete all flags of being an output if entered manually
  const metaData = range.getDeveloperMetadata();
  metaData.forEach((m) => {
    if (m.getKey() === DEV_DATA_MINIZINC) {
      m.remove();
    }
  });
}

function readCell_(sheet: GoogleAppsScript.Spreadsheet.Sheet | GoogleAppsScript.Spreadsheet.Spreadsheet, prop: IProperty) {
  const range = sheet.getRange(prop.range);

  if (prop.dim === 0) {
    // 0D
    return range.getValue();
  }
  const numRows = range.getNumRows();
  let numCols = range.getNumColumns();

  if (prop.dim === 1) {
    if (!prop.expand) {
      // take it as it is
      // vertical 1D
      if (numRows === 1 && numCols > 1) {
        return range.getValues().map((v) => v[0]);
      }
      return range.getValues()[0];
    }

    // auto expand depending on expand setting
    if (prop.expand === 'row') {
      // start with given range and then go down
      const vs: any[] = range.getValues().map((v) => v[0]);

      let row = numRows;
      let v = range.offset(row, 0, 1, 1).getValue();
      while (v != null) {
        vs.push(v);
        v = range.offset(++row, 0, 1, 1).getValue();
      }
      return vs;
    }

    // auto expand to column
    // start with given columns and then increase
    const vs: any[] = range.getValues()[0];
    let col = numCols;
    let v = range.offset(0, col, 1, 1).getValue();
    while (v != null) {
      vs.push(v);
      v = range.offset(0, ++col, 1, 1).getValue();
    }
  }

  // 2D
  if (!prop.expand) {
    return range.getValues();
  }

  // auto expand
  const vs = range.getValues();

  // TODO cornercase not working when a new column could be added for an added row

  if (prop.expand === 'column' || prop.expand === 'both') {
    let col = numCols;
    let v = range.offset(0, col, numRows, 1).getValues().map((v) => v[0]);
    while (v.some((vi) => vi != null)) {
      // push to each row
      vs.forEach((row, i) => row.push(v[i]!));
      v = range.offset(0, ++col, numRows, 1).getValues().map((v) => v[0]);
    }
    // adapt to the new number of columns
    numCols = col - 1; // without the last empty one
  }

  if (!(prop.expand === 'row' || prop.expand === 'both')) {
    return;
  }

  let row = numRows;
  // one row
  let v = range.offset(row, 0, 1, numCols).getValues()[0];
  while (v.some((vi) => vi != null)) {
    vs.push(v);
    v = range.offset(++row, 0, 1, numCols).getValues()[0];
  }
  return vs;
}

function writeCell_(sheet: GoogleAppsScript.Spreadsheet.Sheet | GoogleAppsScript.Spreadsheet.Spreadsheet, prop: IProperty, value: any, offsetCol: number, offsetRow: number, isOutput: boolean = false) {
  let r = sheet.getRange(prop.range);
  if (offsetCol > 0 || offsetRow > 0) {
    r = r.offset(offsetRow, offsetCol, r.getNumRows(), r.getNumColumns());
  }
  // clear old
  clearOutputFlags_(r);
  r.clearContent();

  const addMetaData = (r: GoogleAppsScript.Spreadsheet.Range) => {
    // Adding developer metadata to arbitrary ranges is not currently supported.
    // Developer metadata may only be added to the top - level spreadsheet, an individual sheet,
    // or an entire row or column.at[unknown function](server / api: 100) at writeCell_(server / api: 116) at[unknown function](server / api: 217) at[unknown function](server / api: 213) at writeMultiOutput(server / api: 207)
    //
    // if (isOutput) {
    //   r.addDeveloperMetadata(DEV_DATA_MINIZINC);
    // }
    return r.getA1Notation();
  };

  // 0D
  if (prop.dim === 0 || !Array.isArray(value)) {
    return addMetaData(r.offset(0, 0, 1, 1).setValue(value));
  }

  let values: any[] = value;
  const numRows = r.getNumRows();
  const numCols = r.getNumColumns();

  if (prop.dim === 1) {
    if (prop.expand === 'row') {
      return addMetaData(r.offset(0, 0, values.length, 1).setValues(values.map((v) => [v]))); // 2D array
    }
    if (prop.expand) { // column
      return addMetaData(r.offset(0, 0, 1, values.length).setValues([values]));
    }
    // don't expand so match
    if (numRows > 1) {
      // user indicates that she wants it vertically, which is not clear for 1D arrays
      // match sizes
      if (numRows < values.length) {
        values = values.slice(0, numRows);
      } else if (numRows > values.length) {
        values = values.concat(new Array(numRows - values.length).fill(null));
      }
      return addMetaData(r.setValues(values.map((v) => [v]))); // 2D array
    }
    if (numCols < values.length) {
      values = values.slice(0, numCols);
    } else if (numCols > values.length) {
      values = values.concat(new Array(numCols - values.length).fill(null));
    }
    return addMetaData(r.setValues([values]));
  }

  let valueNumColumns = values.length > 0 ? values[0].length : 0;
  // 2D case
  if (prop.expand === 'both') {
    return addMetaData(r.offset(0, 0, values.length, valueNumColumns).setValues(values));
  }
  if (prop.expand !== 'row') {
    // clamp rows
    if (numRows < values.length) {
      values = values.slice(0, numRows);
    } else if (numRows > values.length) {
      // fill up with extra rows
      values = values.concat(new Array(numRows - values.length).fill(new Array(valueNumColumns).fill(null)));
    }
  }
  if (prop.expand !== 'column') {
    // clamp columns
    if (numCols < valueNumColumns) {
      values = values.map((row) => row.slice(0, numCols));
    } else if (numRows > valueNumColumns) {
      // fill up with extra columns
      values = values.map((row) => row.concat(new Array(numCols - valueNumColumns).fill(null)));
    }
    valueNumColumns = numCols;
  }

  // match and set
  return addMetaData(r.offset(0, 0, values.length, valueNumColumns).setValues(values));
}

export function readInput(inputs: IProperty[]) {
  const sheet = SpreadsheetApp.getActive();

  const r: IDataObject = {};
  inputs.forEach((input) => {
    r[input.key] = readCell_(sheet, input);
  });
  return r;
}

export function applyInputFormat(input: IProperty) {
  const sheet = SpreadsheetApp.getActive();
  const r = sheet.getRange(input.range);
  switch(input.type) {
    case 'bool':
      r.insertCheckboxes();
      break;
    // case 'int':
    //   r.setNumberFormat('0');
    //   break;
    // case 'float':
    //   r.setNumberFormat('0.00');
    //   break;
  }
  // const rule = SpreadsheetApp.newDataValidation()
  //   .setAllowInvalid(false)
  //   .setHelpText('Number must be between 1 and 100.')
  //   .build();
  // cell.setDataValidation(rule);
}

export function writeOutput(outputs: IProperty[], index: number, assignments: IDataObject) {
  return writeMultiOutput(outputs, [{index, assignments}]);
}

function getSolutionSheetName_(index: number) {
  return `Solution ${index + 1}`;
}

function find_<T>(arr: T[], f: (v: T) => boolean): T | null {
  const l = arr.length;
  for (let i = 0; i < l; i++) {
    const vi = arr[i];
    if (f(vi)) {
      return vi;
    }
  }
  return null;
}

function getSolutionSheet_(index: number) {
  const sheet = SpreadsheetApp.getActive();
  // try existing
  const sheetName = getSolutionSheetName_(index);
  const sheets = sheet.getSheets();
  const solution = find_(sheets, (d) => d.getName() === sheetName);
  if (solution) {
    return solution;
  }

  // create a new one based on the template
  const templateName = exports.getTemplateSheetName();
  const template = find_(sheets, (d) => d.getName() === templateName);
  return template ? sheet.insertSheet(sheetName, sheets.length, {template}) : sheet.insertSheet(sheetName, sheets.length);
}

export function writeMultiOutput(outputs: IProperty[], data: {index: number, assignments: IDataObject}[]) {
  const sheet = SpreadsheetApp.getActive();
  const active = sheet.getActiveSheet();

  outputs.forEach((output) => {
    if (!output.solution || output.solution === 'in_place') {
      // update just with the last one
      const assignments = data[data.length - 1].assignments;
      return writeCell_(sheet, output, assignments[output.key]!, 0, 0, true);
    }

    data.forEach(({index, assignments}) => {
      const value = assignments[output.key]!;
      if (output.solution === 'one_per_sheet') {
        return writeCell_(getSolutionSheet_(index), output, value, 0, 0, true);
      }
      if (output.solution === 'one_per_row') {
        return writeCell_(sheet, output, value, 0, index, true);
      }
      //if (output.solution === 'one_per_column') {
      return writeCell_(sheet, output, value, index, 0, true);
    });
  });

  if (outputs.some((d) => d.solution === 'one_per_sheet')) {
    // reset active
    sheet.setActiveSheet(active);
  }
}

export function clearOutput(outputs: IProperty[], numSolutions: number, assignmentSizes: {[key: string]: number | [number, number]}) {
  const sheet = SpreadsheetApp.getActive();

  const cleanup = (r: GoogleAppsScript.Spreadsheet.Range) => {
    clearOutputFlags_(r);
    r.clearContent();
  };

  if (outputs.some((d) => d.solution === 'one_per_sheet')) {
    // clean up solution sheets
    for (let i = 0; i < numSolutions; i++) {
      const solutionName = getSolutionSheetName_(i);
      // delete whole sheet if exists
      const s = find_(sheet.getSheets(), (d) => d.getName() === solutionName);
      if (s) {
        sheet.deleteSheet(s);
      }
    }
  }

  outputs.forEach((output) => {
    if (output.solution === 'one_per_sheet') {
      return; // handled
    }

    const base = sheet.getRange(output.range);
    const assignmentSize = assignmentSizes[output.key]!;

    let numCols = 1;
    let numRows = 1;

    if (output.expand === 'row' && output.dim === 1) {
      // expand rows to actual dim
      numRows = <number>assignmentSize;
    } else if (output.expand && output.dim === 1) {
      // expand cols to actual dim
      numCols = <number>assignmentSize;
    }
    if (output.expand !== 'column' && output.dim === 2) {
      numRows = (<[number, number]>assignmentSize)[0];
    }
    if (output.expand !== 'row' && output.dim === 2) {
      numCols = (<[number, number]>assignmentSize)[1];
    }
    if (output.solution === 'one_per_row') {
      numRows *= numSolutions;
    } else if (output.solution === 'one_per_column') {
      numCols *= numSolutions;
    }
    // per sheet not yet supported
    return cleanup(base.offset(0, 0, numRows, numCols));
  });
}

export function showMsgBox(title: string, msg: string) {
  const ui = SpreadsheetApp.getUi();
  ui.alert(title, msg, ui.ButtonSet.OK);
}

