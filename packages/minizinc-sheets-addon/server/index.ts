// tslint:disable-next-line: no-reference
/// <reference path="../node_modules/@types/google-apps-script/index.d.ts" />

declare const exports: typeof import('./state') & typeof import('./api') & typeof import('./event');


export function onOpen() {//e: GoogleAppsScript.Events.SheetsOnOpen) {
  const menu = SpreadsheetApp.getUi().createAddonMenu();
  menu.addItem('Show Panel', 'showSideBar_').addToUi();
  menu.addItem('Open Editor', 'openEditor_').addToUi();
  menu.addItem('Solve', 'solveWindow_').addToUi();
}

export function onInstall() {//e: GoogleAppsScript.Events.AddonOnInstall) {
  onOpen();
  showSideBar_();
}

// export function onEdit(e: GoogleAppsScript.Events.SheetsOnEdit) {
//   // const props = PropertiesService.getDocumentProperties();
//   try {
//     const range = e.range.getA1Notation();


//     exports.clearOutputFlags_(e.range);

//     state.inputs.forEach((input) => {
//       if (input.range === range) { // TODO or overlaps
//         exports.pushEvent_({type: 'inputChanged', name: input.key, value: e.value});
//       }
//     });
//   } catch (error) {
//     Logger.log(error);
//   }
// }

export function showSideBar_() {
  const output = HtmlService.createTemplateFromFile('build/index.html').evaluate();
  output.setTitle('MiniZinc Solver');
  SpreadsheetApp.getUi().showSidebar(output);
}

export function openEditor_() {
  const output = HtmlService.createTemplateFromFile('build/editor.html').evaluate();
  output.setTitle('MiniZinc Editor');
  SpreadsheetApp.getUi().showModalDialog(output, 'Model Editor');
}

export function solveWindow_() {
  const output = HtmlService.createTemplateFromFile('build/solve.html').evaluate();
  output.setTitle('MiniZinc Solver');
  SpreadsheetApp.getUi().showModalDialog(output, 'MiniZinc Solver');
}

// export function onChange(e: GoogleAppsScript.Events.SheetsOnChange) {
//   try {
//     Logger.log(JSON.stringify({change: true, e}));
//   } catch (error) {
//     Logger.log(error);
//   }
// }

export function getNextSideBarID() {
  return Math.random().toString(32).slice(3);
}

export function closeSideBar(uniqueID: string) {
  exports.stopCollectingEvents(uniqueID);
}
