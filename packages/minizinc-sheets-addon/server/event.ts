// tslint:disable-next-line: no-reference
/// <reference path="../node_modules/@types/google-apps-script/index.d.ts" />
import {IClientEvent} from '../src/interfaces';
import * as _prop from './prop';
declare const exports: typeof _prop;

export function pushEvent_(event: IClientEvent, sender?: string) {
  const listening = exports.adaptProp_('listening', <string[]>[]);
  listening.forEach((uniqueID) => {
    if (sender === uniqueID) {
      return;
    }
    exports.adaptProp_(`events4${uniqueID}`, <IClientEvent[]>[], (v) => {
      v.push(event);
      return v;
    });
  });
}

export function startCollectingEvents(uniqueID: string) {
  exports.adaptProp_('listening', <string[]>[], (v) => {
    v.push(uniqueID);
    return v;
  });
}

export function stopCollectingEvents(uniqueID: string) {
  exports.adaptProp_('listening', <string[]>[], (v) => {
    const index = v.indexOf(uniqueID);
    if (index >= 0) {
      v.splice(index, 1);
    }
    return v;
  });
}

export function collectEvents(uniqueID: string): IClientEvent[] {
  let events: IClientEvent[] = [];
  exports.adaptProp_(`events4${uniqueID}`, <IClientEvent[]>[], (v) => {
    events = v;
    return [];
  });
  Logger.log(events);
  return events;
}
