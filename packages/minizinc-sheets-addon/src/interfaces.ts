import {ITypeInfo} from 'minizinc';

export declare type EExpandDirection = 'both' | 'column' | 'row';
export declare type EMultiSolution = 'in_place' | 'one_per_row' | 'one_per_column' | 'one_per_sheet';

export interface IProperty extends ITypeInfo {
  range: string;
  expand?: EExpandDirection;
  /**
   * @default in_place
   */
  solution?: EMultiSolution;
}

export interface IInputChangedEvent {
  type: 'inputChanged';
  name: string;
  value: any;
}

export interface IModelChangedEvent {
  type: 'modelChanged';
  model: string;
  range: string;
}

export interface ITemplateSheetChangedEvent {
  type: 'templateSheetChanged';
  name: string;
}

export declare type IClientEvent = IInputChangedEvent | IModelChangedEvent | ITemplateSheetChangedEvent;
