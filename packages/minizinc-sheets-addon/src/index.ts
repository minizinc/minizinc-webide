import {AbortSignal} from 'minizinc';
// import ServerEvents from './events';
import {analyzeAndSolve, clearLastResult, IRunOutput, checkModel} from './model';
import {callServer, getModel, setOptions, setModel} from './server';
import './style.scss';

declare const MiniZincEmbedded: typeof import('minizinc-embedded/src');
declare const uniqueID: string;
declare const initialModel: string;

const loading = document.querySelector<HTMLElement>('.minizinc-embedded-loading')!;
loading.style.display = 'none'; // since loading is done

// const _events = new ServerEvents(uniqueID);
const form = document.querySelector('form')!;


let signal: AbortSignal | null  = null;
let lastRun: IRunOutput | null = null;


const solveAndAbortButton = document.querySelector<HTMLButtonElement>('#solveNabort')!;
solveAndAbortButton.onclick = (evt) => {
  evt.preventDefault();
  if (signal) {
    signal.abort();
  } else {
    solve();
  }
};
const output = document.querySelector<HTMLElement>('#output')!;
const clear = document.querySelector<HTMLButtonElement>('#clear')!;
clear.onclick = (evt) => {
  evt.preventDefault();
  cleanImpl();
};

let updateModelDebouncedTimer = -1;
let checkModelDebouncedTimer = -1;
const UPDATE_MODEL_DEBOUNCE = 2500;

function updateModelDebounced() {
  if (updateModelDebouncedTimer >= 0) {
    self.clearTimeout(updateModelDebouncedTimer);
    updateModelDebouncedTimer = -1;
  }
  updateModelDebouncedTimer = self.setTimeout(() => {
    editor.then((instance) => {
      setModel(instance.value, uniqueID);
    });
  }, UPDATE_MODEL_DEBOUNCE);

  if (checkModelDebouncedTimer >= 0) {
    self.clearTimeout(checkModelDebouncedTimer);
    checkModelDebouncedTimer = -1;
  }
  checkModelDebouncedTimer = self.setTimeout(() => {
    checkModelErrors();
  }, UPDATE_MODEL_DEBOUNCE);
}


document.querySelector<HTMLButtonElement>('#sync')!.onclick = (evt) => {
  evt.preventDefault();
  Promise.all([editor, getModel()]).then(([instance, model]) => {
    instance.value = model;
  });
};

const editor = MiniZincEmbedded.createEditor(document.querySelector<HTMLElement>('#editor')!, initialModel, false, {
  folding: false,
  lineNumbers: 'off',
  glyphMargin: false
}, updateModelDebounced);

function finish(result: IRunOutput | null, error?: any) {
  // console.log(JSON.stringify(result, null, 2));
  lastRun = result;
  clear.disabled = result == null;
  signal = null;
  solveAndAbortButton.textContent = 'Solve';
  loading.style.display = 'none';
  output.textContent = error ? (error.message ? error.message : String(error)) : '';
}

function solve() {
  solveAndAbortButton.textContent = 'Abort';
  loading.style.display = 'block';
  output.textContent = '';
  signal = new AbortSignal();
  getModel().then((model) => {
    return analyzeAndSolve(model, signal!);
  }).then((result) => finish(result))
    .catch((error) => finish(null, error));
}

function checkModelErrors() {
  editor.then((instance) => {
    return checkModel(instance.value, instance);
  });
}
// initially check
checkModelErrors();

function finishClean(error?: any) {
  lastRun = null;
  loading.style.display = 'none';
  output.textContent = error ? (error.message ? error.message : String(error)) : '';
}

function cleanImpl() {
  if (!lastRun) {
    return;
  }
  clear.disabled = true;
  loading.style.display = 'block';
  output.textContent = '';
  clearLastResult(lastRun.outputs, lastRun.result).then(() => finishClean()).catch((error) => finishClean(error));
}

form.onsubmit = (evt) => {
  evt.preventDefault();
  const data = new FormData(form);
  loading.style.display = 'block';
  output.textContent = '';
  setOptions(
    data.get('modelRange')!.toString(),
    (data.get('templateSheetName') || '')!.toString(),
    uniqueID).then((model) => {
      editor.then((instance) => instance.value = model);
      finishClean();
    }).catch((error) => finishClean(error));
};

window.onunload = () => {
  console.log('closing side bar');
  callServer('closeSideBar', uniqueID);
};
