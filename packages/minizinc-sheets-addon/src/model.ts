import {ITypeInfo, IAbortSignal, IResult, EStatus, IDataObject} from 'minizinc';
import sheetsMzn from 'raw-loader!../scripts/sheets.mzn';
import {EExpandDirection, EMultiSolution, IProperty} from './interfaces';
import {writeOutput, readInput, writeMultiOutput, clearOutput} from './server';
import MiniZincEditor from 'minizinc-embedded/src/editor';

declare const MiniZincEmbedded: typeof import('minizinc-embedded/src');

export const options = {
  extraIncludeFiles: {
    'sheets.mzn': sheetsMzn
  }
};

export function toProperty(info: ITypeInfo, model: string): IProperty {
  const range = new RegExp(`:\\s*${info.key}[\\s:][^;]*:\\s*range\\("(.+)"\\)`, 'gm');
  const rrange = range.exec(model);
  const expand = new RegExp(`:\\s*${info.key}[\\s:][^;]*:\\s*(expand|expand_column|expand_row)[^_]`, 'gm');
  const rexpand = expand.exec(model);
  const solution = new RegExp(`:\\s*${info.key}[\\s:][^;]*:\\s*(in_place|one_per_row|one_per_column|one_per_sheet)`, 'gm');
  const rsolution = solution.exec(model);

  const lookup: {[key: string]: EExpandDirection} = {
    expand: 'both',
    expand_column: 'column',
    expand_row: 'row'
  };
  return {
    ...info,
    range: rrange ? rrange[1] : info.key,
    expand: rexpand ? lookup[rexpand[1]] : undefined,
    solution: rsolution ? <EMultiSolution>rsolution[1] : undefined,
  };
}

interface ISolveContext {
  inputs: IProperty[];
  outputs: IProperty[];
  model: string;
  signal?: IAbortSignal;
  computeAllSolutions: boolean;
}


function throwErrorResult(result: IResult) {
  switch (result.status) {
    case EStatus.ERROR:
    case EStatus.UNKNOWN:
      throw new Error('Unknown Error occurred');
    case EStatus.UNSATISFIABLE:
      throw new Error('Unsatisfiable problem');
    case EStatus.UNBOUNDED:
      throw new Error('Unbounded problem');
  }
}

export interface IRunOutput {
  model: string;
  inputs: IProperty[];
  data: IDataObject;

  outputs: IProperty[];
  result: IResult;
}

export function checkModel(model: string, editor: MiniZincEditor) {
  return MiniZincEmbedded.analyze(model, options)
    .then(() => editor.setErrors([]))
    .catch((error) => {
      if (error.errors) {
        editor.setErrors(error.errors);
        return;
      }
      editor.setErrors([{
        type: 'Unknown',
        message: error.message || String(error)
      }]);
    });
}

export function analyzeAndSolve(model: string, signal?: IAbortSignal): Promise<IRunOutput> {
  return MiniZincEmbedded.analyze(model, Object.assign({signal}, options)).then((metaData) => {
    const inputs = metaData.inputs.map((input) => toProperty(input, model));
    const outputs = metaData.outputs.map((output) => toProperty(output, model));
    const computeAllSolutions = outputs.some((d) => d.solution != null);

    return solveImpl({inputs, outputs, computeAllSolutions, signal, model}).then(({result, data}) => ({
      result, inputs, outputs, model, data
    }));
  });
}

export function solveImpl({inputs, outputs, model, signal, computeAllSolutions}: ISolveContext) {
  return readInput(inputs).then((data) => {
    if (!computeAllSolutions) {
      return MiniZincEmbedded.solve(model, data, Object.assign({signal}, options)).then((result) => {
        throwErrorResult(result);
        const last = result.solutions.length - 1;
        // single so index is 0
        return writeOutput(outputs, 0, result.solutions[last].assignments).then(() => ({result, data}));
      });
    }

    const partialErrors: any[] = [];
    let batch: {index: number, assignments: IDataObject}[] = [];
    let debounceTimer = -1;
    const DEBOUNCE_TIME = 500; // limit API requests and use batching

    const writeLast = (result: IResult) => {
      if (debounceTimer >= 0) {
        self.clearTimeout(debounceTimer);
        debounceTimer = -1;
      }
      const index = result.solutions.length - 1;
      const assignments = result.solutions[index].assignments;
      batch.push({index, assignments});

      debounceTimer = self.setTimeout(() => {
        const send = batch;
        batch = [];
        // console.log('write multi', JSON.stringify(send));
        writeMultiOutput(outputs, send).catch((error) => partialErrors.push(error));
      }, DEBOUNCE_TIME);
    };

    return MiniZincEmbedded.solve({model, all_solutions: true}, data, {
      ...options,
      onPartialResult: (type: string, result: IResult) => {
        if (type !== 'solution') {
          return;
        }
        writeLast(result);
      }
    }).then((result) => {
      if (partialErrors.length > 0) {
        console.log(JSON.stringify(partialErrors));
        throw new Error(partialErrors.map((m) => m.message || String(m)).join('\n'));
      }
      throwErrorResult(result);
      return ({result, data});
    });
  });
}

export function clearLastResult(outputs: IProperty[], result: IResult) {
  const assignmentSizes: {[key: string]: number | [number, number]} = {};
  const first = result.solutions.length > 0 ? result.solutions[0].assignments : null;

  for (const output of outputs) {
    if (output.dim === 0 || !first || first[output.key] == null) {
      assignmentSizes[output.key] = 1;
    } else if (output.dim === 1) {
      assignmentSizes[output.key] = (<any[]>first[output.key]).length;
    } else {
      const v = (<any[][]>first[output.key]);
      assignmentSizes[output.key] = [v.length, v.length === 0 ? 0 : v[0].length];
    }
  }
  return clearOutput(outputs, result.solutions.length, assignmentSizes);
}
