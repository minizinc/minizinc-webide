import './style.scss';
import {callServer, closeWindow} from './server';

const loading = document.querySelector<HTMLElement>('.minizinc-embedded-loading')!;
loading.style.display = 'none';

declare const MiniZincEmbedded: typeof import('minizinc-embedded/src');
declare const initialModel: string;

function setModel(model: string, senderID?: string): Promise<boolean> {
  return callServer<boolean>('setModel', model, senderID);
}

const editor = MiniZincEmbedded.createEditor(document.querySelector<HTMLElement>('#editor')!, initialModel, false, {
  folding: false,
  lineNumbers: 'off',
  glyphMargin: false
});

document.querySelector<HTMLButtonElement>('#saveButton')!.onclick = (evt) => {
  evt.preventDefault();
  editor.then((instance) => setModel(instance.value)).then(() => closeWindow());
};

document.querySelector<HTMLButtonElement>('#cancelButton')!.onclick = (evt) => {
  evt.preventDefault();
  closeWindow();
};

