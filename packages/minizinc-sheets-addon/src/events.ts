import {IClientEvent, IInputChangedEvent, IModelChangedEvent} from './interfaces';
import {EventEmitter} from 'events';
import {callServer} from './server';

const DEFAULT_POLL_INTERVALL = 5000;
const HIGH_POLL_INTERVALL = 250;
const LOW_POLL_INTERVALL = 10000;

export default class ServerEvents extends EventEmitter {
  private pollInterval = DEFAULT_POLL_INTERVALL;
  private id: number = -1;

  private readonly collectEvents = () => {
    callServer<IClientEvent[]>('collectEvents', this.uniqueID).then((events) => {
      this.handleEvents(events);
      this.id = self.setTimeout(this.collectEvents, this.pollInterval);
    });
  }

  constructor(private readonly uniqueID: string) {
    super();
  }

  start() {
    if (this.id >= 0) {
      return;
    }
    callServer('startCollectingEvents', this.uniqueID);
    this.id = self.setTimeout(this.collectEvents, this.pollInterval);
  }

  stop() {
    if (this.id < 0) {
      return;
    }
    clearTimeout(this.id);
    callServer('stopCollectingEvents', this.uniqueID);
    this.id = -1;
  }

  collectFast() {
    this.pollInterval = HIGH_POLL_INTERVALL;
  }

  collectNormal() {
    this.pollInterval = DEFAULT_POLL_INTERVALL;
  }

  collectSlow() {
    this.pollInterval = LOW_POLL_INTERVALL;
  }


  private handleEvents(events: IClientEvent[]) {
    for (const event of events) {
      this.emit(event.type, event);
    }
  }

  on(type: 'inputChanged', listener: (event: IInputChangedEvent) => void): this;
  on(type: 'modelChanged', listener: (event: IModelChangedEvent) => void): this;
  on(type: string, listener: (event: any) => void) {
    return super.on(type, listener);
  }

  off(type: 'inputChanged', listener: (event: IInputChangedEvent) => void): this;
  off(type: 'modelChanged', listener: (event: IModelChangedEvent) => void): this;
  off(type: string, listener: (event: any) => void) {
    return super.off(type, listener);
  }
}
