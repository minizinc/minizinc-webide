import {AbortSignal} from 'minizinc';
import {analyzeAndSolve} from './model';
import {closeWindow} from './server';
import './style.scss';

declare const model: string;

const loading = document.querySelector<HTMLElement>('.minizinc-embedded-loading')!;

const signal = new AbortSignal();
const abortButton = document.querySelector<HTMLButtonElement>('#abort')!;
abortButton.onclick = (evt) => {
  evt.preventDefault();
  signal.abort();
};
const output = document.querySelector<HTMLElement>('#output')!;

function showError(error: any) {
  loading.style.display = 'none';
  abortButton.style.display = 'none';
  output.textContent = error.message ? error.message : String(error);

  const okButton = document.createElement('button');
  okButton.innerText = 'OK';
  okButton.onclick = (evt) => {
    evt.preventDefault();
    closeWindow();
  };
  abortButton.insertAdjacentElement('afterend', okButton);

  return true;
}

analyzeAndSolve(model, signal)
  .then(() => setTimeout(() => closeWindow(), 2000))
  .catch((error) => showError(error));

