import express from 'express';
import cors from 'cors';
import {initialize} from 'express-openapi';
import {json, text} from 'body-parser';
import {resolve} from 'path';
import {AbortSignal, IResult} from 'minizinc';
import createDriver from 'minizinc/build/driver';
import * as swaggerUi from 'swagger-ui-express';
import yargs, {Argv} from 'yargs';

const app = express();
const driver = createDriver();

app.use(json());
app.use(text());
app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, {
  swaggerUrl: '../v1.0/api-docs'
}));


function closeSignal(req: any) {
  const signal = new AbortSignal();
  req.on('close', () => {
    console.warn('connection closed by client, aborting');
    signal.abort();
  });
  return signal;
}

function handleError(res: any, error: any) {
  console.warn('caught error', error);
  if (error.errors) {
    return res.status(400).send(JSON.stringify(error.errors));
  }
  return res.status(500).send(error.toString());
}

initialize({
  app,
  apiDoc: resolve(__dirname, 'openapi.yaml'),
  docsPath: '/api-docs',
  validateApiDoc: false,
  promiseMode: true,
  operations: {
    postSolve(req, res) {
      let params = req.body;
      if (typeof req.body === 'string') {
        params = {
          model: req.body
        };
      }
      const signal = closeSignal(req);
      return driver.solve(params, params.data, Object.assign({}, params.options || {}, {signal}))
         .then((result) => res.send(result))
         .catch(handleError.bind(this, res));
    },
    postSolveAll(req, res) {
      let params = req.body;
      if (typeof req.body === 'string') {
        params = {
          model: req.body
        };
      }
      params.all_solutions = true;
      const signal = closeSignal(req);
      return driver.solve(params, params.data, Object.assign({}, params.options || {}, {signal}))
         .then((result) => res.send(result))
         .catch(handleError.bind(this, res));
    },
    postSolveStream(req, res) {
      let params = req.body;
      if (typeof req.body === 'string') {
        params = {
          model: req.body
        };
      }
      const signal = closeSignal(req);
      let first = true;
      const sendHeader = () => {
        if (!first) {
          return;
        }
        first = false;
        res.writeHead(200, {
          'Content-Type': 'text/plain', //'application/x-ndjson'
        });
      };
      const onPartialResult = (type: 'solution' | 'header', result: IResult) => {
        sendHeader();
        if (type === 'header') {
          res.write(JSON.stringify({header: result.header}));
        } else {
          res.write(JSON.stringify({solution: result.solutions[result.solutions.length - 1]}));
        }
        res.write('\n');
      };
      return driver.solve(params, params.data, Object.assign({}, params.options || {}, {signal, onPartialResult}))
        .then((result) => {
          sendHeader();
          delete result.solutions;
          res.write(JSON.stringify({result}));
          res.write('\n');
          res.end();
        })
        .catch(handleError.bind(this, res));
    },
    postSolveRaw(req, res) {
      let params = req.body;
      if (typeof req.body === 'string') {
        params = {
          model: req.body
        };
      }
      const signal = closeSignal(req);
      let first = true;
      const sendHeader = () => {
        if (!first) {
          return;
        }
        first = false;
        res.writeHead(200, {
          'Content-Type': 'text/plain'
        });
      };
      const onChunk = (chunk: string) => {
        sendHeader();
        res.write(chunk);
      };
      return driver.solveRaw(params, params.data,  Object.assign({}, params.options || {}, {signal, onChunk}))
        .then((result) => {
          if (first) {
            sendHeader();
            res.send(result);
          }
          res.end();
        })
        .catch(handleError.bind(this, res));
    },
    postAnalyze(req, res) {
      let params = req.body;
      if (typeof req.body === 'string') {
        params = {
          model: req.body
        };
      }
      const signal = closeSignal(req);
      return driver.analyze(params, Object.assign({}, params.options || {}, {signal}))
        .then((model) => res.send(model))
        .catch(handleError.bind(this, res));
    },
    getResolveSolvers(_req, res) {
      return driver.resolveSolvers()
        .then((solvers) => res.send(solvers))
        .catch(handleError.bind(this, res));
    },
    getVersion(_req, res) {
      return driver.version()
        .then((version) => res.send(version))
        .catch(handleError.bind(this, res));
    },
    postTerminate(_req, res) {
      return driver.terminate()
        .then((r) => res.send(r))
        .catch(handleError.bind(this, res));
    }
  }
});

// app.use((err: any, _req: any, res: any) => {
//   console.log(err, _req, res);
//   if (typeof res.status === 'function') {
//     res.status(err.status);
//   }
//   if (typeof res.json === 'function') {
//     res.json(err);
//   }
// });

app.use(express.static('public'));

interface IOptions {
  port: number;
  host: string;
}

const args = (<Argv<IOptions>>yargs(process.argv))
  .option('port', {
    alias: 'p',
    describe: 'Port to launch',
    number: true,
    default: 5000
  }).option('host', {
    alias: 'host',
    describe: 'Host to attach to',
    default: 'localhost'
  }).help()
  .argv;

console.log(`running at http://${args.host}:${args.port}/api-docs/`);
app.listen(args.port, args.host);
