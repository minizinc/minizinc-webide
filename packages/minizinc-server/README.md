MiniZinc Server
===============
[![License: MIT][mit-image]][mit-url] [![NPM version][npm-image]][npm-url]

This is a REST server based on [Express and OpenAPI](https://www.npmjs.com/package/express-openapi) providing an unified access to [minizinc](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc). Best to be used with the [RESTMiniZinc](https://gitlab.com/minizinc/minizinc-webide/blob/master/packages/minizinc/src/RESTMiniZinc.ts) helper class.

Usage
-----

```
npm i minizinc-server
npx minizinc-server --port 5000

``` 
// access localhost:5000/api-docs for Swagger UI
// example client -> public/index.js


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```


[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
[npm-image]: https://badge.fury.io/js/minizinc-server.svg
[npm-url]: https://npmjs.org/package/minizinc-server
