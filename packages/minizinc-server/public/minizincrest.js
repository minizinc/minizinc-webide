
(function () {

  const BASEURL = `http://localhost:5000/v1.0`;


  class BackendException extends Error {
    constructor(errors) {
      super(errors[0].message);
      this.errors = errors;
    }

    get error() {
      return this.errors[0];
    }
  }


  function parseNDJSON(r, onChunk, onFinish) {
    const parseChunk = (value, ended) => {
      let start = 0;
      let end = -1;
      do {
        end = value.indexOf('\n', start);
        if (end < 0 && ended) {
          end = value.length;
        }
        if (end < 0) {
          break;
        }
        const line = value.slice(start, end);
        start = end + 1;
        if (line.trim().length > 0) {
          const obj = JSON.parse(line);
          onChunk(obj);
        }
      } while (start < value.length);
      return start < value.length ? value.slice(start) : '';
    };
    const reader = r.body.getReader();
    const decoder = new TextDecoder();
    let output = '';
    const readBlock = ({done, value}) => {
      if (done) {
        parseChunk(output, true);
        return onFinish();
      }
      output = parseChunk(output + decoder.decode(value), false);
      return reader.read().then(readBlock);
    };
    return reader.read().then(readBlock);
  }

  function handleError(r) {
    if (r.ok) {
      return r.json().catch(handleGenericError);
    }
    return r.text().then((t) => {
      let result = t;
      try {
        result = JSON.parse(t);
      }
      catch (_a) {
        // dummy
      }
      if (typeof result === 'string') {
        throw new BackendException([{
          type: 'unknown',
          message: result
        }]);
      }
      throw new BackendException(Array.isArray(result) ? result : [result]);
    });
  }

  function handleStream(onNewSolution) {
    return (r) => {
      if (!r.ok) {
        return handleError(r);
      }
      const result = {
        outputs: [],
        solutions: [],
        complete: false,
        status: 'UNKNOWN'
      };
      const onChunk = (chunk) => {
        if (chunk.solution) {
          result.solutions.push(chunk.solution);
          onNewSolution(chunk.solution, result);
        }
        else if (chunk.result) {
          Object.assign(result, chunk.result, {solutions: result.solutions});
        }
      };
      return parseNDJSON(r, onChunk, () => result).catch(handleGenericError);
    };
  }

  function handleGenericError(error) {
    if (error instanceof BackendException || Array.isArray(error.errors)) {
      throw error;
    }
    throw new BackendException([{
      type: 'unknown',
      message: String(error)
    }]);
  }

  function fetchAbort(signal) {
    if (!signal) {
      return undefined;
    }
    const controller = new AbortController();
    signal.onabort = () => controller.abort();
    return controller.signal;
  }

  window.MiniZincRest = {
    createSignal() {
      return {
        abort() {
          if (this.onabort) {
            this.onabort();
          }
        },
        onabort: () => undefined
      };
    },
    analyze: (code, options = {}) => {
      return fetch(`${BASEURL}/analyze`, {
        method: 'POST',
        body: code,
        headers: {
          'Content-Type': 'text/plain'
        },
        signal: fetchAbort(options.signal)
      }).then(handleError).catch(handleGenericError);
    },
    solve: (code, data, options = {}) => {
      const params = typeof code === 'string' ? {model: code} : code;
      if (data) {
        params.data = Object.assign({}, params.data || {}, data);
      }
      return fetch(`${BASEURL}/solve${options.onNewSolution ? '/stream' : ''}`, {
        method: 'POST',
        body: JSON.stringify(params, null, 2),
        headers: {
          'Content-Type': 'application/json'
        },
        signal: fetchAbort(options.signal)
      }).then(options.onNewSolution ? handleStream(options.onNewSolution) : handleError).catch(handleGenericError);
    },
    resolveSolvers: () => {
      return fetch(`${BASEURL}/resolveSolvers`, {
        method: 'GET'
      }).then(handleError).catch(handleGenericError);
    }
  }
})();
