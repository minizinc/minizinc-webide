import {remote, ipcRenderer} from 'electron';
import {readFile, writeJSONSync, writeFileSync, readJSON} from 'fs-extra';
import {IDataObject} from 'minizinc';
import {IUIAdapter, IFile, ApplicationStore} from 'minizinc-webide';
import {EVENT_UI_ADAPTER, IUIAdapterRequest} from './messages';

export default class ElectronUIAdapter implements IUIAdapter {
  store: ApplicationStore | null = null;

  constructor() {
    ipcRenderer.on(EVENT_UI_ADAPTER, (_evt: any, arg: IUIAdapterRequest) => {
      if (!this.store) {
        return;
      }
      switch (arg) {
        case 'new':
          this.store.createNew();
          break;
        case 'open':
          this.store.open();
          break;
        case 'openData':
          this.store.openData();
          break;
        case 'save':
          this.store.save();
          break;
        case 'saveAs':
          this.store.saveAs();
          break;
        case 'solve':
          this.store.solve();
          break;
        case 'analyze':
          this.store.compile();
          break;
        case 'abort':
            this.store.abort();
            break;
      }
    });
  }

  openFile() {
    const r = remote.dialog.showOpenDialog({
      properties: ['openFile'],
      filters: [
        {name: 'MiniZinc', extensions: ['mzn']}
      ]
    });

    if (!r || r.length === 0) {
      return Promise.reject('no files selected');
    }
    return readFile(r[0]).then((content) => (<IFile>{
      fileName: r[0],
      content: content.toString()
    }));
  }

  openData() {
    const r = remote.dialog.showOpenDialog({
      properties: ['openFile'],
      filters: [
        {name: 'MiniZinc Data JSON', extensions: ['json']}
      ]
    });

    if (!r || r.length === 0) {
      return Promise.reject('no files selected');
    }
    return readJSON(r[0]);
  }

  save(fileName: string, code: string, data: IDataObject) {
    writeFileSync(fileName, code);
    if (Object.keys(data).length > 0) {
      writeJSONSync(`${fileName.endsWith('.mzn') ? fileName.slice(0, fileName.length - 4) : fileName}.json`, data);
    }
  }

  saveAs(code: string, data: IDataObject) {
    const fileName = remote.dialog.showSaveDialog({
      filters: [
        {name: 'MiniZinc', extensions: ['mzn']}
      ]
    });
    if (!fileName) {
      return null;
    }
    this.save(fileName, code, data);
    return fileName;
  }
}

