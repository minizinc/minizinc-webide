export const EVENT_UI_ADAPTER = 'uiAdapter';
export declare type IUIAdapterRequest = 'new' | 'save' | 'saveAs' | 'open' | 'openData' | 'solve' | 'analyze' | 'abort';
