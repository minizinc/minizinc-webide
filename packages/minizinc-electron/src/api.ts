import {ipcMain, Menu, shell, BrowserWindow} from 'electron';
import createDriver from 'minizinc/build/driver';
import MessagingReceiver from 'minizinc/build/MessagingReceiver';
import {EVENT_UI_ADAPTER} from './messages';
import {IMiniZincConstructorOptions} from 'minizinc';
import charts from 'raw-loader!minizinc-webide/scripts/charts.mzn';
import json from 'raw-loader!minizinc-webide/scripts/json.mzn';
import ui from 'raw-loader!minizinc-webide/scripts/ui.mzn';

const common: Partial<IMiniZincConstructorOptions> = {
  extraIncludeFiles: {
    'charts.mzn': charts,
    'json.mzn': json,
    'ui.mzn': ui,
  }
};

const driver = createDriver(common);


export default function register(window: BrowserWindow) {
  const r = new MessagingReceiver(driver, ipcMain);
  r.register();

  const send = (key: string) => () => window.webContents.send(EVENT_UI_ADAPTER, key);

  const isMac = process.platform === 'darwin';
  const template: any[] = [
    // { role: 'appMenu' }
    ...(isMac ? [{
      label: 'MiniZinc IDE',
      submenu: [
        {role: 'about'},
        {type: 'separator'},
        {role: 'services'},
        {type: 'separator'},
        {role: 'hide'},
        {role: 'hideothers'},
        {role: 'unhide'},
        {type: 'separator'},
        {role: 'quit'}
      ]
    }] : []),
    // { role: 'fileMenu' }
    {
      label: 'File',
      submenu: [
        {
          label: 'New File...',
          accelerator: 'CmdOrCtrl+N',
          click: send('new')
        },
        {type: 'separator'},
        {
          label: 'Open File...',
          accelerator: 'CmdOrCtrl+O',
          click: send('open')
        },
        {
          label: 'Open Data File...',
          accelerator: 'CmdOrCtrl+Shift+O',
          click: send('openData')
        },
        {type: 'separator'},
        {
          label: 'Save File',
          accelerator: 'CmdOrCtrl+S',
          click: send('save')
        },
        {
          label: 'Save File As...',
          accelerator: 'CmdOrCtrl+Shift+S',
          click: send('saveAs')
        },
        {type: 'separator'},
        isMac ? {role: 'close'} : {role: 'quit'}
      ]
    },
    // { role: 'editMenu' }
    {
      label: 'Edit',
      submenu: [
        {role: 'undo'},
        {role: 'redo'},
        {type: 'separator'},
        {role: 'cut'},
        {role: 'copy'},
        {role: 'paste'},
        ...(isMac ? [
          {role: 'pasteAndMatchStyle'},
          {role: 'delete'},
          {role: 'selectAll'},
          {type: 'separator'},
          {
            label: 'Speech',
            submenu: [
              {role: 'startspeaking'},
              {role: 'stopspeaking'}
            ]
          }
        ] : [
            {role: 'delete'},
            {type: 'separator'},
            {role: 'selectAll'}
          ])
      ]
    },
    {
      label: 'MiniZinc',
      submenu: [
        {
          label: 'Run/Solve...',
          accelerator: 'CmdOrCtrl+R',
          click: send('solve')
        },
        {
          label: 'Stop/Cancel',
          accelerator: 'CmdOrCtrl+E',
          click: send('abort')
        },
        {
          label: 'Compile/Analyze...',
          accelerator: 'CmdOrCtrl+B',
          click: send('analyze')
        }
      ]
    },
    // { role: 'viewMenu' }
    {
      label: 'View',
      submenu: [
        {
          role: 'reload',
          accelerator: null
        },
        {
          role: 'forcereload',
          accelerator: null
        },
        {role: 'toggledevtools'},
        {type: 'separator'},
        {role: 'resetzoom'},
        {role: 'zoomin'},
        {role: 'zoomout'},
        {type: 'separator'},
        {role: 'togglefullscreen'}
      ]
    },
    // { role: 'windowMenu' }
    {
      label: 'Window',
      submenu: [
        {role: 'minimize'},
        {role: 'zoom'},
        ...(isMac ? [
          {type: 'separator'},
          {role: 'front'},
          {type: 'separator'},
          {role: 'window'}
        ] : [
            {role: 'close'}
          ])
      ]
    },
    {
      role: 'help',
      submenu: [
        {
          label: 'Learn More',
          click() {
            shell.openExternalSync('https://www.minizinc.org');
          }
        }
      ]
    }
  ];

  const menu = Menu.buildFromTemplate(template);
  window.setMenu(menu);
}
