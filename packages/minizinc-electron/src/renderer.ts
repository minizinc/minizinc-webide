import MessagingProxyMiniZinc from 'minizinc/build/MessagingMiniZinc';
import launch from 'minizinc-webide';
import ElectronUIAdapter from './ElectronUIAdapter';
import {ipcRenderer} from 'electron';

launch(new MessagingProxyMiniZinc(ipcRenderer), new ElectronUIAdapter());
