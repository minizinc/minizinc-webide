minizinc-webide
============
[![License: MIT][mit-image]][mit-url]


This is an [Electron](https://electronjs.org/) wrapper around the [minizinc-webide](../packages/minizinc-webide) combining it the [minizinc](../packages/minizinc) server.


Usage
-----

**TODO**


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc-webide
npm install
```

**Build distribution packages**

```bash
npm run build
```

**Run Linting**

```bash
npm run lint
```



[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
