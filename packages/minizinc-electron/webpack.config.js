const resolve = require('path').resolve;
const {createConfig, withHTMLPlugin, withCSSExtractPlugin} = require('../../webpack.base');

/**
 * generate a webpack configuration
 */
module.exports = (_env, options) => {
  const dev = options.mode.startsWith('d');

  const server = createConfig({
    mode: options.mode,
    pkg: require('./package.json'),
  }, [{
    node: {
      __dirname: false,
    },
    target: 'electron-main',
    entry: {
      index: './src/index.ts'
    },
    output: {
      path: resolve(__dirname, 'build')
    }
  }]);

  const client = createConfig({
    mode: options.mode,
    pkg: require('./package.json'),
  }, [{
    target: 'electron-renderer',
    entry: {
      app: './src/renderer.ts'
    },
    output: {
      path: resolve(__dirname, 'build')
    }
  },
  withCSSExtractPlugin(),
  withHTMLPlugin({
    chunks: ['app'],
    template: resolve(__dirname, '../minizinc-webide/src/index.html'),
    title: 'MiniZinc IDE',
  })
    ]);

  return [server, client];
};
