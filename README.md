minizinc-webide
============
[![License: MIT][mit-image]][mit-url] [![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)


This is a monorepo for node.js packages around [MiniZinc](https://minizinc.org). It includes

 * [minizinc](./packages/minizinc/README.md) a common wrapper node package around [MinZinc](http://minizinc.org) providing a common interface for MinZinc CLI and Emscripten version
 * [minizinc-server](./packages/minizinc-server/README.md) a REST server based on [Express and OpenAPI](https://www.npmjs.com/package/express-openapi) providing access to the MiniZinc wrapper
 * [minizinc-webide](./packages/minizinc-webide/README.md) an Web-IDE based on [React](https://reactjs.org/) and [Monaco](https://microsoft.github.io/monaco-editor) using an REST service or the embedded version of MiniZinc
 * [minizinc-embedded](./packages/minizinc-embedded/README.md) an embedding friendly utility for MiniZinc
 * [minizinc-electron](./packages/minizinc-electron/README.md) an [Electron](https://electronjs.org/) wrapper around the web ide combining it with an server

This repo is using [Lerna](https://github.com/lerna/lerna) for managing the monorepo. 

Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-webide.git
cd minizinc
npm install
```

**Build distribution packages**

```bash
npm run dist
```

[mit-image]: https://img.shields.io/badge/License-MIT-yellow.svg
[mit-url]: https://opensource.org/licenses/MIT
